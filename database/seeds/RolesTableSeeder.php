<?php
use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $owner = new Role();
        $owner = new Role();
        $owner->name         = 'superadmin';
        $owner->display_name = 'Project Owner'; // optional
        $owner->description  = 'User is the owner of a project'; // optional
        $owner->save();

        $admin = new Role();
        $admin->name         = 'agency';
        $admin->display_name = 'Agency Owner'; // optional
        $admin->description  = 'User is the owner of agency'; // optional
        $admin->save();

        $admin = new Role();
        $admin->name         = 'agent';
        $admin->display_name = 'Agency Admin'; // optional
        $admin->description  = 'User is the admin of agency'; // optional
        $admin->save();
    }
}
