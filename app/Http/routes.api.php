<?php
/*
|--------------------------------------------------------------------------
| Web Router
|--------------------------------------------------------------------------
*/


/*login and registration api*/
Route::group(['prefix' => 'api', 'middleware' => ['cors']], function () {
    //authenticate api for get jwt token
    Route::post('authenticate', 'Auth\JwtAuthenticateController@authenticate');

    //register api for regitraion and get jwt token
    Route::post('register', 'Auth\JwtAuthenticateController@register');



    

//    --------Add to cart Package Start---


//    --------Add to cart Package End---



    Route::get('image/{folder}/{file}', 'Auth\JwtAuthenticateController@getPictureAction');
    Route::get('/countries', function () {
        return Countries::getList('en', 'xml');
    });

});


/*login and registration api*/
Route::group(['prefix' => 'api', 'middleware' => ['cors', 'jwt.auth']], function () {


    //token check
    Route::post('token-check', 'Auth\JwtAuthenticateController@checkRoles');

    /*User's traveller*/
    Route::get('traveller', 'PassengerforselectJWTController@getPassengerListByUser');
    Route::get('traveller/{id}', 'PassengerforselectJWTController@getSingleTravellerJWT');
    Route::post('traveller', 'PassengerforselectJWTController@createSingleTravellerJWT');
    Route::match(['put', 'patch'], 'traveller/{id}', 'PassengerforselectJWTController@updateSingleTravellerJWT');
    Route::delete('traveller/{id}', 'PassengerforselectJWTController@deleteSingleTravellerJWT');


});Route::group(['prefix' => 'api', 'middleware' => ['cors', 'jwt.auth']], function () {


    //token check
    Route::post('token-check', 'Auth\JwtAuthenticateController@checkRoles');

    /*User's traveller*/
    Route::get('traveller', 'PassengerforselectJWTController@getPassengerListByUser');
    Route::get('traveller/{id}', 'PassengerforselectJWTController@getSingleTravellerJWT');
    Route::post('traveller', 'PassengerforselectJWTController@createSingleTravellerJWT');
    Route::match(['put', 'patch'], 'traveller/{id}', 'PassengerforselectJWTController@updateSingleTravellerJWT');
    Route::delete('traveller/{id}', 'PassengerforselectJWTController@deleteSingleTravellerJWT');


});
Route::group(['prefix' => 'api', 'middleware' => ['cors', 'jwt.refresh']], function () {
    //token check
    Route::post('token-refresh', 'Auth\JwtAuthenticateController@tokenRefresh');

});
//Route::group(['prefix' => 'api', 'middleware' => ['before' => 'jwt.refresh', 'after' => 'jwt.auth']], function () {
//    Route::post('info', 'Auth\JwtAuthenticateController@checkRoles');
//});




Route::group(['prefix' => 'api', 'middleware' => ['cors']], function () {
    //token check
    Route::post('trigger/air-availability', 'TravelportController@airAvailability');
    Route::post('trigger/amadeus/air-availability', 'AmadeusController@lfsAction');
    Route::post('trigger/qpx/air-availability', 'GoogleQPXController@airAvailability');

});