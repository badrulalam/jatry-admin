<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
//use Illuminate\Routing\UrlGenerator;

use App\Http\Requests;
use Session;
use Auth;
use Hash;
use Mail;
//use Request;

class UserController extends Controller
{


    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('auth', ['except' => ['resetPasswordAction', 'resetPassworSendAction', 'resetPassworCodeAction', 'resetPassworCodeDoneAction']]);

    }



    protected function newAction()
    {
        $roles = Role::lists('display_name','id');
        return view('user.new',compact('roles'));
    }

    protected function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }



//    public function index()
//    {
//        $users = \DB::table('users')->get();
//        return view('user.index', ['users' => $users]);
//    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $data = User::orderBy('id','DESC')->paginate(5);
        $data = User::orderBy('id','ASC')->paginate(10);
        return view('user.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 3);
    }




//    public function edit($id)
//    {
//        $user = User::findOrFail($id);
//        return view('user.edit', ['user' => $user]);
//    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::lists('display_name','id');
        $userRole = $user->roles->lists('id','id')->toArray();

        return view('user.edit',compact('user','roles','userRole'));
    }




//    public function update($id, Request $request)
//    {
//        $user = User::findOrFail($id);
//        $this->validate($request, [
//            'name' => 'required',
//            'email' => 'required'
//        ]);
//        $input = $request->all();
//        $user->fill($input)->save();
//       \Session::flash('flash_message', 'Task successfully added!');
//        return redirect()->back();
//    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
//            $input['password'] = Hash::make($input['password']);
            $input['password'] = bcrypt($input['password']);
        }else{
            $input = array_except($input,array('password'));
        }

        $user = User::findOrFail($id);
        $user->update($input);
        \DB::table('role_user')->where('user_id',$id)->delete();


        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User updated successfully');
    }






    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
//        \Session::flash('flash_message', 'Task successfully deleted!');
        return redirect()->route('allusers')
            ->with('success','User deleted successfully');
    }


}
