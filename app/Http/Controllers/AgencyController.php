<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;



use App\User;
use App\Agency;
use App\Agent;
use App\Role;
use DB;

use Html;

use Session;
//use \Illuminate\Support\Facades\Auth;


class AgencyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function addAgentAction()
    {
        return view('adminarea.agency.addagent');

    }


    public function agencyAgentListAction(Request $request)
    {


        $data = Agent::where('agency_id', \Auth::user()->agency->id)
        ->orderBy('id','ASC')->paginate(3);
        return view('adminarea.agency.agencyagentlist',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 3);

        return view('adminarea.agency.addagent');

    }


    public function addAgentSaveAction(Request $request)

    {

        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',


            'first_name' => 'required',
            'agent_email' => 'required|email|unique:agent,email',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postalcode' => 'required',
            'country' => 'required',
            'phone' => 'required',
            'image' => 'required',

        ]);


        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $role = Role::where('name', 'agent')->first();
        $user->attachRole($role->id);

        $agent = new Agent();
        $agent->first_name = $request->input('first_name');
        $agent->last_name = $request->input('last_name');
        $agent->email = $request->input('email');
        $agent->address = $request->input('address');
        $agent->city = $request->input('city');
        $agent->state = $request->input('state');
        $agent->postalcode = $request->input('postalcode');
        $agent->country = $request->input('country');
        $agent->phone = $request->input('phone');


//        if($request->input('isactive') == 1){
        if($request->has('isactive')){
            $agent->isactive = true;
        }
        else{
            $agent->isactive = false;
        }
        $agent->user_id = $user->id;

        if ($request->file('image')) {
            $image = $request->file('image');
            $imagename = rand(11111,99999).'-'.$image->getClientOriginalName();
            $image->move(storage_path().'/uploads/agent-image/', $imagename);

            $agent->image = $imagename;
//            return  $image->getClientOriginalName();

        }

        $agent->agency_id = \Auth::user()->agency->id;
        $agent->save();

        $agent->code = 'AGT-'.$agent->id;
        $agent->save();

        return redirect()->route('agencyagentlist')
            ->with('success','Agent created successfully');

//        return $request->input();

    }

    public function agencyAgentEditAction($id)
    {
        $agent = Agent::findOrFail($id);

    if(\Auth::user()->hasRole('superagent') && ($agent->agency_id == \Auth::user()->agency->id)){
            return view('adminarea.agency.editagent',compact('agent'));
        }
        else{
            return 'This agent not of your';
        }

    }

    public function agencyAgentUpdateAction(Request $request, $id)
    {

        $agent = Agent::findOrFail($id);

//        dd(storage_path().'/uploads/agent-image/'.$agent->image);
//        dd($request->input('isactive'));




        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$agent->user->id,
            'password' => 'same:confirm-password',


            'first_name' => 'required',
            'agent_email' => 'required|email|unique:agent,email,'.$id,
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postalcode' => 'required',
            'country' => 'required',
            'phone' => 'required',

        ]);


//        $agent = Agent::findOrFail($id);

        $user = $agent->user;

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if(!empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();


        $agent->first_name = $request->input('first_name');
        $agent->last_name = $request->input('last_name');
        $agent->email = $request->input('email');
        $agent->address = $request->input('address');
        $agent->city = $request->input('city');
        $agent->state = $request->input('state');
        $agent->postalcode = $request->input('postalcode');
        $agent->country = $request->input('country');
        $agent->phone = $request->input('phone');



        if ($request->has('isactive')) {
            $agent->isactive = true;
        }
        else{
            $agent->isactive = false;
        }

        if ($request->file('image')) {
            \File::delete(storage_path().'/uploads/agent-image/'.$agent->image);
            $image = $request->file('image');
            $imagename = rand(11111,99999).'-'.$image->getClientOriginalName();
            $image->move(storage_path().'/uploads/agent-image/', $imagename);
            $agent->image = $imagename;

        }

        $agent->save();



        return redirect()->route('agencyagentlist')
            ->with('success','Agent updated successfully');
    }


    public function allAgencyListAction(Request $request)
    {
        $data = Agency::orderBy('id','ASC')->paginate(3);
        return view('adminarea.agency.allagencylist',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 3);
    }


    public function adminAgencyShowAction($id)
    {
        $agent = Agent::findOrFail($id);

        if(\Auth::user()->hasRole('superagent') && ($agent->agency_id == \Auth::user()->agency->id)){
            return view('adminarea.agency.editagent',compact('agent'));
        }
        else{
            return 'This agent not of your';
        }

    }

    public function adminAgencyAgentListAction(Request $request,$id)
    {
        $agent = Agency::findOrFail($id);

        $data = Agent::where('agency_id', $id)
        ->orderBy('id','ASC')
            ->paginate(3);
        return view('adminarea.agency.adminagencyagentlist',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 3);

    }





}
