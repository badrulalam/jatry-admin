<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class LocationController extends Controller
{
    public function getLocationByName(Request $request)
    {
        $name = $request->input('name');
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "https://demo.travelportuniversalapi.com/Api//TravelData/GetLocationByName?name={$name}");

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return (new Response($output, 200))
            ->header('Content-Type', 'application/json; charset=utf-8');
    }

    public function getTest(Request $request)
    {
        $adults = $request->input('adults');
        $child = $request->input('child');
        $infat = $request->input('infant');
        $departureDate = $request->input('departureDate');
        $formatedDepartureDate = date('Y-m-d', strtotime($departureDate));
        $fromAirport = $request->input('fromAirport');
        $returnDate = $request->input('returnDate');
        $formatedReturnDate = date('Y-m-d', strtotime($returnDate));
        $toAirport = $request->input('toAirport');


        $fromAirport1 = $request->input('fromAirport1');
        $toAirport1 = $request->input('toAirport1');
        $departureDate1 = $request->input('departureDate1');
        $formatedDepartureDate1 = date('Y-m-d', strtotime($departureDate1));

        $fromAirport2 = $request->input('fromAirport2');
        $toAirport2 = $request->input('toAirport2');
        $departureDate2 = $request->input('departureDate2');
        $formatedDepartureDate2 = date('Y-m-d', strtotime($departureDate2));

        $fromAirport3 = $request->input('fromAirport3');
        $toAirport3 = $request->input('toAirport3');
        $departureDate3 = $request->input('departureDate3');
        $formatedDepartureDate3 = date('Y-m-d', strtotime($departureDate3));

        $fromAirport4 = $request->input('fromAirport4');
        $toAirport4 = $request->input('toAirport4');
        $departureDate4 = $request->input('departureDate4');
        
        $formatedDepartureDate4 = date('Y-m-d', strtotime($departureDate4));

        $type = $request->input('tripType');





        $all_request = $request->input();

        $TARGETBRANCH = 'P7004166';
        $CREDENTIALS = 'Universal API/uAPI1750790704-327b5ca9:XaW2P3SQrD746HZW5s7e4wXW5';
        $Provider = '1G'; // Any provider you want to use like 1G/1P/1V/ACH


        if ($type == 'O'){
            $message = one_way($TARGETBRANCH, $fromAirport, $toAirport, $formatedDepartureDate, $Provider);
        }
        elseif ($type == 'R'){
            $message = round_trip($TARGETBRANCH, $fromAirport, $toAirport, $formatedDepartureDate, $formatedReturnDate, $Provider);
        }
        elseif ($type == 'multicity_2'){
            $message = multicity_2($TARGETBRANCH, $fromAirport1, $toAirport1, $formatedDepartureDate1, $fromAirport2, $toAirport2, $formatedDepartureDate2, $Provider);
        }
        elseif ($type == 'multicity_3'){
            $message = multicity_3($TARGETBRANCH, $fromAirport1, $toAirport1, $formatedDepartureDate1, $fromAirport2, $toAirport2, $formatedDepartureDate2, $fromAirport3, $toAirport3, $formatedDepartureDate3, $Provider);
        }
        elseif ($type == 'multicity_4'){
            $message = multicity_4($TARGETBRANCH, $fromAirport1, $toAirport1, $formatedDepartureDate1, $fromAirport2, $toAirport2, $formatedDepartureDate2, $fromAirport3, $toAirport3, $formatedDepartureDate3, $fromAirport4, $toAirport4, $formatedDepartureDate4, $Provider);
        }else{
            return 'type not match';
        }

        $auth = base64_encode("$CREDENTIALS");
        $soap_do = curl_init("https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService");
        $header = array(
            "Content-Type: text/xml;charset=UTF-8",
            "Accept: gzip,deflate",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"\"",
            "Authorization: Basic $auth",
            "Content-length: " . strlen($message),
        );


//        dd($message);
//curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 30);
//curl_setopt($soap_do, CURLOPT_TIMEOUT, 30);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $message);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true); // this will prevent the curl_exec to return result and will let us to capture output
        $return = curl_exec($soap_do);
//        return $return;
        return (new Response($return, 200))
            ->header('Content-Type', 'application/xml; charset=utf-8');
    }

    public function getDummy()
    {
        $bodyElement = getBody();
        $headerElement = getHeader();
        $envelopElement = getEnvelope();
        $lfsElement = getLowFareSearchReq();
        $air_leg1 =getSearchAirLeg('DAC', 'DEL', '2016-07-28');

        sxml_append($lfsElement, getBillingPointOfSaleInfo());
        sxml_append($lfsElement, $air_leg1);
        sxml_append($lfsElement, getAirSearchModifiers());
        sxml_append($lfsElement, getSearchPassenger());
        sxml_append($lfsElement, getAirPricingModifiers());
        sxml_append($bodyElement, $lfsElement);
        sxml_append($envelopElement, $headerElement);
        sxml_append($envelopElement, $bodyElement);

        $message_obj = $envelopElement;
        $message = <<<XML
XML;
        return (new Response($message, 200))
            ->header('Content-Type', 'application/xml; charset=utf-8');
    }

    /**
     * @return string
     */
    public function getXml()
    {

        $bodyElement = getBody();
        $headerElement = getHeader();
        $envelopElement = getEnvelope();
        $lfsElement = getLowFareSearchReq();
        $air_leg1 =getSearchAirLeg('DAC', 'DEL', '2016-07-28');

        sxml_append($lfsElement, getBillingPointOfSaleInfo());
        sxml_append($lfsElement, $air_leg1);
        sxml_append($lfsElement, getAirSearchModifiers());
        sxml_append($lfsElement, getSearchPassenger());
        sxml_append($lfsElement, getAirPricingModifiers());
        sxml_append($bodyElement, $lfsElement);
        sxml_append($envelopElement, $headerElement);
        sxml_append($envelopElement, $bodyElement);

        return (new Response($envelopElement->asXML(), 200))
            ->header('Content-Type', 'application/xml; charset=utf-8');
//        return $movies->asXML();
    }
public function ConvertAction(){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://localhost/tv/xmldata.xml");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $xml_data = curl_exec($ch);
    curl_close($ch);

    $xml_obj = new \SimpleXMLElement($xml_data);
    return (new Response($xml_data, 200))
        ->header('Content-Type', 'application/xml; charset=utf-8');


}







    function append_simplexml(&$simplexml_to, &$simplexml_from)     {
        foreach ($simplexml_from->children() as $simplexml_child)
        {
            $simplexml_temp = $simplexml_to->addChild($simplexml_child->getName(), (string) $simplexml_child);
            foreach ($simplexml_child->attributes() as $attr_key => $attr_value)
            {
                $simplexml_temp->addAttribute($attr_key, $attr_value);
            }

            append_simplexml($simplexml_temp, $simplexml_child);
        }
    }
}

Class TravelportXmlBuilder{
   public function getSearchAirLeg($originCityCode, $destinationCityCode, $preferredTime, $xmlns='http://www.travelport.com/schema/common_v36_0')     {
        $air_leg = <<<XML
    <SearchAirLeg>
    <SearchOrigin>
      <CityOrAirport xmlns="{$xmlns}" Code="{$originCityCode}" PreferCity="true" />
    </SearchOrigin>
    <SearchDestination>
      <CityOrAirport xmlns="{$xmlns}" Code="{$destinationCityCode}" PreferCity="true" />
    </SearchDestination>
    <SearchDepTime PreferredTime="{$preferredTime}" />
    </SearchAirLeg>
XML;
        return new \SimpleXMLElement($air_leg);
    }
}
function getSearchAirLeg($originCityCode, $destinationCityCode, $preferredTime, $xmlns='http://www.travelport.com/schema/common_v36_0') {
    $air_leg = <<<XML
    <air:SearchAirLeg xmlns:air="{$xmlns}">
    <air:SearchOrigin>
      <com:CityOrAirport xmlns:com="{$xmlns}" Code="{$originCityCode}" PreferCity="true" />
    </air:SearchOrigin>
    <air:SearchDestination>
      <com:CityOrAirport xmlns:com="{$xmlns}" Code="{$destinationCityCode}" PreferCity="true" />
    </air:SearchDestination>
    <airSearchDepTime PreferredTime="{$preferredTime}" />
    </air:SearchAirLeg>
XML;
    return new \SimpleXMLElement($air_leg);
}
function getSearchPassenger($code = "ADT", $age = "40", $dob="1976-07-19", $xmlns='http://www.travelport.com/schema/common_v36_0') {
    $air_leg = <<<XML
<com:SearchPassenger xmlns:com="{$xmlns}" Code="{$code}" Age="{$age}" DOB="{$dob}" />
XML;
    return new \SimpleXMLElement($air_leg);
}

function getAirSearchModifiers($providerCode = "1G", $xmlns='http://www.travelport.com/schema/common_v36_0') {
    $air_leg = <<<XML
<air:AirSearchModifiers xmlns:air="{$xmlns}">
    <air:PreferredProviders>
      <com:Provider xmlns:com="{$xmlns}" Code="{$providerCode}" />
    </air:PreferredProviders>
  </air:AirSearchModifiers>
XML;
    return new \SimpleXMLElement($air_leg);
}
function getAirPricingModifiers($accountCode = "-", $xmlns='http://www.travelport.com/schema/common_v36_0') {
    $air_leg = <<<XML
  <air:AirPricingModifiers xmlns:air="{$xmlns}" FaresIndicator="PublicFaresOnly">
    <air:AccountCodes >
      <com:AccountCode xmlns:com="{$xmlns}" Code="{$accountCode}" />
    </air:AccountCodes>
  </air:AirPricingModifiers>
XML;
    return new \SimpleXMLElement($air_leg);
}

function getBillingPointOfSaleInfo($originApplication = "uAPI", $xmlns='http://www.travelport.com/schema/common_v36_0') {
    $air_leg = <<<XML
  <com:BillingPointOfSaleInfo xmlns:com="http://www.travelport.com/schema/common_v36_0" OriginApplication="{$originApplication}" />
XML;
    return new \SimpleXMLElement($air_leg);
}


function getLowFareSearchReq ($targetBranch = "P7004166", $traceId = "65ce1e17-319f-467a-b580-e1ad41de405a", $returnUpsellFare = "true", $xmlns='http://www.travelport.com/schema/air_v36_0') {
    $air_leg = <<<XML
  <air:LowFareSearchReq xmlns:air="{$xmlns}" TraceId="{$traceId}" TargetBranch="{$targetBranch}" ReturnUpsellFare="{$returnUpsellFare}">
  
</air:LowFareSearchReq>
XML;
    return new \SimpleXMLElement($air_leg);
}

function getEnvelope ($soapenv="http://schemas.xmlsoap.org/soap/envelope/") {
    $xml_envelop = <<<XML
<soapenv:Envelope xmlns:soapenv="{$soapenv}">
</soapenv:Envelope>
XML;
    return new \SimpleXMLElement($xml_envelop);
}



function getHeader ($soapenv="http://schemas.xmlsoap.org/soap/envelope/") {
    $xml_envelop = <<<XML
<soapenv:Header xmlns:soapenv="{$soapenv}"/>
XML;
    return new \SimpleXMLElement($xml_envelop);
}


function getBody ($soapenv="http://schemas.xmlsoap.org/soap/envelope/") {
    $xml_envelop = <<<XML
  <soapenv:Body xmlns:soapenv="{$soapenv}">
 </soapenv:Body>
XML;
    return new \SimpleXMLElement($xml_envelop);
}




function sxml_append(\SimpleXMLElement $to, \SimpleXMLElement $from) {
    $toDom = dom_import_simplexml($to);
    $fromDom = dom_import_simplexml($from);
    $toDom->appendChild($toDom->ownerDocument->importNode($fromDom, true));
}
function one_way($TARGETBRANCH,
                 $fromAirport,
                 $toAirport,
                 $formatedDepartureDate,
                 $Provider){
    $message = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header/>
   <soapenv:Body>
   <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="92f3577b-1736-460c-baef-d315a710f9d9" TargetBranch="{$TARGETBRANCH}" ReturnUpsellFare="true">
     <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate}" />
     </SearchAirLeg>
     <AirSearchModifiers>
       <PreferredProviders>
         <Provider xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$Provider}" />
       </PreferredProviders>
     </AirSearchModifiers>
     <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" DOB="1976-06-24" />
     <AirPricingModifiers FaresIndicator="PublicFaresOnly" />
   </LowFareSearchReq>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
    return $message;
}

function round_trip($TARGETBRANCH,
                 $fromAirport,
                 $toAirport,
                 $formatedDepartureDate,
                 $formatedReturnDate,
                 $Provider){
    $message = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header/>
   <soapenv:Body>
   <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="92f3577b-1736-460c-baef-d315a710f9d9" TargetBranch="{$TARGETBRANCH}" ReturnUpsellFare="true">
     <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedReturnDate}" />
     </SearchAirLeg>
     <AirSearchModifiers>
       <PreferredProviders>
         <Provider xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$Provider}" />
       </PreferredProviders>
     </AirSearchModifiers>
     <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" DOB="1976-06-24" />
     <AirPricingModifiers FaresIndicator="PublicFaresOnly" />
   </LowFareSearchReq>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
    return $message;
}


function multicity_2($TARGETBRANCH,
                 $fromAirport1,
                 $toAirport1,
                 $formatedDepartureDate1,
                 $fromAirport2,
                 $toAirport2,
                 $formatedDepartureDate2,

                 $Provider){
    $message = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header/>
   <soapenv:Body>
   <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="92f3577b-1736-460c-baef-d315a710f9d9" TargetBranch="{$TARGETBRANCH}" ReturnUpsellFare="true">
     <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport1}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport1}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate1}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport2}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport2}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate2}" />
     </SearchAirLeg>
     <AirSearchModifiers>
       <PreferredProviders>
         <Provider xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$Provider}" />
       </PreferredProviders>
     </AirSearchModifiers>
     <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" DOB="1976-06-24" />
     <AirPricingModifiers FaresIndicator="PublicFaresOnly" />
   </LowFareSearchReq>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
    return $message;
}


function multicity_3($TARGETBRANCH,
                 $fromAirport1,
                 $toAirport1,
                 $formatedDepartureDate1,
                 $fromAirport2,
                 $toAirport2,
                 $formatedDepartureDate2,
                 $fromAirport3,
                 $toAirport3,
                 $formatedDepartureDate3,

                 $Provider){
    $message = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header/>
   <soapenv:Body>
   <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="92f3577b-1736-460c-baef-d315a710f9d9" TargetBranch="{$TARGETBRANCH}" ReturnUpsellFare="true">
     <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport1}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport1}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate1}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport2}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport2}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate2}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport3}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport3}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate3}" />
     </SearchAirLeg>
     <AirSearchModifiers>
       <PreferredProviders>
         <Provider xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$Provider}" />
       </PreferredProviders>
     </AirSearchModifiers>
     <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" DOB="1976-06-24" />
     <AirPricingModifiers FaresIndicator="PublicFaresOnly" />
   </LowFareSearchReq>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
    return $message;
}


function multicity_4($TARGETBRANCH,
                 $fromAirport1,
                 $toAirport1,
                 $formatedDepartureDate1,
                 $fromAirport2,
                 $toAirport2,
                 $formatedDepartureDate2,
                 $fromAirport3,
                 $toAirport3,
                 $formatedDepartureDate3,
                 $fromAirport4,
                 $toAirport4,
                 $formatedDepartureDate4,

                 $Provider){
    $message = <<<EOM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header/>
   <soapenv:Body>
   <LowFareSearchReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="92f3577b-1736-460c-baef-d315a710f9d9" TargetBranch="{$TARGETBRANCH}" ReturnUpsellFare="true">
     <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport1}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport1}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate1}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport2}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport2}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate2}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport3}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport3}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate3}" />
     </SearchAirLeg>
     <SearchAirLeg>
       <SearchOrigin>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$fromAirport4}" PreferCity="true" />
       </SearchOrigin>
       <SearchDestination>
         <CityOrAirport xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$toAirport4}" PreferCity="true" />
       </SearchDestination>
       <SearchDepTime PreferredTime="{$formatedDepartureDate4}" />
     </SearchAirLeg>
     <AirSearchModifiers>
       <PreferredProviders>
         <Provider xmlns="http://www.travelport.com/schema/common_v36_0" Code="{$Provider}" />
       </PreferredProviders>
     </AirSearchModifiers>
     <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" DOB="1976-06-24" />
     <AirPricingModifiers FaresIndicator="PublicFaresOnly" />
   </LowFareSearchReq>
   </soapenv:Body>
</soapenv:Envelope>
EOM;
    return $message;
}