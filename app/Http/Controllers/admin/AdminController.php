<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function adminDashboardAction()
    {
        return view('adminarea.admindash');

    }

    public function agencyDashboardAction()
    {
        return view('adminarea.agencydash');

    }

    public function agentDashboardAction()
    {
        return view('adminarea.agentdash');

    }

    public function adminSearchAction()
    {
        return view('adminarea.adminsearch');

    }
    
    public function amadeusSearchAction()
    {
        return view('adminarea.amadeussearch');

    }
    public function qpxSearchAction()
    {
        return view('adminarea.qpxsearch');

    }



}
