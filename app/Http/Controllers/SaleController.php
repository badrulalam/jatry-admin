<?php

namespace App\Http\Controllers;


use App\Saleairroute;
use App\Saleairroutesegment;
use Illuminate\Http\Request;

use App\Http\Requests;




use Illuminate\Http\Response;

use App\User;
use App\Agency;
use App\Agent;
use App\Role;

use Session;
use Auth;
use PDF;
use Mail;
use App\Cart;
use App\Sale;
use App\Cardinfo;
use App\Cartitemair;
use App\Saleitemair;
use App\Saleitemairpassenger;




class SaleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    function cardByCheck($card_number, $cardholder, $exp_date, $cvv){

        $cart =  Auth::user()->cart;

        $cardinfo = Cardinfo::where('card_number',$card_number )->first();

        if($cardinfo){

        }
        else{
            $cardinfo = new Cardinfo();

            $cardinfo->billing_firstname = $cart->billing_firstname;
            $cardinfo->billing_lastname = $cart->billing_lastname;
            $cardinfo->billing_email = $cart->billing_email;
            $cardinfo->billing_address = $cart->billing_address;
            $cardinfo->billing_city = $cart->billing_city;
            $cardinfo->billing_postalcode = $cart->billing_postalcode;
            $cardinfo->billing_country = $cart->billing_country;
            $cardinfo->billing_state = $cart->billing_state;
            $cardinfo->billing_phone = $cart->billing_phone;

            $cardinfo->card_number = $card_number;
            $cardinfo->cardholder = $cardholder;
            $cardinfo->exp_date = $exp_date;
            $cardinfo->cvv = $cvv;

            $cardinfo->user_id = Auth::user()->id;

            $cardinfo->save();
        }



        return $cardinfo->id;



    }






    public function TicketSaleAction($invoice){
        $sale = Sale::where('order_number', $invoice)->with('saleitemair')->first();
//        ==============

        $customer =  Auth::user()->customer;
        $cuser =  Auth::user();
        $invnum = $sale->order_number;
        $pdflnk = public_path().'/salepdf/'.$invoice.'.pdf';

        $pdf = PDF::loadView('sale.checkout-pdf',['sale'=>$sale]);
        $pdf->save(public_path().'/salepdf/'.$sale->order_number.'.pdf');

        Mail::send('sale.salemail', ['title' => 'Your Invoice mail title', 'content' => 'Please check your invoice attachment'], function ($message) use ($invoice, $cuser)
        {
//            $message->from('no-reply@zeteq.com', 'Christian Nwamba');

            $message->sender('vincej1657@gmail.com');
            $message->subject('Jatry Invoice');

            $message->attach(public_path().'/salepdf/'.$invoice.'.pdf');

//            $message->to('zqarif@gmail.com');
            $message->to($cuser->email);
//            $message->cc('limon3640@gmail.com', $name = null);
            $message->cc('zeshaq@gmail.com', $name = null);
//            $message->getSwiftMessage();

        });
//        ==============

//        unlink(public_path('file/to/delete'));
        unlink(public_path().'/salepdf/'.$sale->order_number.'.pdf');


        return view('sale.checkout-final', compact('sale'));
    }


    public function TicketSalePdfAction($invoice){
//        return "success";

        $sale = Sale::where('order_number', $invoice)->with('saleitemair')->first();
        $pdf = PDF::loadView('sale.checkout-pdf',['sale'=>$sale]);
//        return $pdf->download('invoice.pdf');
        return $pdf->stream();


    }


    public function saleInvoiceAction($invoice){
        $sale = Sale::where('order_number', $invoice)->with('ticket')->first();

//        return $sale;
        return view('adminarea.sale.sale-invoice', compact('sale'));
    }



}
