<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Response;
use App\Passengerforselect;
use App\Cardinfo;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
class PassengerforselectJWTController extends Controller
{
    protected $user;
    public function __construct()
    {
        if(Auth::guest()){
            $this->user = false;
        }elseif(Auth::user()){

            $this->user =  Auth::user();

        }
    }

    public function getPassengerListByUser(){
        return Passengerforselect::where('user_id', $this->user->id)->get();
    }

    public function getSingleTravellerJWT($id){
        $data =  Passengerforselect::where('user_id', $this->user->id)
            ->where('id', $id)->first();
        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json('No record found with the specified `id`.', 404);
        }
    }
    
    public function createSingleTravellerJWT(Request $request){
        $validator = $this->validateParams($request);
        if($validator->fails()){
            return response()->json(['invalidAttributes' => $validator->errors()], 400);
        }
        $passengerforselect = new Passengerforselect();
        $passengerforselect->user_id = $this->user->id;
        $passengerforselect->title = $request->input('title');
        $passengerforselect->first_name = $request->input('first_name');
        $passengerforselect->middle_name = $request->input('middle_name');
        $passengerforselect->last_name = $request->input('last_name');
        $passengerforselect->dob = $request->input('dob');
        $passengerforselect->save();


        if($passengerforselect){
            return response()->json($passengerforselect, 201);
        }else{
            return response()->json($passengerforselect, 500);
        }
    }
    public function updateSingleTravellerJWT(Request $request, $id){

        $validator = $this->validateParams($request);
        if($validator->fails()){
            return response()->json(['invalidAttributes' => $validator->errors()], 400);
        }
        $passengerforselect =  Passengerforselect::where('user_id', $this->user->id)
            ->where('id', $id)->first();
        if(!$passengerforselect){
            return response()->json('No record found with the specified `id`.', 404);
        }
        $passengerforselect->user_id = $this->user->id;
        $passengerforselect->title = $request->input('title');
        $passengerforselect->first_name = $request->input('first_name');
        $passengerforselect->middle_name = $request->input('middle_name');
        $passengerforselect->last_name = $request->input('last_name');
        $passengerforselect->nationality = $request->input('nationality');
        $passengerforselect->passport_no = $request->input('passport_no');
        $passengerforselect->passport_issue_country = $request->input('passport_issue_country');
        $passengerforselect->passport_expiry_date = $request->input('passport_expiry_date');
        $passengerforselect->passenger_type = $request->input('passenger_type');
        $passengerforselect->passenger_order = $request->input('passenger_order');
        $passengerforselect->save();


        if($passengerforselect){
            return response()->json($passengerforselect, 200);
        }else{
            return response()->json($passengerforselect, 500);
        }
    }
    public function deleteSingleTravellerJWT($id){

        $passengerforselect =  Passengerforselect::where('user_id', $this->user->id)
            ->where('id', $id)->first();
        if(!$passengerforselect){
            return response()->json('No record found with the specified `id`.', 404);
        }

        $passengerforselect->delete();


        if($passengerforselect){
            return response()->json($passengerforselect, 200);
        }else{
            return response()->json($passengerforselect, 500);
        }
    }

    private function validateParams($request){
        return Validator::make($request->all(), [
            'title' => 'required|string',
            'first_name' => 'required|string',
            'middle_name' => 'string',
            'last_name' => 'required|string',
            'dob' => 'date',

            'nationality' => 'string',
            'passport_no' => 'string',
            'passport_issue_country' => 'string',
            'passport_expiry_date' => 'string',
            'passenger_type' => 'string',
            'passenger_order' => 'string'

        ]);

    }



}
