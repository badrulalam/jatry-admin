<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Tpackage;
use App\Jcontinent;
use App\Jcountry;
use Auth;
use Session;

class JcountryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function indexAction(Request $request)
    {

        $user = \Auth::user();
        if($user->hasRole('superadmin')){
            $data = Jcountry::orderBy('name','ASC')
                ->paginate(10);
            return view('adminarea.jcountry.index',compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 10);

        }
        else{
            return redirect()->back();
        }




    }


    public function newAction()
    {

         $continent = Jcontinent::lists('name','id');
         $continent->prepend('Select Continent', '');
         return view('adminarea.jcountry.new',compact('continent'));

    }


    public function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:jcountry,name',
            'continent' => 'required',
            'image' => 'required'
        ]);

        $jcountry = new Jcountry();
        $jcountry->name = $request->input('name');
        $jcountry->description = $request->input('description');
        $jcountry->jcontinent_id = $request->input('continent');



        if ($request->file('image')) {
//            \File::delete(storage_path().'/uploads/agent-image/'.$user->agent->image);
            $upimage = $request->file('image');
            $upimagename = rand(11111,99999).'-'.$upimage->getClientOriginalName();
            $upimage->move(storage_path().'/uploads/jcountry-image/', $upimagename);
            $jcountry->image = $upimagename;
        }

        $jcountry->save();
        return redirect()->route('alljcountry')
            ->with('success','Country created successfully');
    }


    public function editAction($id)
    {
        $continent = Jcontinent::lists('name','id');
        $jcountry = Jcountry::findOrFail($id);


        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.jcountry.edit',compact('jcountry', 'continent'));
        }

        else{
            return redirect()->back();
        }

    }

    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'name' => 'required|unique:jcountry,name,'.$id
//            'image' => 'required'
        ]);

        $jcountry = Jcountry::findOrFail($id);
        $jcountry->slug = null;
        $jcountry->name = $request->input('name');
        $jcountry->description = $request->input('description');
        $jcountry->jcontinent_id = $request->input('continent');
        if ($request->file('image')) {
            \File::delete(storage_path().'/uploads/jcountry-image/'.$jcountry->image);
            $upimage = $request->file('image');
            $upimagename = rand(11111,99999).'-'.$upimage->getClientOriginalName();
            $upimage->move(storage_path().'/uploads/jcountry-image/', $upimagename);
            $jcountry->image = $upimagename;
        }
        $jcountry->save();
        return redirect()->route('alljcountry')
            ->with('success','Continent created successfully');
    }

    public function destroyAction($id)
    {

        $jcountry = Jcountry::findOrFail($id);
        if (\Auth::user()->hasRole('superadmin')) {
            \File::delete(storage_path() . '/uploads/jcountry-image/' . $jcountry->image);
            $jcountry->delete();
            return redirect()->route('alljcountry')
                ->with('success', 'Continent deleted successfully');
        } else {
            return redirect()->back();
        }
    }


}
