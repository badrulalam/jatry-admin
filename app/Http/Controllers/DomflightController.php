<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Domairline;
use App\Domcity;
use App\Domflight;
use Auth;
use Session;

class DomflightController extends Controller
{


    public function indexAction(Request $request)
    {
        $data = Domflight::orderBy('id','ASC')->paginate(20);
        return view('adminarea.domflight.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    }


    public function newAction()
    {
        $airlines = Domairline::lists('name','id');
        $airlines->prepend('Select Airline', '');

        $orgin = Domcity::lists('name','id');
        $orgin->prepend('Select Orgin City', '');

        $destination = Domcity::lists('name','id');
        $destination->prepend('Select Destination City', '');

        return view('adminarea.domflight.new',compact('airlines', 'orgin', 'destination'));
    }


    public function createAction(Request $request)
    {
        $this->validate($request, [
//            'name' => 'required|unique:pcategory,name',
            'name' => 'required',
            'airline' => 'required',
            'orgin' => 'required',
            'destination' => 'required',
            'Deperture_time' => 'required',
            'Arrival_time' => 'required',
            'price' => 'required',
            'sat_economy_price' => 'required',
            'sat_business_price' => 'required',
            'sun_economy_price' => 'required',
            'sun_business_price' => 'required',
            'mon_economy_price' => 'required',
            'mon_business_price' => 'required',
            'tue_economy_price' => 'required',
            'tue_business_price' => 'required',
            'wed_economy_price' => 'required',
            'wed_business_price' => 'required',
            'thu_economy_price' => 'required',
            'thu_business_price' => 'required',
            'fri_economy_price' => 'required',
            'fri_business_price' => 'required'
        ]);

        $flight = new Domflight();
        $flight->name = $request->input('name');
        $flight->Deperture_time = $request->input('Deperture_time');
        $flight->Arrival_time = $request->input('Arrival_time');
        $flight->domairline_id = $request->input('airline');
        $flight->orgincity_id = $request->input('orgin');
        $flight->destinationcity_id = $request->input('destination');

        $flight->price = $request->input('price');

        $flight->sat_economy_price = $request->input('sat_economy_price');
        $flight->sat_business_price = $request->input('sat_business_price');
        $flight->sun_economy_price = $request->input('sun_economy_price');
        $flight->sun_business_price = $request->input('sun_business_price');
        $flight->mon_economy_price = $request->input('mon_economy_price');
        $flight->mon_business_price = $request->input('mon_business_price');
        $flight->tue_economy_price = $request->input('tue_economy_price');
        $flight->tue_business_price = $request->input('tue_business_price');
        $flight->wed_economy_price = $request->input('wed_economy_price');
        $flight->wed_business_price = $request->input('wed_business_price');
        $flight->thu_economy_price = $request->input('thu_economy_price');
        $flight->thu_business_price = $request->input('thu_business_price');
        $flight->fri_economy_price = $request->input('fri_economy_price');
        $flight->fri_business_price = $request->input('fri_business_price');

        if($request->has('daily')){
            $flight->daily = true;
        }
        else{
            $flight->daily = false;
        }

        if($request->has('sat')){
            $flight->sat = true;
        }
        else{
            $flight->sat = false;
        }
        if($request->has('sun')){
            $flight->sun = true;
        }
        else{
            $flight->sun = false;
        }
        if($request->has('mon')){
            $flight->mon = true;
        }
        else{
            $flight->mon = false;
        }
        if($request->has('tue')){
            $flight->tue = true;
        }
        else{
            $flight->tue = false;
        }
        if($request->has('wed')){
            $flight->wed = true;
        }
        else{
            $flight->wed = false;
        }
        if($request->has('thu')){
            $flight->thu = true;
        }
        else{
            $flight->thu = false;
        }
        if($request->has('fri')){
            $flight->fri = true;
        }
        else{
            $flight->fri = false;
        }


        $flight->save();


        return redirect()->route('domesticflightindex')
            ->with('success','Flight created successfully');
    }



    public function editAction($id)
    {
        $flight = Domflight::findOrFail($id);

//        $pcategory = Pcategory::lists('name','id');
//        $pkgCat = $tpackage->pcategory->lists('id','id')->toArray();


        $airlines = Domairline::lists('name','id');
        $airlines->prepend('Select Airline', '');

        $orgin = Domcity::lists('name','id');
        $orgin->prepend('Select Orgin City', '');

        $destination = Domcity::lists('name','id');
        $destination->prepend('Select Destination City', '');


        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.domflight.edit',compact('flight', 'airlines', 'orgin', 'destination'));
        }

        else{
            return redirect()->back();
        }

    }

    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'name' => 'required',
            'airline' => 'required',
            'orgin' => 'required',
            'destination' => 'required',
            'Deperture_time' => 'required',
            'Arrival_time' => 'required',
            'price' => 'required',
            'sat_economy_price' => 'required',
            'sat_business_price' => 'required',
            'sun_economy_price' => 'required',
            'sun_business_price' => 'required',
            'mon_economy_price' => 'required',
            'mon_business_price' => 'required',
            'tue_economy_price' => 'required',
            'tue_business_price' => 'required',
            'wed_economy_price' => 'required',
            'wed_business_price' => 'required',
            'thu_economy_price' => 'required',
            'thu_business_price' => 'required',
            'fri_economy_price' => 'required',
            'fri_business_price' => 'required'
        ]);

        $flight = Domflight::findOrFail($id);
        $flight->name = $request->input('name');
        $flight->Deperture_time = $request->input('Deperture_time');
        $flight->Arrival_time = $request->input('Arrival_time');
        $flight->domairline_id = $request->input('airline');
        $flight->orgincity_id = $request->input('orgin');
        $flight->destinationcity_id = $request->input('destination');

        $flight->price = $request->input('price');
        $flight->sat_economy_price = $request->input('sat_economy_price');
        $flight->sat_business_price = $request->input('sat_business_price');
        $flight->sun_economy_price = $request->input('sun_economy_price');
        $flight->sun_business_price = $request->input('sun_business_price');
        $flight->mon_economy_price = $request->input('mon_economy_price');
        $flight->mon_business_price = $request->input('mon_business_price');
        $flight->tue_economy_price = $request->input('tue_economy_price');
        $flight->tue_business_price = $request->input('tue_business_price');
        $flight->wed_economy_price = $request->input('wed_economy_price');
        $flight->wed_business_price = $request->input('wed_business_price');
        $flight->thu_economy_price = $request->input('thu_economy_price');
        $flight->thu_business_price = $request->input('thu_business_price');
        $flight->fri_economy_price = $request->input('fri_economy_price');
        $flight->fri_business_price = $request->input('fri_business_price');

        if($request->has('daily')){
            $flight->daily = true;
        }
        else{
            $flight->daily = false;
        }

        if($request->has('sat')){
            $flight->sat = true;
        }
        else{
            $flight->sat = false;
        }
        if($request->has('sun')){
            $flight->sun = true;
        }
        else{
            $flight->sun = false;
        }
        if($request->has('mon')){
            $flight->mon = true;
        }
        else{
            $flight->mon = false;
        }
        if($request->has('tue')){
            $flight->tue = true;
        }
        else{
            $flight->tue = false;
        }
        if($request->has('wed')){
            $flight->wed = true;
        }
        else{
            $flight->wed = false;
        }
        if($request->has('thu')){
            $flight->thu = true;
        }
        else{
            $flight->thu = false;
        }
        if($request->has('fri')){
            $flight->fri = true;
        }
        else{
            $flight->fri = false;
        }


        $flight->save();

        return redirect()->route('domesticflightindex')
            ->with('success','Flight update successfully');
    }


    public function destroyAction($id)
    {

        $flight = Domflight::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){

            $flight->delete();
            return redirect()->route('domesticflightindex')
                ->with('success','Flight deleted successfully');
        }

        else{
            return redirect()->back();
        }

    }

}
