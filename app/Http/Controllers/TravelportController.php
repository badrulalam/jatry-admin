<?php

namespace App\Http\Controllers;

use App\Cartitemair;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Response;
use App\Helpers\DummyData;
use App\Helpers\XML2Array;

class TravelportController extends Controller
{
    private $TARGETBRANCH = 'P105124';
    private $CREDENTIALS = 'Universal API/uAPI-197757298:mHs934HpZEZEsxwseMmTtYsSh';
    private $soapUrl = "https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/AirService";
    private $Provider = '1G';

    public function triggerValidateAction(Request $request)
    {
        $validate = $this->validateParams($request);
        if ($validate->fails()) {
            $errorMessage = [];
            $errorMessage['invalidAttributes'] = $validate->errors();
            return (new Response($errorMessage, 400))
                ->header('Content-Type', 'application/json; charset=utf-8');
        } else {
            $errorMessage = [];
            $errorMessage['invalidAttributes'] = $validate->errors();
            return (new Response($errorMessage, 202))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
    }

    public function triggerActionReal(Request $request)
    {
        $validate = $this->validateParams($request);
        if ($validate->fails()) {
            $errorMessage = [];
            $errorMessage['invalidAttributes'] = $validate->errors();
            return (new Response($errorMessage, 400))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
        $message = $this->getRequestXml($request);
        $content = $this->executeSoap($message);
        if (!$content) {
            return 'not found';
        }

        $encodedJson = $this->parseOutputTest($content);

        $array = XML2Array::createArray($content);

        $json_data = [];
        $json_data["FlightDetails"] = $this->getFlightDetails($array);
        $json_data["AirSegment"] = $this->getAirSegment($array);
        $json_data["FareInfo"] = $this->getFareInfo($array);
        $json_data["Route"] = $this->getRoute($array);
        $json_data["Brand"] = $this->getBrand($array);
        $json_data["AirPricingSolution"] = $this->getAirPricingSolution($array);
        $json_data["Parse"] = json_decode($encodedJson);

        if ($json_data) {
            return (new Response($json_data, 200))
                ->header('Content-Type', 'application/json; charset=utf-8');
        } else {
            return (new Response($json_data, 404))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
    }

    public function airAvailability(Request $request)
    {
//        $validate = $this->validateParams($request);
//        if ($validate->fails()) {
//            $errorMessage = [];
//            $errorMessage['invalidAttributes'] = $validate->errors();
//            return (new Response($errorMessage, 400))
//                ->header('Content-Type', 'application/json; charset=utf-8');
//        }
        $message = $this->gerAirAvailibilityXML($request);
        $content = $this->executeSoap($message);

        if (!$content) {
            return 'not found';
        }
return $content;
        $encodedJson = $this->parseOutputAirAvailability($content);
        return $encodedJson;

        $array = XML2Array::createArray($content);

        $json_data = [];
        $json_data["FlightDetails"] = $this->getFlightDetails($array);
        $json_data["AirSegment"] = $this->getAirSegment($array);
        $json_data["FareInfo"] = $this->getFareInfo($array);
        $json_data["Route"] = $this->getRoute($array);
        $json_data["Brand"] = $this->getBrand($array);
        $json_data["AirPricingSolution"] = $this->getAirPricingSolution($array);
        $json_data["Parse"] = json_decode($encodedJson);

        if ($json_data) {
            return (new Response($json_data, 200))
                ->header('Content-Type', 'application/json; charset=utf-8');
        } else {
            return (new Response($json_data, 404))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
    }

    public function uapiAction(Request $request)
    {


        $message = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        <soapenv:Header/>
        <soapenv:Body>
<AirPriceReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="5a21cac8-fd24-4bab-990c-7eb711a00503" AuthorizedBy="Travelport" TargetBranch="{$this->TARGETBRANCH}">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
  <AirItinerary>
    <AirSegment Key="Bdx6siBAAA/BNCoZHBAAAA==" AvailabilitySource="P" Equipment="319" AvailabilityDisplayType="Fare Shop/Optimal Shop" Group="0" Carrier="AI" FlightNumber="229" Origin="DAC" Destination="CCU" DepartureTime="2016-12-03T21:20:00.000+06:00" ArrivalTime="2016-12-03T21:50:00.000+05:30" FlightTime="60" Distance="146" ProviderCode="1G" ClassOfService="S" />
    <AirSegment Key="Bdx6siBAAA/BPCoZHBAAAA==" AvailabilitySource="P" Equipment="319" AvailabilityDisplayType="Fare Shop/Optimal Shop" Group="0" Carrier="AI" FlightNumber="763" Origin="CCU" Destination="DEL" DepartureTime="2016-12-04T07:00:00.000+05:30" ArrivalTime="2016-12-04T09:10:00.000+05:30" FlightTime="130" Distance="816" ProviderCode="1G" ClassOfService="Q" />
  </AirItinerary>
  <AirPricingModifiers InventoryRequestType="DirectAccess">
    <BrandModifiers ModifierType="FareFamilyDisplay" />
  </AirPricingModifiers>
  <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" BookingTravelerRef="U2I0WkV4TlJPMzduejY0eQ==" Key="U2I0WkV4TlJPMzduejY0eQ==" />
  <AirPricingCommand>
    <AirSegmentPricingModifiers AirSegmentRef="Bdx6siBAAA/BNCoZHBAAAA==" FareBasisCode="SOWBD">
      <PermittedBookingCodes>
        <BookingCode Code="S" />
      </PermittedBookingCodes>
    </AirSegmentPricingModifiers>
    <AirSegmentPricingModifiers AirSegmentRef="Bdx6siBAAA/BPCoZHBAAAA==" FareBasisCode="SOWBD">
      <PermittedBookingCodes>
        <BookingCode Code="Q" />
      </PermittedBookingCodes>
    </AirSegmentPricingModifiers>
  </AirPricingCommand>
  <FormOfPayment xmlns="http://www.travelport.com/schema/common_v36_0" Type="Credit" />
</AirPriceReq>
</soapenv:Body>
</soapenv:Envelope>
XML;

        $message = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        <soapenv:Header/>
        <soapenv:Body>
<SeatMapReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="28d5e924-dae3-4d19-816d-d19cc0f704cd" AuthorizedBy="Travelport" TargetBranch="{$this->TARGETBRANCH}" ReturnSeatPricing="true" ReturnBrandingInfo="true">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
  <AirSegment Key="zjf0siBAAA/BkkFAJBAAAA==" HostTokenRef="zjf0siBAAA/BpkFAJBAAAA==" Equipment="319" AvailabilityDisplayType="Fare Specific Fare Quote Unbooked" Group="0" Carrier="AI" FlightNumber="229" Origin="DAC" Destination="CCU" DepartureTime="2016-12-03T21:20:00.000+06:00" ArrivalTime="2016-12-03T21:50:00.000+05:30" FlightTime="60" TravelTime="60" Distance="146" ProviderCode="1G" ClassOfService="S">
    <CodeshareInfo OperatingCarrier="AI" />
  </AirSegment>
  <AirSegment Key="zjf0siBAAA/BmkFAJBAAAA==" HostTokenRef="zjf0siBAAA/BpkFAJBAAAA==" Equipment="319" AvailabilityDisplayType="Fare Specific Fare Quote Unbooked" Group="0" Carrier="AI" FlightNumber="763" Origin="CCU" Destination="DEL" DepartureTime="2016-12-04T07:00:00.000+05:30" ArrivalTime="2016-12-04T09:10:00.000+05:30" FlightTime="130" TravelTime="130" Distance="816" ProviderCode="1G" ClassOfService="Q">
    <CodeshareInfo OperatingCarrier="AI" />
  </AirSegment>
  <HostToken xmlns="http://www.travelport.com/schema/common_v36_0" Key="zjf0siBAAA/BpkFAJBAAAA==">GFB10101ADT00  01SOWBD                                 0200010002#GFB200010101NADTV3008BD01003000029904048Z#</HostToken>
  <HostToken xmlns="http://www.travelport.com/schema/common_v36_0" Key="zjf0siBAAA/BpkFAJBAAAA==">GFB10101ADT00  01SOWBD                                 0200010002#GFB200010101NADTV3008BD01003000029904048Z#</HostToken>
  <SearchTraveler Code="ADT" Age="40" Key="U2I0WkV4TlJPMzduejY0eQ==">
    <Name xmlns="http://www.travelport.com/schema/common_v36_0" Prefix="Mr" First="John" Last="Smith" />
  </SearchTraveler>
</SeatMapReq>
</soapenv:Body>
</soapenv:Envelope>
XML;

        $message = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        <soapenv:Header/>
        <soapenv:Body>
<AirCreateReservationReq xmlns="http://www.travelport.com/schema/universal_v36_0" TraceId="28d9bfa2-0925-4405-b278-412bb6fb48ab" AuthorizedBy="Travelport" TargetBranch="P105273" ProviderCode="1G" RetainReservation="Both">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="UAPI" />
  <BookingTraveler xmlns="http://www.travelport.com/schema/common_v36_0" Key="TmM2S3pMRGZWS3VZUVh3eg==" TravelerType="ADT" Age="40" DOB="1976-11-30" Gender="M" Nationality="US">
    <BookingTravelerName Prefix="Mr" First="John" Last="Smith" />
    <DeliveryInfo>
      <ShippingAddress Key="TmM2S3pMRGZWS3VZUVh3eg==">
        <Street>Via Augusta 59 5</Street>
        <City>Madrid</City>
        <State>IA</State>
        <PostalCode>50156</PostalCode>
        <Country>US</Country>
      </ShippingAddress>
    </DeliveryInfo>
    <PhoneNumber Location="DEN" CountryCode="1" AreaCode="303" Number="123456789" />
    <Email EmailID="johnsmith@travelportuniversalapidemo.com" />
    <Address>
      <AddressName>DemoSiteAddress</AddressName>
      <Street>Via Augusta 59 5</Street>
      <City>Madrid</City>
      <State>IA</State>
      <PostalCode>50156</PostalCode>
      <Country>US</Country>
    </Address>
  </BookingTraveler>
  <FormOfPayment xmlns="http://www.travelport.com/schema/common_v36_0" Type="Credit" Key="1">
    <CreditCard Type="VI" Number="4562568745126598" ExpDate="2018-01" Name="John Smith" CVV="595" Key="1">
      <BillingAddress Key="2b8dac13-1de1-4aa6-8518-f92f33739443">
        <AddressName>DemoSiteAddress</AddressName>
        <Street>Via Augusta 59 5</Street>
        <City>Madrid</City>
        <State>IA</State>
        <PostalCode>50156</PostalCode>
        <Country>US</Country>
      </BillingAddress>
    </CreditCard>
  </FormOfPayment>
  <AirPricingSolution xmlns="http://www.travelport.com/schema/air_v36_0" Key="LQsTsi8Q2BKAmGLUAAAAAA==" TotalPrice="GBP108.60" BasePrice="USD85.00" ApproximateTotalPrice="GBP108.60" ApproximateBasePrice="GBP66.00" EquivalentBasePrice="GBP66.00" Taxes="GBP42.60">
    <AirSegment Key="LQsTsi8Q2BKAiGLUAAAAAA==" OptionalServicesIndicator="false" AvailabilityDisplayType="Fare Specific Fare Quote Unbooked" Group="0" Carrier="AI" FlightNumber="229" Origin="DAC" Destination="CCU" DepartureTime="2016-12-03T21:20:00.000+06:00" ArrivalTime="2016-12-03T21:50:00.000+05:30" FlightTime="60" TravelTime="60" Distance="146" ProviderCode="1G" ClassOfService="E">
      <CodeshareInfo OperatingCarrier="AI" />
    </AirSegment>
    <AirSegment Key="LQsTsi8Q2BKAkGLUAAAAAA==" OptionalServicesIndicator="false" AvailabilityDisplayType="Fare Specific Fare Quote Unbooked" Group="0" Carrier="AI" FlightNumber="763" Origin="CCU" Destination="DEL" DepartureTime="2016-12-04T07:00:00.000+05:30" ArrivalTime="2016-12-04T09:10:00.000+05:30" FlightTime="130" TravelTime="130" Distance="816" ProviderCode="1G" ClassOfService="V">
      <CodeshareInfo OperatingCarrier="AI" />
    </AirSegment>
    <AirPricingInfo PricingMethod="Auto" Taxes="GBP42.60" Key="LQsTsi8Q2BKAoGLUAAAAAA==" TotalPrice="GBP108.60" BasePrice="USD85.00" ApproximateTotalPrice="GBP108.60" ApproximateBasePrice="GBP66.00" ProviderCode="1G">
      <FareInfo FareFamily="" PromotionalFare="false" DepartureDate="2016-12-03" Amount="GBP66.00" EffectiveDate="2016-11-30T04:54:00.000+00:00" Destination="DEL" Origin="DAC" PassengerTypeCode="ADT" FareBasis="EOWBD" Key="LQsTsi8Q2BKAuGLUAAAAAA==">
        <FareRuleKey FareInfoRef="LQsTsi8Q2BKAuGLUAAAAAA==" ProviderCode="1G">6UUVoSldxwi7E6jzDriQFMbKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG052UHcuWlOZd4TlqGWfocQbZVCulqMwbpNYRHrzWZukgthqPDydJZxOwAaD+cnSRZmHq/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTuAdcY0m0a4yk4nKb2G5mOF9PbNHV8pTH4MtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK/01BlpTwyo+r9GjARZYw7j/TROTkGAG0MevndHyll0GiOPZ/LoevdgzcF7TvWtaxDyk1kouXkh9S</FareRuleKey>
        <Brand Key="LQsTsi8Q2BKAuGLUAAAAAA==" />
      </FareInfo>
      <BookingInfo BookingCode="E" CabinClass="Economy" FareInfoRef="LQsTsi8Q2BKAuGLUAAAAAA==" SegmentRef="LQsTsi8Q2BKAiGLUAAAAAA==" HostTokenRef="LQsTsi8Q2BKAnGLUAAAAAA==" />
      <BookingInfo BookingCode="V" CabinClass="Economy" FareInfoRef="LQsTsi8Q2BKAuGLUAAAAAA==" SegmentRef="LQsTsi8Q2BKAkGLUAAAAAA==" HostTokenRef="LQsTsi8Q2BKAnGLUAAAAAA==" />
      <TaxInfo Key="LQsTsi8Q2BKApGLUAAAAAA==" Category="BD" Amount="GBP5.10" />
      <TaxInfo Key="LQsTsi8Q2BKAqGLUAAAAAA==" Category="E5" Amount="GBP0.80" />
      <TaxInfo Key="LQsTsi8Q2BKArGLUAAAAAA==" Category="OW" Amount="GBP5.10" />
      <TaxInfo Key="LQsTsi8Q2BKAsGLUAAAAAA==" Category="UT" Amount="GBP12.20" />
      <TaxInfo Key="LQsTsi8Q2BKAtGLUAAAAAA==" Category="YQ" Amount="GBP19.40" />
      <PassengerType Code="ADT" Age="40" BookingTravelerRef="TmM2S3pMRGZWS3VZUVh3eg==" />
    </AirPricingInfo>
    <HostToken xmlns="http://www.travelport.com/schema/common_v36_0" Key="LQsTsi8Q2BKAnGLUAAAAAA==">GFB10101ADT00  01EOWBD                                 0200010002#GFB200010101NADTV3008BD01004000029904048Z#</HostToken>
  </AirPricingSolution>
  <ActionStatus xmlns="http://www.travelport.com/schema/common_v36_0" Type="ACTIVE" TicketDate="T*" ProviderCode="1G" />
  <Payment xmlns="http://www.travelport.com/schema/common_v36_0" Key="2" Type="Itinerary" FormOfPaymentRef="1" Amount="GBP108.60" />
  <SpecificSeatAssignment xmlns="http://www.travelport.com/schema/air_v36_0" BookingTravelerRef="TmM2S3pMRGZWS3VZUVh3eg==" SegmentRef="LQsTsi8Q2BKAiGLUAAAAAA==" SeatId="6-D" />
  <SpecificSeatAssignment xmlns="http://www.travelport.com/schema/air_v36_0" BookingTravelerRef="TmM2S3pMRGZWS3VZUVh3eg==" SegmentRef="LQsTsi8Q2BKAkGLUAAAAAA==" SeatId="18-A" />
</AirCreateReservationReq>
</soapenv:Body>
</soapenv:Envelope>
XML;
//        $message = <<<XML
//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
//        <soapenv:Header/>
//        <soapenv:Body>
//<UniversalRecordRetrieveReq xmlns="http://www.travelport.com/schema/universal_v36_0" TraceId="ed29c137-d33c-441e-bc15-3ee5cc855b10" AuthorizedBy="Travelport" TargetBranch="{$this->TARGETBRANCH}">
//  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
//  <UniversalRecordLocatorCode>P5VGYK</UniversalRecordLocatorCode>
//</UniversalRecordRetrieveReq>
//</soapenv:Body>
//</soapenv:Envelope>
//XML;

        $content = $this->executeSoap($message);
        return $content;
    }

    public function gerAirAvailibilityXML( $request)
    {

        $message = <<<XML
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<soapenv:Header/>
<soapenv:Body>
<air:AvailabilitySearchReq xmlns:air="http://www.travelport.com/schema/air_v39_0" AuthorizedBy="user" TargetBranch="{$this->TARGETBRANCH}" TraceId="trace" >
<com:BillingPointOfSaleInfo xmlns:com="http://www.travelport.com/schema/common_v39_0" OriginApplication="UAPI"/>
<air:SearchAirLeg>
<air:SearchOrigin>
<com:Airport xmlns:com="http://www.travelport.com/schema/common_v39_0" Code="{$request->input('origin')}"/>
</air:SearchOrigin>
<air:SearchDestination>
<com:Airport xmlns:com="http://www.travelport.com/schema/common_v39_0" Code="{$request->input('destination')}"/>
</air:SearchDestination>
<air:SearchDepTime PreferredTime="{$this->getFormatedDate($request->input('departureDate'))}"/>
</air:SearchAirLeg>
<air:AirSearchModifiers>
<air:PreferredProviders>
<com:Provider xmlns:com="http://www.travelport.com/schema/common_v39_0" Code="1G"/>
</air:PreferredProviders>
</air:AirSearchModifiers>

</air:AvailabilitySearchReq>
</soapenv:Body>
</soapenv:Envelope>
XML;
//        <air:PreferredCarriers>
//      <com:Carrier xmlns:com="http://www.travelport.com/schema/common_v39_0" Code="EY" />
//      <com:Carrier xmlns:com="http://www.travelport.com/schema/common_v39_0" Code="9W" />
//    </air:PreferredCarriers>
//</air:AirSearchModifiers>
        return $message;
    }

    public function getSeatMap()
    {
        $message = <<<XML
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        <soapenv:Header/>
        <soapenv:Body>
<SeatMapReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="c270bfff-3e9e-41b0-99ec-9c57442055ca" AuthorizedBy="Travelport" TargetBranch="{$this->TARGETBRANCH}" ReturnSeatPricing="true" ReturnBrandingInfo="true">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
  <AirSegment Key="3SsSQhBAAA/B/IHc2EAAAA==" HostTokenRef="3SsSQhBAAA/BEJHc2EAAAA==" Equipment="319" AvailabilityDisplayType="Fare Specific Fare Quote Unbooked" Group="0" Carrier="AI" FlightNumber="229" Origin="DAC" Destination="CCU" DepartureTime="2016-11-25T21:20:00.000+06:00" ArrivalTime="2016-11-25T21:50:00.000+05:30" FlightTime="60" TravelTime="60" Distance="146" ProviderCode="1G" ClassOfService="E">
    <CodeshareInfo OperatingCarrier="AI" />
  </AirSegment>
  <AirSegment Key="3SsSQhBAAA/BBJHc2EAAAA==" HostTokenRef="3SsSQhBAAA/BEJHc2EAAAA==" Equipment="321" AvailabilityDisplayType="Fare Specific Fare Quote Unbooked" Group="0" Carrier="AI" FlightNumber="763" Origin="CCU" Destination="DEL" DepartureTime="2016-11-26T07:00:00.000+05:30" ArrivalTime="2016-11-26T09:10:00.000+05:30" FlightTime="130" TravelTime="130" Distance="816" ProviderCode="1G" ClassOfService="Q">
    <CodeshareInfo OperatingCarrier="AI" />
  </AirSegment>
  <HostToken xmlns="http://www.travelport.com/schema/common_v36_0" Key="3SsSQhBAAA/BEJHc2EAAAA==">GFB10101ADT00  01EOWBD                                 0200010002#GFB200010101NADTV3008BD01004000029904048Z#</HostToken>
  <HostToken xmlns="http://www.travelport.com/schema/common_v36_0" Key="3SsSQhBAAA/BEJHc2EAAAA==">GFB10101ADT00  01EOWBD                                 0200010002#GFB200010101NADTV3008BD01004000029904048Z#</HostToken>
  <SearchTraveler Code="ADT" Age="40" Key="QzBtS1FoSHlzOFIxR0RrMg==">
    <Name xmlns="http://www.travelport.com/schema/common_v36_0" Prefix="Mr" First="John" Last="Smith" />
  </SearchTraveler>
</SeatMapReq>
</soapenv:Body>
</soapenv:Envelope>
XML;

        $content = $this->executeSoap($message);
        return $content;
    }

    public function getAirPriceReq()
    {
        $message = <<<XML
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        <soapenv:Header/>
        <soapenv:Body>
<AirPriceReq xmlns="http://www.travelport.com/schema/air_v36_0" TraceId="8584f6e1-f001-4800-9f80-3d7740b35458" AuthorizedBy="Travelport" TargetBranch="{$this->TARGETBRANCH}">
  <BillingPointOfSaleInfo xmlns="http://www.travelport.com/schema/common_v36_0" OriginApplication="uAPI" />
  <AirItinerary>
    <AirSegment Key="bH6aQhkJ0BKAriJb1EAAAA==" AvailabilitySource="S" Equipment="319" AvailabilityDisplayType="Fare Shop/Optimal Shop" Group="0" Carrier="AI" FlightNumber="229" Origin="DAC" Destination="CCU" DepartureTime="2016-11-25T21:20:00.000+06:00" ArrivalTime="2016-11-25T21:50:00.000+05:30" FlightTime="60" Distance="146" ProviderCode="1G" ClassOfService="E" />
    <AirSegment Key="bH6aQhkJ0BKAtiJb1EAAAA==" AvailabilitySource="S" Equipment="321" AvailabilityDisplayType="Fare Shop/Optimal Shop" Group="0" Carrier="AI" FlightNumber="763" Origin="CCU" Destination="DEL" DepartureTime="2016-11-26T07:00:00.000+05:30" ArrivalTime="2016-11-26T09:10:00.000+05:30" FlightTime="130" Distance="816" ProviderCode="1G" ClassOfService="Q" />
  </AirItinerary>
  <AirPricingModifiers InventoryRequestType="DirectAccess">
    <BrandModifiers ModifierType="FareFamilyDisplay" />
  </AirPricingModifiers>
  <SearchPassenger xmlns="http://www.travelport.com/schema/common_v36_0" Code="ADT" Age="40" BookingTravelerRef="QzBtS1FoSHlzOFIxR0RrMg==" Key="QzBtS1FoSHlzOFIxR0RrMg==" />
  <AirPricingCommand>
    <AirSegmentPricingModifiers AirSegmentRef="bH6aQhkJ0BKAriJb1EAAAA==" FareBasisCode="EOWBD">
      <PermittedBookingCodes>
        <BookingCode Code="E" />
      </PermittedBookingCodes>
    </AirSegmentPricingModifiers>
    <AirSegmentPricingModifiers AirSegmentRef="bH6aQhkJ0BKAtiJb1EAAAA==" FareBasisCode="EOWBD">
      <PermittedBookingCodes>
        <BookingCode Code="Q" />
      </PermittedBookingCodes>
    </AirSegmentPricingModifiers>
  </AirPricingCommand>
  <FormOfPayment xmlns="http://www.travelport.com/schema/common_v36_0" Type="Credit" />
</AirPriceReq>
</soapenv:Body>
</soapenv:Envelope>
XML;

        $content = $this->executeSoap($message);
        return $content;
    }

    public function triggerAction(Request $request)
    {
        $validate = $this->validateParams($request);
        if ($validate->fails()) {
            $errorMessage = [];
            $errorMessage['invalidAttributes'] = $validate->errors();
            return (new Response($errorMessage, 400))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
        $content = DummyData::getDummy($request);
        if (!$content) {
            return 'not found';
        }

        $encodedJson = $this->parseOutputTest($content);

        $array = XML2Array::createArray($content);

        $json_data = [];
        $json_data["FlightDetails"] = $this->getFlightDetails($array);
        $json_data["AirSegment"] = $this->getAirSegment($array);
        $json_data["FareInfo"] = $this->getFareInfo($array);
        $json_data["Route"] = $this->getRoute($array);
        $json_data["Brand"] = $this->getBrand($array);
        $json_data["AirPricingSolution"] = $this->getAirPricingSolution($array);
        $json_data["Parse"] = json_decode($encodedJson);

        if ($json_data) {
            return (new Response($json_data, 200))
                ->header('Content-Type', 'application/json; charset=utf-8');
        } else {
            return (new Response($json_data, 404))
                ->header('Content-Type', 'application/json; charset=utf-8');
        }
    }

    private function getAvailableTicket($rows)
    {
//        dd(gettype($rows));
        $ticketExist = Cartitemair::lists('ticket_key')->toArray();
        for ($i = 0; $i < count($rows); $i++) {
            if (in_array($rows[$i]->Key, $ticketExist)) {
                array_splice($rows, $i, 1);
//                print_r($rows[$i]->Key);
            }
        }
        return $rows;
    }

    private function validateParams($request)
    {
        $validator = Validator::make($request->all(), [
            'tripType' => 'required|string|size:1|in:O,R,M',
            'noOfSegments' => 'required|integer|min:1|max:5',
            'adults' => 'required|integer|min:1|max:9',
            'childs' => 'required|integer|min:0|max:9',
            'infant' => 'required|integer|min:0|max:9',
            'class' => 'required|string',
            'origin' => 'required_if:tripType,O,R|string|size:3',
            'destination' => 'required_if:tripType,O,R|string|size:3',
            'departureDate' => 'required_if:tripType,O,R|date|after:yesterday',
            'arrivalDate' => 'required_if:tripType,R|date|after:departureDate'


        ]);
        $validator->sometimes(['origin_1', 'destination_1'], 'required|string|size:3', function ($input) {
            return ($input->tripType == 'M' && 1 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes(['origin_2', 'destination_2'], 'required|string|size:3', function ($input) {
            return ($input->tripType == 'M' && 2 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes(['origin_3', 'destination_3'], 'required|string|size:3', function ($input) {
            return ($input->tripType == 'M' && 3 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes(['origin_4', 'destination_4'], 'required|string|size:3', function ($input) {
            return ($input->tripType == 'M' && 4 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes(['origin_5', 'destination_5'], 'required|string|size:3', function ($input) {
            return ($input->tripType == 'M' && 5 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });

        $validator->sometimes('departureDate_1', 'required|date|after:yesterday', function ($input) {
            return ($input->tripType == 'M' && 1 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes('departureDate_2', 'required|date|after:departureDate_1', function ($input) {
            return ($input->tripType == 'M' && 2 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes('departureDate_3', 'required|date|after:departureDate_2', function ($input) {
            return ($input->tripType == 'M' && 3 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes('departureDate_4', 'required|date|after:departureDate_3', function ($input) {
            return ($input->tripType == 'M' && 4 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });
        $validator->sometimes('departureDate_5', 'required|date|after:departureDate_3', function ($input) {
            return ($input->tripType == 'M' && 5 <= $input->noOfSegments && $input->noOfSegments <= 5);
        });

        return $validator;

    }

    private function getRequestXml($request)
    {
        $xml = "";
        $xml = $xml . "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">";
        $xml = $xml . "<soapenv:Header/>";
        $xml = $xml . "<soapenv:Body>";
        $xml = $xml . "<LowFareSearchReq xmlns=\"http://www.travelport.com/schema/air_v36_0\" TraceId=\"9b3b5b52-99a9-4a50-ae52-6c0db3d46d7d\"  SolutionResult=\"true\" TargetBranch=\"{$this->TARGETBRANCH}\" ReturnUpsellFare=\"true\">";
        $xml = $xml . "<BillingPointOfSaleInfo xmlns=\"http://www.travelport.com/schema/common_v36_0\" OriginApplication=\"uAPI\" />";
        if ($request->input('tripType') === 'O') {
            $xml = $xml . "<SearchAirLeg>";
            $xml = $xml . "<SearchOrigin>";
            $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('origin')}\" PreferCity=\"true\" />";
            $xml = $xml . "</SearchOrigin>";
            $xml = $xml . "<SearchDestination>";
            $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('destination')}\" PreferCity=\"true\" />";
            $xml = $xml . "</SearchDestination>";
            if ($request->input('flexible') == true) {
                $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('departureDate'))}\" >";
                $xml = $xml . "<SearchExtraDays xmlns=\"http://www.travelport.com/schema/common_v36_0\" DaysBefore=\"3\" DaysAfter=\"3\" />";
                $xml = $xml . "</SearchDepTime>";
            } else {
                $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('departureDate'))}\" />";
            }

            $xml = $xml . "</SearchAirLeg>";
        }
        if ($request->input('tripType') === 'R') {
            $xml = $xml . "<SearchAirLeg>";
            $xml = $xml . "<SearchOrigin>";
            $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('origin')}\" PreferCity=\"true\" />";
            $xml = $xml . "</SearchOrigin>";
            $xml = $xml . "<SearchDestination>";
            $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('destination')}\" PreferCity=\"true\" />";
            $xml = $xml . "</SearchDestination>";
            if ($request->input('flexible') == true) {
                $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('departureDate'))}\" >";
                $xml = $xml . "<SearchExtraDays xmlns=\"http://www.travelport.com/schema/common_v36_0\" DaysBefore=\"3\" DaysAfter=\"3\" />";
                $xml = $xml . "</SearchDepTime>";
            } else {
                $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('departureDate'))}\" />";
            }


            $xml = $xml . "</SearchAirLeg>";

            $xml = $xml . "<SearchAirLeg>";
            $xml = $xml . "<SearchOrigin>";
            $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('destination')}\" PreferCity=\"true\" />";
            $xml = $xml . "</SearchOrigin>";
            $xml = $xml . "<SearchDestination>";
            $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('origin')}\" PreferCity=\"true\" />";
            $xml = $xml . "</SearchDestination>";
            if ($request->input('flexible') == true) {
                $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('arrivalDate'))}\" >";
                $xml = $xml . "<SearchExtraDays xmlns=\"http://www.travelport.com/schema/common_v36_0\" DaysBefore=\"3\" DaysAfter=\"3\" />";
                $xml = $xml . "</SearchDepTime>";
            } else {
                $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('arrivalDate'))}\" />";
            }

            $xml = $xml . "</SearchAirLeg>";
        }

        if ($request->input('tripType') === 'M') {
            for ($i = 1; $i <= $request->input('noOfSegments'); $i++) {
                $xml = $xml . "<SearchAirLeg>";
                $xml = $xml . "<SearchOrigin>";
                $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('origin_'.$i)}\" PreferCity=\"true\" />";
                $xml = $xml . "</SearchOrigin>";
                $xml = $xml . "<SearchDestination>";
                $xml = $xml . "<CityOrAirport xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$request->input('destination_'.$i)}\" PreferCity=\"true\" />";
                $xml = $xml . "</SearchDestination>";
                if ($request->input('flexible') == true) {
                    $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('departureDate_'.$i))}\" >";
                    $xml = $xml . "<SearchExtraDays xmlns=\"http://www.travelport.com/schema/common_v36_0\" DaysBefore=\"3\" DaysAfter=\"3\" />";
                    $xml = $xml . "</SearchDepTime>";
                } else {
                    $xml = $xml . "<SearchDepTime PreferredTime=\"{$this->getFormatedDate($request->input('departureDate_'.$i))}\" />";
                }

                $xml = $xml . "</SearchAirLeg>";
            }
        }


        $xml = $xml . "<AirSearchModifiers>";
        $xml = $xml . "<PreferredProviders>";
        $xml = $xml . "<Provider xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"{$this->Provider}\" />";
        $xml = $xml . "</PreferredProviders>";
        $xml = $xml . "</AirSearchModifiers>";

        for ($i = 0; $i < $request->input('adults'); $i++) {
            $xml = $xml . "<SearchPassenger xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"ADT\" Age=\"40\" DOB=\"1976-08-29\" />";
        }
        for ($i = 0; $i < $request->input('childs'); $i++) {
            $xml = $xml . "<SearchPassenger xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"CNN\" Age=\"4\" DOB=\"2012-10-30\" />";
        }
        for ($i = 0; $i < $request->input('infant'); $i++) {
            $xml = $xml . "<SearchPassenger xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"INF\" Age=\"0\" DOB=\"2016-10-30\" />";
        }


        $xml = $xml . "<AirPricingModifiers FaresIndicator=\"PublicFaresOnly\" CurrencyType=\"BDT\">";
        $xml = $xml . "<AccountCodes>";
        $xml = $xml . "<AccountCode xmlns=\"http://www.travelport.com/schema/common_v36_0\" Code=\"-\" />";
        $xml = $xml . "</AccountCodes>";
        $xml = $xml . "</AirPricingModifiers>";
        $xml = $xml . "</LowFareSearchReq>";
        $xml = $xml . "</soapenv:Body>";
        $xml = $xml . "</soapenv:Envelope>";

        return $xml;
    }

    private function executeSoap($message)
    {
        $auth = base64_encode($this->CREDENTIALS);
        $soap_do = curl_init($this->soapUrl);
        $header = array(
            "Content-Type: text/xml;charset=UTF-8",
            "Accept: gzip,deflate",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"\"",
            "Authorization: Basic $auth",
            "Content-length: " . strlen($message),
        );

        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 30);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $message);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        $return = curl_exec($soap_do);
        curl_close($soap_do);
        return $return;
    }

    private function parseOutput($content)
    {    //parse the Search response to get values to use in detail request
        $LowFareSearchRsp = $content; //use this if response is not saved anywhere else use above variable
        //echo $LowFareSearchRsp;
        $xml = \simplexml_load_string("$LowFareSearchRsp", null, null, 'SOAP', true);

        if ($xml) {
            // echo "Processing! Please wait!";
        } else {
//            \trigger_error("Encoding Error!", E_USER_ERROR);
            return json_encode($content);
        }

        $Results = $xml->children('SOAP', true);

        foreach ($Results->children('SOAP', true) as $fault) {
            if (strcmp($fault->getName(), 'Fault') == 0) {
                $faultObj = [];
                foreach ($fault->children() as $key => $value) {
                    $faultObj[$key] = $value->__toString();
                }
//                return $content;
                return ($faultObj);
            }
        }


        $count = 0;

        $jsonObj = [];
        foreach ($Results->children('air', true) as $lowFare) {
            foreach ($lowFare->children('air', true) as $airPriceSol) {
                if (strcmp($airPriceSol->getName(), 'AirPricingSolution') == 0) {
                    $count = $count + 1;
                    $airSolution = [];
                    $airSolution['Summary'] = [];
                    $airSolution['Journey']['Routes'] = [];
                    $airSolution['Price'] = [];


                    foreach ($airPriceSol->children('air', true) as $journey) {
                        if (strcmp($journey->getName(), 'Journey') == 0) {

                            foreach ($airPriceSol->attributes() as $x => $y) {
                                $airSolution['Summary'][$x] = $y->__toString();
                            }

                            $journeyList = [];
                            $journeyList['Options'] = [];
                            foreach ($journey->children('air', true) as $segmentRef) {
                                if (strcmp($segmentRef->getName(), 'AirSegmentRef') == 0) {
                                    $AirSegmentRef = [];
                                    foreach ($segmentRef->attributes() as $a => $b) {
                                        $segment = $this->ListAirSegments($b, $lowFare);
                                        foreach ($segment->attributes() as $c => $d) {
                                            $AirSegmentRef[$c] = $d->__toString();
                                            if (strcmp($c, "Origin") == 0) {

                                                $AirSegmentRef['Origin'] = $d->__toString();
                                            }
                                            if (strcmp($c, "Destination") == 0) {

                                                $AirSegmentRef['Destination'] = $d->__toString();
                                            }
                                            if (strcmp($c, "Carrier") == 0) {

                                                $AirSegmentRef['Carrier'] = $d->__toString();
                                            }
                                            if (strcmp($c, "FlightNumber") == 0) {

                                                $AirSegmentRef['FlightNumber'] = $d->__toString();
                                            }
                                            if (strcmp($c, "DepartureTime") == 0) {

                                                $AirSegmentRef['DepartureTime'] = $d->__toString();
                                            }
                                            if (strcmp($c, "ArrivalTime") == 0) {

                                                $AirSegmentRef['ArrivalTime'] = $d->__toString();
                                            }
                                            if (strcmp($c, "FlightTime") == 0) {

                                                $AirSegmentRef['FlightTime'] = $d->__toString();
                                            }
                                        }

                                    }
                                    array_push($journeyList['Options'], $AirSegmentRef);


                                }
                            }

                            $journeyList['Duration'] = date_diff(
                                date_create(end($journeyList['Options'])['ArrivalTime']),
                                date_create(reset($journeyList['Options'])['DepartureTime'])
                            );
                            $journeyList['ArrivalTime'] = end($journeyList['Options'])['ArrivalTime'];
                            $journeyList['DepartureTime'] = reset($journeyList['Options'])['DepartureTime'];
                            $journeyList['Origin'] = reset($journeyList['Options'])['Origin'];
                            $journeyList['Destination'] = end($journeyList['Options'])['Destination'];

                            $journeyList['Stops'] = count($journeyList['Options']);
                            $journeyList['ViaStops'] = $this->getViaStops($journeyList['Options']);
                            array_push($airSolution['Journey']['Routes'], $journeyList);

                        }
                    }

                    foreach ($airPriceSol->children('air', true) as $priceInfo) {
                        if (strcmp($priceInfo->getName(), 'AirPricingInfo') == 0) {


                            foreach ($priceInfo->attributes() as $e => $f) {
                                if (strcmp($e, "ApproximateBasePrice") == 0) {

                                    $airSolution['Price']['ApproximateBasePrice'] = $f->__toString();
                                }
                                if (strcmp($e, "ApproximateTaxes") == 0) {

                                    $airSolution['Price']['ApproximateTaxes'] = $f->__toString();
                                }
                                if (strcmp($e, "ApproximateTotalPrice") == 0) {

                                    $airSolution['Price']['ApproximateTotalPrice'] = $f->__toString();
                                }
                                if (strcmp($e, "BasePrice") == 0) {

                                    $airSolution['Price']['BasePrice'] = $f->__toString();
                                }
                                if (strcmp($e, "Taxes") == 0) {

                                    $airSolution['Price']['Taxes'] = $f->__toString();
                                }
                                if (strcmp($e, "TotalPrice") == 0) {

                                    $airSolution['Price']['TotalPrice'] = $f->__toString();
                                }
                                if (strcmp($e, "PlatingCarrier") == 0) {

                                    $airSolution['PlatingCarrier'] = $f->__toString();
                                }
                                if (strcmp($e, "Refundable") == 0) {

                                    $airSolution['Refundable'] = $f->__toString();
                                }

                            }
                            foreach ($priceInfo->children('air', true) as $bookingInfo) {
                                if (strcmp($bookingInfo->getName(), 'BookingInfo') == 0) {
                                    foreach ($bookingInfo->attributes() as $e => $f) {
                                        $airSolution[$e] = $f->__toString();
                                        if (strcmp($e, "CabinClass") == 0) {
                                            $airSolution['CabinClass'] = $f->__toString();
                                        }
                                    }
                                }
                            }

                        }
                    }

                    array_push($jsonObj, $airSolution);

                }

            }
        }

        return json_encode($jsonObj);
    }

    private function parseOutputTest($content)
    {    //parse the Search response to get values to use in detail request
        $LowFareSearchRsp = $content; //use this if response is not saved anywhere else use above variable
        //echo $LowFareSearchRsp;
        $xml = \simplexml_load_string("$LowFareSearchRsp", null, null, 'SOAP', true);

        if ($xml) {
            // echo "Processing! Please wait!";
        } else {
//            \trigger_error("Encoding Error!", E_USER_ERROR);
            return json_encode($content);
        }

        $Results = $xml->children('SOAP', true);

        foreach ($Results->children('SOAP', true) as $fault) {
            if (strcmp($fault->getName(), 'Fault') == 0) {
                $faultObj = [];
                foreach ($fault->children() as $key => $value) {
                    $faultObj[$key] = $value->__toString();
                }
//                return $content;
                return json_encode($faultObj);
            }
        }


        $jsonObj = [];
        foreach ($Results->children('air', true) as $lowFare) {
            foreach ($lowFare->children('air', true) as $airPriceSol) { //print_r($airPriceSol->asXML());
                if (strcmp($airPriceSol->getName(), 'AirPricingSolution') == 0) {

                    $airSolution = [];
                    $airSolution['Summary'] = [];
                    $airSolution['Journey']['Routes'] = [];
                    $airSolution['Price'] = [];

                    /*init attributes*/
                    foreach ($airPriceSol->attributes() as $x => $y) {
                        $airSolution[$x] = $y->__toString();
                    }


                    foreach ($airPriceSol->children('air', true) as $journey) {
                        if (strcmp($journey->getName(), 'Journey') == 0) {

                            /*init attributes*/
                            foreach ($journey->attributes() as $x => $y) {
                                $airSolution['Journey'][$x] = $y->__toString();
                            }

                            $segmentList = $this->getAllAirSegments($journey, $lowFare);
                            array_push($airSolution['Journey']['Routes'], $segmentList);


                        }
                    }

                    foreach ($airPriceSol->children('air', true) as $priceInfo) {
                        if (strcmp($priceInfo->getName(), 'AirPricingInfo') == 0) {


                            foreach ($priceInfo->attributes() as $e => $f) {
                                if (strcmp($e, "ApproximateBasePrice") == 0) {

                                    $airSolution['Price']['ApproximateBasePrice'] = $f->__toString();
                                }
                                if (strcmp($e, "ApproximateTaxes") == 0) {

                                    $airSolution['Price']['ApproximateTaxes'] = $f->__toString();
                                }
                                if (strcmp($e, "ApproximateTotalPrice") == 0) {

                                    $airSolution['Price']['ApproximateTotalPrice'] = $f->__toString();
                                }
                                if (strcmp($e, "BasePrice") == 0) {

                                    $airSolution['Price']['BasePrice'] = $f->__toString();
                                }
                                if (strcmp($e, "Taxes") == 0) {

                                    $airSolution['Price']['Taxes'] = $f->__toString();
                                }
                                if (strcmp($e, "TotalPrice") == 0) {

                                    $airSolution['Price']['TotalPrice'] = $f->__toString();
                                }
                                if (strcmp($e, "PlatingCarrier") == 0) {

                                    $airSolution['PlatingCarrier'] = $f->__toString();
                                }
                                if (strcmp($e, "Refundable") == 0) {

                                    $airSolution['Refundable'] = $f->__toString();
                                }

                            }


                            $airSolution['Price']['BookingInfo'] = [];

                            foreach ($priceInfo->children('air', true) as $bookingInfo) {
                                if (strcmp($bookingInfo->getName(), 'BookingInfo') == 0) {
                                    $bInfo = [];

                                    /*init bookinginfo attributes*/
                                    foreach ($bookingInfo->attributes() as $e => $f) {
                                        $bInfo[$e] = $f->__toString();
                                        if (strcmp($e, "FareInfoRef") == 0) {

                                            $fareInfo = $this->ListFareInfo($f, $lowFare);

                                            $bInfo['FareInfo'] = $this->FormatListFareInfo($fareInfo, $lowFare);
//                                            $airSolution['CabinClass'] = $f->__toString();
                                        }
                                    }
                                    array_push($airSolution['Price']['BookingInfo'], $bInfo);

                                }
                            }

                            $airSolution['Price']['ChangePenalty'] = false;
                            $airSolution['Price']['CancelPenalty'] = false;
                            if (isset($priceInfo->ChangePenalty->Amount)) {
                                $airSolution['Price']['ChangePenalty'] = $priceInfo->ChangePenalty->Amount->__toString();
                            }
                            if (isset($priceInfo->CancelPenalty->Amount)) {
                                $airSolution['Price']['CancelPenalty'] = $priceInfo->CancelPenalty->Amount->__toString();
                            }
                        }
                    }

                    array_push($jsonObj, $airSolution);
//                    break;
                }

            }
        }

        return json_encode($jsonObj);
    }



    private function parseOutputAirAvailability($content)
    {    //parse the Search response to get values to use in detail request
        $LowFareSearchRsp = $content; //use this if response is not saved anywhere else use above variable
        //echo $LowFareSearchRsp;
        $xml = \simplexml_load_string("$LowFareSearchRsp", null, null, 'SOAP', true);

        if ($xml) {
            // echo "Processing! Please wait!";
        } else {
//            \trigger_error("Encoding Error!", E_USER_ERROR);
            return json_encode($content);
        }

        $Results = $xml->children('SOAP', true);

        foreach ($Results->children('SOAP', true) as $fault) {
            if (strcmp($fault->getName(), 'Fault') == 0) {
                $faultObj = [];
                foreach ($fault->children() as $key => $value) {
                    $faultObj[$key] = $value->__toString();
                }
//                return $content;
                return json_encode($faultObj);
            }
        }


        $jsonObj = [];
        foreach ($Results->children('air', true) as $lowFare) {
            foreach ($lowFare->children('air', true) as $airPriceSol) { //print_r($airPriceSol->asXML());
                if (strcmp($airPriceSol->getName(), 'AirSegmentList') == 0) {

                    $airSolution = [];
                    $airSolution['Summary'] = [];
                    $airSolution['Journey']['Routes'] = [];
                    $airSolution['Price'] = [];

                    /*init attributes*/
                    foreach ($airPriceSol->attributes() as $x => $y) {
                        $airSolution[$x] = $y->__toString();
                    }


                    foreach ($airPriceSol->children('air', true) as $journey) {
                        if (strcmp($journey->getName(), 'Journey') == 0) {

                            /*init attributes*/
                            foreach ($journey->attributes() as $x => $y) {
                                $airSolution['Journey'][$x] = $y->__toString();
                            }

                            $segmentList = $this->getAllAirSegments($journey, $lowFare);
                            array_push($airSolution['Journey']['Routes'], $segmentList);


                        }
                    }

                    foreach ($airPriceSol->children('air', true) as $priceInfo) {
                        if (strcmp($priceInfo->getName(), 'AirPricingInfo') == 0) {


                            foreach ($priceInfo->attributes() as $e => $f) {
                                if (strcmp($e, "ApproximateBasePrice") == 0) {

                                    $airSolution['Price']['ApproximateBasePrice'] = $f->__toString();
                                }
                                if (strcmp($e, "ApproximateTaxes") == 0) {

                                    $airSolution['Price']['ApproximateTaxes'] = $f->__toString();
                                }
                                if (strcmp($e, "ApproximateTotalPrice") == 0) {

                                    $airSolution['Price']['ApproximateTotalPrice'] = $f->__toString();
                                }
                                if (strcmp($e, "BasePrice") == 0) {

                                    $airSolution['Price']['BasePrice'] = $f->__toString();
                                }
                                if (strcmp($e, "Taxes") == 0) {

                                    $airSolution['Price']['Taxes'] = $f->__toString();
                                }
                                if (strcmp($e, "TotalPrice") == 0) {

                                    $airSolution['Price']['TotalPrice'] = $f->__toString();
                                }
                                if (strcmp($e, "PlatingCarrier") == 0) {

                                    $airSolution['PlatingCarrier'] = $f->__toString();
                                }
                                if (strcmp($e, "Refundable") == 0) {

                                    $airSolution['Refundable'] = $f->__toString();
                                }

                            }


                            $airSolution['Price']['BookingInfo'] = [];

                            foreach ($priceInfo->children('air', true) as $bookingInfo) {
                                if (strcmp($bookingInfo->getName(), 'BookingInfo') == 0) {
                                    $bInfo = [];

                                    /*init bookinginfo attributes*/
                                    foreach ($bookingInfo->attributes() as $e => $f) {
                                        $bInfo[$e] = $f->__toString();
                                        if (strcmp($e, "FareInfoRef") == 0) {

                                            $fareInfo = $this->ListFareInfo($f, $lowFare);

                                            $bInfo['FareInfo'] = $this->FormatListFareInfo($fareInfo, $lowFare);
//                                            $airSolution['CabinClass'] = $f->__toString();
                                        }
                                    }
                                    array_push($airSolution['Price']['BookingInfo'], $bInfo);

                                }
                            }

                            $airSolution['Price']['ChangePenalty'] = false;
                            $airSolution['Price']['CancelPenalty'] = false;
                            if (isset($priceInfo->ChangePenalty->Amount)) {
                                $airSolution['Price']['ChangePenalty'] = $priceInfo->ChangePenalty->Amount->__toString();
                            }
                            if (isset($priceInfo->CancelPenalty->Amount)) {
                                $airSolution['Price']['CancelPenalty'] = $priceInfo->CancelPenalty->Amount->__toString();
                            }
                        }
                    }

                    array_push($jsonObj, $airSolution);
//                    break;
                }

            }
        }

        return json_encode($jsonObj);
    }

    private function ListAirSegments($key, $lowFare)
    {
        foreach ($lowFare->children('air', true) as $airSegmentList) {
            if (strcmp($airSegmentList->getName(), 'AirSegmentList') == 0) {
                foreach ($airSegmentList->children('air', true) as $airSegment) {
                    if (strcmp($airSegment->getName(), 'AirSegment') == 0) {
                        foreach ($airSegment->attributes() as $a => $b) {
                            if (strcmp($a, 'Key') == 0) {
                                if (strcmp($b, $key) == 0) {
                                    return $airSegment;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function ListFlightDetails($key, $lowFare)
    {
        foreach ($lowFare->children('air', true) as $airSegmentList) {
            if (strcmp($airSegmentList->getName(), 'FlightDetailsList') == 0) {
                foreach ($airSegmentList->children('air', true) as $airSegment) {
                    if (strcmp($airSegment->getName(), 'FlightDetails') == 0) {
                        foreach ($airSegment->attributes() as $a => $b) {
                            if (strcmp($a, 'Key') == 0) {
                                if (strcmp($b, $key) == 0) {
                                    return $airSegment;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function ListFareInfo($key, $lowFare)
    {
        foreach ($lowFare->children('air', true) as $airSegmentList) {
            if (strcmp($airSegmentList->getName(), 'FareInfoList') == 0) {
                foreach ($airSegmentList->children('air', true) as $fareInfo) {
                    if (strcmp($fareInfo->getName(), 'FareInfo') == 0) {
                        foreach ($fareInfo->attributes() as $a => $b) {
                            if (strcmp($a, 'Key') == 0) {
                                if (strcmp($b, $key) == 0) {
                                    return $fareInfo;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function FormatListFareInfo($fareInfo, $lowFare)
    {
        $finfo = [];
        foreach ($fareInfo->attributes() as $e => $f) {
            $finfo[$e] = $f->__toString();
        }
        $finfo['BaggageAllowance']['MaxWeight'] = [];
        if (isset($fareInfo->BaggageAllowance->MaxWeight->attributes()['Value'])) {
            $finfo['BaggageAllowance']['MaxWeight']['Value'] = $fareInfo->BaggageAllowance->MaxWeight->attributes()['Value']->__toString();
        }
        if (isset($fareInfo->BaggageAllowance->MaxWeight->attributes()['Unit'])) {
            $finfo['BaggageAllowance']['MaxWeight']['Unit'] = $fareInfo->BaggageAllowance->MaxWeight->attributes()['Unit']->__toString();
        }
        $finfo['FareRuleKey'] = $fareInfo->FareRuleKey->__toString();
        return $finfo;


    }

    private function getFormatedDate($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    private function getFlightDetails($json_array)
    {
        if (!isset($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:FlightDetailsList']['air:FlightDetails'])) {
            return array();
        }
        $air_FlightDetails = $this->makeArray($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:FlightDetailsList']['air:FlightDetails']);
        $FlightDetails = [];
        foreach ($air_FlightDetails as $key => $value) {
            $index_key = $value["@attributes"]["Key"];
            $FlightDetails[$index_key] = $value;
        }
        return $FlightDetails;
    }

    private function getAirSegment($json_array)
    {
        if (!isset($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:AirSegmentList']['air:AirSegment'])) {
            return array();
        }
        $air_FlightDetails = $this->makeArray($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:AirSegmentList']['air:AirSegment']);
        $FlightDetails = [];
        foreach ($air_FlightDetails as $key => $value) {
            $index_key = $value["@attributes"]["Key"];
            $FlightDetails[$index_key] = $value;
        }
        return $FlightDetails;
    }

    private function getFareInfo($json_array)
    {
        if (!isset($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:FareInfoList']['air:FareInfo'])) {
            return array();
        }
        $air_FlightDetails = $this->makeArray($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:FareInfoList']['air:FareInfo']);
        $FlightDetails = [];
        foreach ($air_FlightDetails as $key => $value) {
            $index_key = $value["@attributes"]["Key"];
            $FlightDetails[$index_key] = $value;
        }
        return $FlightDetails;
    }

    private function getRoute($json_array)
    {
        if (!isset($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:RouteList']['air:Route'])) {
            return array();
        }
        $air_FlightDetails = $this->makeArray($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:RouteList']['air:Route']);
        $FlightDetails = [];
        foreach ($air_FlightDetails as $key => $value) {
            $index_key = $value["@attributes"]["Key"];
            $FlightDetails[$index_key] = $value;
        }
        return $FlightDetails;
    }

    private function getBrand($json_array)
    {
        if (!isset($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:BrandList']['air:Brand'])) {
            return array();
        }
        $air_FlightDetails = $this->makeArray($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:BrandList']['air:Brand']);
        $FlightDetails = [];
        foreach ($air_FlightDetails as $key => $value) {
            $index_key = $value["@attributes"]["Key"];
            $FlightDetails[$index_key] = $value;
        }
        return $FlightDetails;
    }

    private function getAirPricingSolution($json_array)
    {
        if (!isset($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:BrandList']['air:Brand'])) {
            return array();
        }
        $air_FlightDetails = $this->makeArray($json_array['SOAP:Envelope']['SOAP:Body']['air:LowFareSearchRsp']['air:AirPricingSolution']);
        $FlightDetails = [];
        foreach ($air_FlightDetails as $key => $value) {
            $index_key = $value["@attributes"]["Key"];
            $FlightDetails[$index_key] = $value;
        }
        return $FlightDetails;
    }

    private function makeArray($array)
    {
        return isset($array[0]) ? $array : [$array];
    }

    private function getAllAirSegments($journey, $lowFare)
    {
        $journeyList = [];
        $journeyList['AirSegment'] = [];
        foreach ($journey->children('air', true) as $segmentRef) {
            if (strcmp($segmentRef->getName(), 'AirSegmentRef') == 0) {
                $AirSegmentRef = [];
                $AirSegmentRef['FlightDetails'] = [];
                foreach ($segmentRef->attributes() as $a => $b) {
                    $segment = $this->ListAirSegments($b, $lowFare);
                    foreach ($segment->attributes() as $c => $d) {
                        $AirSegmentRef[$c] = $d->__toString();
                    }

                    $flightDetailsList = [];
                    $flightDetailsList['FlightDetailsRef'] = [];
                    foreach ($segment->children('air', true) as $flightDetailsRef) {
                        if (strcmp($flightDetailsRef->getName(), 'FlightDetailsRef') == 0) {
                            $flightDetails = [];
                            foreach ($flightDetailsRef->attributes() as $f => $g) {
//                                $flightDetails[$f] = $g->__toString();
                                $fdetails = $this->ListFlightDetails($g, $lowFare);
                                foreach ($fdetails->attributes() as $h => $i) {
                                    $flightDetails[$h] = $i->__toString();
                                }
                            }
                            array_push($flightDetailsList['FlightDetailsRef'], $flightDetails);
                        }
                    }
                    array_push($AirSegmentRef['FlightDetails'], $flightDetailsList);


                }
                array_push($journeyList['AirSegment'], $AirSegmentRef);


            }
        }

        $journeyList['Duration'] = date_diff(
            date_create(end($journeyList['AirSegment'])['ArrivalTime']),
            date_create(reset($journeyList['AirSegment'])['DepartureTime'])
        );
        $journeyList['ArrivalTime'] = end($journeyList['AirSegment'])['ArrivalTime'];
        $journeyList['DepartureTime'] = reset($journeyList['AirSegment'])['DepartureTime'];
        $journeyList['Origin'] = reset($journeyList['AirSegment'])['Origin'];
        $journeyList['Destination'] = end($journeyList['AirSegment'])['Destination'];
        $journeyList['ViaStops'] = $this->getViaStops($journeyList['AirSegment']);
        $journeyList['Stops'] = count($journeyList['AirSegment']);
//        array_push($airSolution['Journey']['Routes'], $journeyList);
        return $journeyList;
    }

    private function getViaStops($jlist)
    {
        if (count($jlist) < 2) {
            return false;
        }
//        $ret_data = [];
//        for($i = 0; $i < count($jlist) - 1; $i++){
//            $ret_data[$i] = $jlist[$i]["Destination"];
//        }
        $ret_data = "";
        for ($i = 0; $i < count($jlist) - 1; $i++) {
            $ret_data .= $jlist[$i]["Destination"];
            if ($i + 1 < (count($jlist) - 1)) {
                $ret_data .= ", ";
            }
        }
        return $ret_data;
    }

}


function xml2array($xmlObject, $out = array())
{
    foreach ((array)$xmlObject as $index => $node)
        $out[$index] = (is_object($node)) ? xml2array($node) : $node;

    return $out;
}