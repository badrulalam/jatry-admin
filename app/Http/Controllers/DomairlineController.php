<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Domairline;
use Auth;
use Session;

class DomairlineController extends Controller
{



    public function indexAction(Request $request)
    {
        $data = Domairline::orderBy('id','ASC')->paginate(10);
        return view('adminarea.domair.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function newAction()
    {
           return view('adminarea.domair.new');
    }

    public function createAction(Request $request)
    {
        $this->validate($request, [
//            'name' => 'required|unique:pcategory,name',
            'name' => 'required',
            'image' => 'required',
        ]);

        $airline = new Domairline();
        $airline->name = $request->input('name');
        $airline->image = $request->input('image');
        $airline->save();

        return redirect()->route('domesticairlineindex')
            ->with('success','Airline created successfully');
    }

    public function editAction($id)
    {
        $airline = Domairline::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.domair.edit',compact('airline'));
        }

        else{
            return redirect()->back();
        }

    }

    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'name' => 'required',
            'image' => 'required',
        ]);

        $airline = Domairline::findOrFail($id);
        $airline->name = $request->input('name');
        $airline->image = $request->input('image');
        $airline->save();



        return redirect()->route('domesticairlineindex')
            ->with('success','Airline update successfully');


    }

    public function destroyAction($id)
    {

        $airline = Domairline::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){

            $airline->delete();
            return redirect()->route('domesticairlineindex')
                ->with('success','Airline deleted successfully');
        }

        else{
            return redirect()->back();
        }

    }

}
