<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

use App\Http\Requests;

use Redirect;
use URL;
use Validator;


use App\User;
use App\Agency;
use App\Agent;
use App\Role;
use App\Passengerforselect;
use App\Cardinfo;
use App\Sale;

use Session;
use MyFuncs;

use Socialite;

class CustomerController extends Controller
{


    public function __construct()
    {

    }


    public function MyAccountSaveAction_bf_change(Request $request)
    {

        $user = \Auth::user();
        $this->validate($request, [

            'user_name' => 'required',
            'user_email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'same:confirm-password',


            'customer_firstname' => 'required',
//            'customer_lastname' => 'required',
            'customer_email' => 'required|email|unique:customer,email,'.$user->customer->id,
            'customer_address' => 'required',
            'customer_city' => 'required',
            'customer_state' => 'required',
            'customer_postalcode' => 'required',
            'customer_country' => 'required',
            'customer_phone' => 'required',
//            'customer_image' => 'required',

        ]);






        $user->name = $request->input('user_name');
        $user->email = $request->input('user_email');

        if(!empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();


        $user->customer->first_name = $request->input('customer_firstname');
        $user->customer->last_name = $request->input('customer_lastname');
        $user->customer->email = $request->input('customer_email');
        $user->customer->address = $request->input('customer_address');
        $user->customer->city = $request->input('customer_city');
        $user->customer->state = $request->input('customer_state');
        $user->customer->postalcode = $request->input('customer_postalcode');
        $user->customer->country = $request->input('customer_country');
        $user->customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

        if ($request->file('customer_image')) {
            \File::delete(storage_path().'/uploads/customer-image/'.$user->customer->image);
            $customerimage = $request->file('customer_image');
            $customerimagename = rand(11111,99999).'-'.$customerimage->getClientOriginalName();
            $customerimage->move(storage_path().'/uploads/customer-image/', $customerimagename);
            $user->customer->image = $customerimagename;
        }
        $user->customer->save();


        return view('customer.myaccount', ['user' => $user]);
    }



    public function allCustomerListAction(Request $request)
    {
        $data = Customer::orderBy('id','ASC')->paginate(10);
        return view('adminarea.customer.allcustomerlist',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }


    public function allCustomerSaleAction(Request $request, $userId)
    {
        $data = Sale::where('user_id',$userId )->orderBy('id','DESC')->paginate(10);
        return view('adminarea.customer.customer-sale',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }


//    =======================Facebook Start=============

    public function handleProviderCallback()
    {
        try{
            $fbUser = Socialite::driver('facebook')->user();
        }
        catch( \Exception $e){
            return redirect('home');
        }

        $usermail = User::where('email', $fbUser->getEmail())->first();

        if($usermail){
            if($usermail->facebook_id == null || $usermail->facebook_id == ''){
                $usermail->facebook_id = $fbUser->getId();
                $usermail->save();
//                $usermail->customer->image = $fbUser->getAvatar();
//                $usermail->customer->save();
            }
        }

        $findfbuser = User::where('facebook_id', $fbUser->getId())->first();
        if(!$findfbuser){
//            $svuser =  User::create([
//                'facebook_id' => $fbUser->getId(),
//                'name' => $fbUser->getName(),
//                'email' => $fbUser->getEmail(),
//            ]);

//            ----------------
            $user = new User();
            $user->facebook_id =  $fbUser->getId();
            $user->name = $fbUser->getName();
            $user->email = $fbUser->getEmail();
//            $user->password = bcrypt($request->input('password'));
            $user->save();

            $role = Role::where('name', 'customer')->first();
            $user->attachRole($role->id);

            $customer = new Customer();

            $customer->first_name = $fbUser->getName();
//            $customer->last_name = $request->input('customer_lastname');
            $customer->email = $fbUser->getEmail();
//            $customer->address = $request->input('customer_address');
//            $customer->city = $request->input('customer_city');
//            $customer->state = $request->input('customer_state');
//            $customer->postalcode = $request->input('customer_postalcode');
//            $customer->country = $request->input('customer_country');
//            $customer->phone = $request->input('customer_phone');
//        $customer->customer_image = $request->input('customer_image');

//            $customer->image = $fbUser->getAvatar();

            $customer->user_id = $user->id;
            if(MyFuncs::getAgency()){
                $customer->agency_id = MyFuncs::getAgency()->id;
                $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

                $customer->byjatry = false;
                $customer->byagency = true;
                $customer->byagent = false;
            }
            else{
                $customer->agency_id = null;
                $customer->agent_id = null;

                $customer->byjatry = true;
                $customer->byagency = false;
                $customer->byagent = false;

            }

            $customer->save();
            $customer->code = 'CUS-'.$customer->id;;
            $customer->save();


//            ----------------

            auth()->login($user);
            return redirect()->route('home');

        }
        else{
            auth()->login($findfbuser);
            return redirect()->route('home');
        }


    }


//    =======================Facebook End===============






}
