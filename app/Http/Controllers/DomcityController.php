<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\User;
use App\Role;
use App\Domcity;
use Auth;
use Session;

class DomcityController extends Controller
{



    public function indexAction(Request $request)
    {
        $data = Domcity::orderBy('id','ASC')->paginate(10);
        return view('adminarea.domcity.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function newAction()
    {
        return view('adminarea.domcity.new');
    }

    public function createAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $city = new Domcity();
        $city->name = $request->input('name');
        $city->save();

        return redirect()->route('domesticcityindex')
            ->with('success','City created successfully');
    }

    public function editAction($id)
    {
        $city = Domcity::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){
            return view('adminarea.domcity.edit',compact('city'));
        }

        else{
            return redirect()->back();
        }

    }

    public function updateAction(Request $request, $id)
    {
        $user = \Auth::user();

        $this->validate($request, [
            'name' => 'required'
        ]);

        $city = Domcity::findOrFail($id);
        $city->name = $request->input('name');
        $city->save();

        return redirect()->route('domesticcityindex')
            ->with('success','City update successfully');
    }

    public function destroyAction($id)
    {

        $city = Domcity::findOrFail($id);

        if(\Auth::user()->hasRole('superadmin')){

            $city->delete();
            return redirect()->route('domesticcityindex')
                ->with('success','City deleted successfully');
        }

        else{
            return redirect()->back();
        }

    }
}
