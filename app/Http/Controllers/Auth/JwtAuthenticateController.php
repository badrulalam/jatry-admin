<?php

namespace App\Http\Controllers\Auth;

use App\Permission;
use App\Role;
use App\User;
use Faker\Provider\Base;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Customer;
use MyFuncs;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Mail;

class JwtAuthenticateController extends Controller
{
    
    public function profile()
    {
        return response()->json(['auth'=>Auth::user()]);
    }

    
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['invalidAttributes' => $validator->errors()], 400);
        }

        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(['token' => $token]);
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirmPassword',
        ]);
        if($validator->fails()){
            return response()->json(['invalidAttributes' => $validator->errors()], 400);
        }


        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        $role = Role::where('name', 'customer')->first();
        $user->attachRole($role->id);

        $customer = new Customer();
        $customer->email = $request->input('email');
        $customer->user_id = $user->id;

        if(MyFuncs::getAgency()){
            $customer->agency_id = MyFuncs::getAgency()->id;
            $customer->agent_id = MyFuncs::getAgency()->user->agent->id;

            $customer->byjatry = false;
            $customer->byagency = true;
            $customer->byagent = false;
        }
        else{
            $customer->agency_id = null;
            $customer->agent_id = null;

            $customer->byjatry = true;
            $customer->byagency = false;
            $customer->byagent = false;

        }

        $customer->save();
        $customer->code = 'CUS-'.$customer->id;;
        $customer->save();

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->has('remember')))
        {
            if(Auth::user()->hasRole('customer')){

                $reguser = Auth::user();
//               ========Mail Start=============
                Mail::send('emails.registermail', ['name' => $reguser->name, 'content' => 'Thank you for registration.'], function ($message) use ($reguser)
                {
                    $message->sender('vincej1657@gmail.com');
                    $message->subject('Jatry Registration');
                    $message->to($reguser->email);

                });
//               ========Mail End=============

                $credentials = $request->only('email', 'password');

                try {
                    // verify the credentials and create a token for the user
                    if (! $token = JWTAuth::attempt($credentials)) {
                        return response()->json(['error' => 'invalid_credentials'], 401);
                    }
                } catch (JWTException $e) {
                    // something went wrong
                    return response()->json(['error' => 'could_not_create_token'], 500);
                }

                // if no errors are encountered we can return a JWT
                return response()->json(['token' => $token]);
            }
        }








//        $validator = Validator::make($request->all(), [
//            'email' => 'required|email',
//            'password' => 'required'
//        ]);
//        if($validator->fails()){
//            return response()->json(['invalidAttributes' => $validator->errors()], 400);
//        }
//
//        $credentials = $request->only('email', 'password');
//
//        try {
//            // verify the credentials and create a token for the user
//            if (! $token = JWTAuth::attempt($credentials)) {
//                return response()->json(['error' => 'invalid_credentials'], 401);
//            }
//        } catch (JWTException $e) {
//            // something went wrong
//            return response()->json(['error' => 'could_not_create_token'], 500);
//        }
//
//        // if no errors are encountered we can return a JWT
//        return response()->json(['token' => $token]);
    }

    public function checkRoles(Request $request){
        return $this->getAuthenticatedUser();
        $user = JWTAuth::parseToken()->authenticate();
//        return $user;
//        $user = User::where('email', '=', $request->input('email'))->first();
        Log::info($user);
        return response()->json([
            "user" => $user,
            "owner" => $user->hasRole('owner'),
            "admin" => $user->hasRole('admin'),
            "customer" => $user->hasRole('customer'),
            "editUser" => $user->can('edit-user'),
            "listUsers" => $user->can('list-users')
        ]);
    }
    public function tokenRefresh(Request $request){
        return response()->json(true);
    }

    public function getPictureAction(Request $request, $folder, $file){

        if($folder=='agent-image'){
            $file_path = storage_path('uploads/agent-image/'.$file);
        }
        elseif($folder=='agency-image'){
            $file_path = storage_path('uploads/agency-image/'.$file);
        }
        elseif($folder=='agency-logo'){
            $file_path = storage_path('uploads/agency-logo/'.$file);
        }
        elseif($folder=='customer-image'){
            $file_path = storage_path('uploads/customer-image/'.$file);
        }
        elseif($folder=='banner-image'){
            $file_path = storage_path('uploads/banner-image/'.$file);
        }
        elseif($folder=='pcategory-image'){
            $file_path = storage_path('uploads/pcategory-image/'.$file);
        }
        elseif($folder=='tpackage-image'){
            $file_path = storage_path('uploads/tpackage-image/'.$file);
        }
        elseif($folder=='jcontinent-image'){
            $file_path = storage_path('uploads/jcontinent-image/'.$file);
        }
        elseif($folder=='jcountry-image'){
            $file_path = storage_path('uploads/jcountry-image/'.$file);
        }
        else{
            $file_path = storage_path('upload/1.jpg');
        }



//        return response()->download($file_path, 'there\'s file name',  ['Content-Type' => 'image/JPEG']);
//        return response()->file($file_path);


        return response()->file($file_path);

    }
    public function getAuthenticatedUser()
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

}

