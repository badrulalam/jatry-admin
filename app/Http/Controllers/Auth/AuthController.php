<?php

namespace App\Http\Controllers\Auth;

use Auth;
//use Illuminate\Auth\Access\Response;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\Role;
use App\Agency;
use DB;
use Mail;
use App\Customer;
use App\Agent;
use Session;
use MyFuncs;
use Socialite;






use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
//    protected $redirectTo = '/admin';
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'userregister', 'userregistersave', 'frontAgentLoginAction', 'handleProviderCallback']]);
    }





/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function doLogin_org(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

//        $email = Request::input('email');
        $email = $request->input('email');

//        $password = Request::input('password');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
//            return redirect()->intended('dashboard');
            return redirect()->route('allusers');
        }
    }




    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');


        $user = User::where('email', $request->input('email'))->first();
        if($user->hasRole('superagent') || $user->hasRole('superagent') || $user->hasRole('agent')){
            if (Auth::attempt($credentials, $request->has('remember')))
            {
                if (Auth::attempt($credentials, $request->has('remember')))
                {
                    if(Auth::user()->hasRole('superadmin')){
                        return redirect()->route('admindashboard');
                    }
                    elseif(Auth::user()->hasRole('superagent')){
                        return redirect()->route('agencydashboard');
                    }
                    elseif(Auth::user()->hasRole('agent')){
                        return redirect()->route('agentdashboard');
                    }
                    return 'You are may be customer';

                }
            }
            else{
                return redirect()->back()
                    ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'crederr' => $this->getFailedLoginMessage(),
                    ]);
            }
        }
        else{
//            return redirect()->route('home');


            return redirect()->back()
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'crederr' => $this->getFailedLoginMessage(),
                ]);
        }








    }



    public function doLogin_aaa(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        $user = User::where('email', $request->input('email'))->first();

        if($user->hasRole('superadmin')){
            if (Auth::attempt($credentials, $request->has('remember')))
            {
                return redirect()->route('admindashboard');
            }

        }

        if($user->hasRole('superagent')){
            if (Auth::attempt($credentials, $request->has('remember')))
            {
                return redirect()->route('agencydashboard');
            }
        }

        if($user->hasRole('agent')){
            if (Auth::attempt($credentials, $request->has('remember')))
            {
                return redirect()->route('agentdashboard');
            }
        }

        if($user->hasRole('customer')){
//            return 'You are customer';
            return redirect()->route('home');
        }


        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'crederr' => $this->getFailedLoginMessage(),
            ]);
    }

//    protected function getFailedLoginMesssage()
//    {
//        return Lang::has('auth.failed')
//            ? Lang::get('auth.failed')
//            : 'These credentials do not match our recordsDDD.';
//    }






















    protected function userregister()
    {
        $roles = Role::lists('display_name','id');
        return view('auth.userregister',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function userregistersave(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }


    protected function agentRegisterSaveAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }
















//    =========================Facebook Login Start===================================================

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {


       try{
           $fbUser = Socialite::driver('facebook')->user();

       }
       catch( \Exception $e){

          return redirect('home');
       }

        $usermail = User::where('email', $fbUser->getEmail())->first();

        if($usermail){
            if($usermail->facebook_id == null || $usermail->facebook_id == ''){
                $usermail->facebook_id = $fbUser->getId();
                $usermail->save();
            }
        }






       $findfbuser = User::where('facebook_id', $fbUser->getId())->first();

        if(!$findfbuser){
            $svuser =  User::create([
                'facebook_id' => $fbUser->getId(),
                'name' => $fbUser->getName(),
                'email' => $fbUser->getEmail(),
            ]);


//            ----------------




//            ----------------





            auth()->login($svuser);
//            if(Session::get('redirect')){
//                $abs_url = Session::get('redirect');
//                Session::forget('redirect');
//                return redirect($abs_url);
//            }
            return redirect()->route('home');

        }
        else{
            auth()->login($findfbuser);

//            if(Session::get('redirect')){
//                $abs_url = Session::get('redirect');
//                Session::forget('redirect');
//                return redirect($abs_url);
//            }
            return redirect()->route('home');
        }







//        $user = Socialite::driver('facebook')->user();
//        $fbArr = array(
//            'token' => $user->token,
//            'getId' => $user->getId(),
//            'getNickname' => $user->getNickname(),
//            'getName' => $user->getName(),
//            'getEmail' => $user->getEmail(),
//            'getAvatar' => $user->getAvatar(),
//        );
//        return $fbArr;

//        return $user->getEmail();


        // $user->token;
    }



//    =========================Facebook Login End=====================================================

}
