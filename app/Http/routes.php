<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
////    return view('welcome');
////    return Request::server('HTTP_HOST');
////    return MyFuncs::full_name("John","Doe");
//    return MyFuncs::Hostname();
//});



Route::get('/', [
    'as' => 'home',
//    'uses' => 'FrontendController@homeAction'
    'uses' => 'admin\AdminController@adminDashboardAction'
]);

//Route::get('/home', 'HomeController@index');
Route::get('/home', 'admin\AdminController@adminDashboardAction');


Route::get('/admin', [
    'as' => 'admindashboard',
    'uses' => 'admin\AdminController@adminDashboardAction'
]);






Route::get('checkout-final', [
    'as' => 'checkoutfinal',
    'uses' => 'FrontendController@CheckoutFinalAction'
]);
Route::get('ticket-sale/{invoice}', [
    'as' => 'ticketsale',
//    'uses' => 'FrontendController@TicketSaleAction'
    'uses' => 'SaleController@TicketSaleAction'
]);


Route::get('sale-pdf/{invoice}', [
    'as' => 'salepdf',
//    'uses' => 'FrontendController@TicketSaleAction'
    'uses' => 'SaleController@TicketSalePdfAction'
]);

//============Password start===========


//============Password end===========



//----------Agency Start-----------------
Route::get('admin-agency/index', [
    'as' => 'allagencylist',
    'uses' => 'AgencyController@allAgencyListAction'
]);


Route::get('admin-agency/{id}/show', [
    'as' => 'adminagencyshow',
    'uses' => 'AgencyController@adminAgencyShowAction'
]);

Route::get('admin-agency/{id}/agent', [
    'as' => 'adminagencyagentlist',
    'uses' => 'AgencyController@adminAgencyAgentListAction'
]);

//----------Agency End-----------------


//-----------Agent Start----------------
Route::get('admin-agent/index', [
    'as' => 'allagencylist',
    'uses' => 'AgentController@allAgentListAction'
]);

//-----------Agent End----------------


//-------------Customer Start-----------------

Route::get('admin-customer/index', [
    'as' => 'allcustomerlist',
    'uses' => 'CustomerController@allCustomerListAction'
]);

Route::get('admin-customer/sale/{id}', [
    'as' => 'allcustomersale',
    'uses' => 'CustomerController@allCustomerSaleAction'
]);

Route::get('sale/{invoice}', [
    'as' => 'saleinvoice',
    'uses' => 'SaleController@saleInvoiceAction'
]);




//-------------Customer End-----------------















//Route::get('/admin', function () {
//    return view('admindash');
//});

Route::auth();


Route::get('/admin-search', [
    'as' => 'admin-search',
    'uses' => 'admin\AdminController@adminSearchAction'
]);

Route::get('/amadeus-search', [
    'as' => 'amadeus-search',
    'uses' => 'admin\AdminController@amadeusSearchAction'
]);

Route::get('/qpx-search', [
    'as' => 'qpx-search',
    'uses' => 'admin\AdminController@qpxSearchAction'
]);





Route::get('/agency', [
    'as' => 'agencydashboard',
    'uses' => 'admin\AdminController@agencyDashboardAction'
]);

Route::get('/agent', [
    'as' => 'agentdashboard',
    'uses' => 'admin\AdminController@agentDashboardAction'
]);





//--------Register- Start-------
Route::get('user/register', [
    'as' => 'userregister',
    'uses' => 'Auth\AuthController@userregister'
]);


Route::post('user/registersave', [
    'as' => 'userregistersave',
    'uses' => 'Auth\AuthController@userregistersave'
]);


//--------Register- End-------







//----User-Start--



Route::get('user/new', [
    'as' => 'newuser',
    'uses' => 'UserController@newAction'
]);


Route::post('user/create', [
    'as' => 'createuser',
    'uses' => 'UserController@createAction'
]);


Route::get('user/index', [
    'as' => 'allusers',
    'uses' => 'UserController@index'
]);

Route::get('user/id/{id}', [
    'as' => 'useredit',
    'uses' => 'UserController@edit'
]);

Route::patch('user/update/{id}', [
    'as' => 'userupdate',
    'uses' => 'UserController@update'
]);


Route::delete('user/delete/{id}', [
    'as' => 'userdelete',
    'uses' => 'UserController@destroy'
]);

//----User-End--

Route::post('/dologin', [
    'as' => 'custlogin',
    'uses' => 'Auth\AuthController@doLogin'
]);






//----Role-Start--

Route::get('role/new', [
    'as' => 'newrole',
    'uses' => 'RoleController@newAction'
]);





Route::get('role/index', [
    'as' => 'allroles',
    'uses' => 'RoleController@indexAction'
]);

Route::post('role/create', [
    'as' => 'createrole',
    'uses' => 'RoleController@createAction'
]);

Route::get('role/id/{id}', [
    'as' => 'roleedit',
    'uses' => 'RoleController@editAction'
]);

Route::patch('role/update/{id}', [
    'as' => 'roleupdate',
    'uses' => 'RoleController@updateAction'
]);

Route::delete('role/delete/{id}', [
    'as' => 'roledelete',
    'uses' => 'RoleController@destroyAction'
]);

//----Role-End--



//----Permission---Start--

Route::get('permission/index', [
    'as' => 'allpermission',
    'uses' => 'PermissionController@indexAction'
]);

Route::get('permission/new', [
    'as' => 'newpermission',
    'uses' => 'PermissionController@newAction'
]);

Route::post('permission/create', [
    'as' => 'createpermission',
    'uses' => 'PermissionController@createAction'
]);



Route::get('permission/id/{id}', [
    'as' => 'permissionedit',
    'uses' => 'PermissionController@editAction'
]);

Route::patch('permission/update/{id}', [
    'as' => 'permissionupdate',
    'uses' => 'PermissionController@updateAction'
]);

Route::delete('permission/delete/{id}', [
    'as' => 'permissiondelete',
    'uses' => 'PermissionController@destroyAction'
]);

//----Permission---End--


//---------Banner Image Start-------
Route::get('bannerimage/index', [
    'as' => 'allbannerimage',
    'uses' => 'BannerimageController@indexAction'
]);


Route::get('bannerimage/new', [
    'as' => 'newbannerimage',
    'uses' => 'BannerimageController@newAction'
]);


Route::post('bannerimage/create', [
    'as' => 'createbannerimage',
    'uses' => 'BannerimageController@createAction'
]);


Route::get('bannerimage/id/{id}', [
    'as' => 'bannerimageedit',
    'uses' => 'BannerimageController@editAction'
]);

Route::patch('bannerimage/update/{id}', [
    'as' => 'bannerimageupdate',
    'uses' => 'BannerimageController@updateAction'
]);

Route::delete('bannerimage/delete/{id}', [
    'as' => 'bannerimagedelete',
    'uses' => 'BannerimageController@destroyAction'
]);



//---------Banner Image End-------





//-------DOmain Route Start----------

//Route::group(['domain' => '{aaa}.ami.com'], function()
//{
//    Route::get('/', function()
//    {
//        return 'My own domain';
//    });
//});



//Route::group(['domain' => '{subdomain}.{domain}.{tld}'], function(){
//
//    Route::get('/', function($sub, $domain, $tld){
//        return 'subdomain: ' . $sub . '.' . $domain . '.' . $tld;
//    });
//});



//-------DOmain Route End----------



//-------------Agent Start------------

Route::get('agent/agency-add-agent', [
    'as' => 'agencyaddagent',
    'uses' => 'AgencyController@addAgentAction'
]);

Route::get('agent/agency-agent-list', [
    'as' => 'agencyagentlist',
    'uses' => 'AgencyController@agencyAgentListAction'
]);





Route::post('agent/agency-add-agent-save', [
    'as' => 'agencyaddagentsave',
    'uses' => 'AgencyController@addAgentSaveAction'
]);

Route::get('agent/agency-agent-edit/{id}', [
    'as' => 'agencyagentedit',
    'uses' => 'AgencyController@agencyAgentEditAction'
]);

Route::patch('agent/agency-agent-update/{id}', [
    'as' => 'agencyagentupdate',
    'uses' => 'AgencyController@agencyAgentUpdateAction'
]);




//-------------Agent End--------------



//----Pcategory---Start--

Route::get('pcategory/index', [
    'as' => 'allpcategory',
    'uses' => 'PcategoryController@indexAction'
]);

Route::get('pcategory/new', [
    'as' => 'newpcategory',
    'uses' => 'PcategoryController@newAction'
]);




Route::post('pcategory/create', [
    'as' => 'createpcategory',
    'uses' => 'PcategoryController@createAction'
]);



Route::get('pcategory/id/{id}', [
    'as' => 'pcategoryedit',
    'uses' => 'PcategoryController@editAction'
]);

Route::patch('pcategory/update/{id}', [
    'as' => 'pcategoryupdate',
    'uses' => 'PcategoryController@updateAction'
]);

Route::delete('pcategory/delete/{id}', [
    'as' => 'pcategorydelete',
    'uses' => 'PcategoryController@destroyAction'
]);

//----Pcategory---End--


//----Tpackage---Start--

Route::get('tpackage/index', [
    'as' => 'alltpackage',
    'uses' => 'TpackageController@indexAction'
]);

Route::get('tpackage/new', [
    'as' => 'newtpackage',
    'uses' => 'TpackageController@newAction'
]);


Route::post('tpackage/create', [
    'as' => 'createtpackage',
    'uses' => 'TpackageController@createAction'
]);



Route::get('tpackage/id/{id}', [
    'as' => 'tpackageedit',
    'uses' => 'TpackageController@editAction'
]);

Route::patch('tpackage/update/{id}', [
    'as' => 'tpackageupdate',
    'uses' => 'TpackageController@updateAction'
]);


Route::delete('tpackage/delete/{id}', [
    'as' => 'tpackagedelete',
    'uses' => 'TpackageController@destroyAction'
]);

//----Tpackage---End--


//----Jcontinent---Start--

Route::get('jcontinent/index', [
    'as' => 'alljcontinent',
    'uses' => 'JcontinentController@indexAction'
]);

Route::get('jcontinent/new', [
    'as' => 'newjcontinent',
    'uses' => 'JcontinentController@newAction'
]);




Route::post('jcontinent/create', [
    'as' => 'createjcontinent',
    'uses' => 'JcontinentController@createAction'
]);


Route::get('jcontinent/id/{id}', [
    'as' => 'jcontinentedit',
    'uses' => 'JcontinentController@editAction'
]);

Route::patch('jcontinent/update/{id}', [
    'as' => 'jcontinentupdate',
    'uses' => 'JcontinentController@updateAction'
]);

Route::delete('jcontinent/delete/{id}', [
    'as' => 'jcontinentdelete',
    'uses' => 'JcontinentController@destroyAction'
]);

//----Jcontinent---End--


//----Jcountry---Start--

Route::get('jcountry/index', [
    'as' => 'alljcountry',
    'uses' => 'JcountryController@indexAction'
]);

Route::get('jcountry/new', [
    'as' => 'newjcountry',
    'uses' => 'JcountryController@newAction'
]);




Route::post('jcountry/create', [
    'as' => 'createjcountry',
    'uses' => 'JcountryController@createAction'
]);


Route::get('jcountry/id/{id}', [
    'as' => 'jcountryedit',
    'uses' => 'JcountryController@editAction'
]);

Route::patch('jcountry/update/{id}', [
    'as' => 'jcountryupdate',
    'uses' => 'JcountryController@updateAction'
]);

Route::delete('jcountry/delete/{id}', [
    'as' => 'jcountrydelete',
    'uses' => 'JcountryController@destroyAction'
]);

//----Jcountry---End--


//----Airline---Start--

Route::get('domestic-airline/index', [
    'as' => 'domesticairlineindex',
    'uses' => 'DomairlineController@indexAction'
]);

Route::get('domestic-airline/new', [
    'as' => 'domesticairlinenew',
    'uses' => 'DomairlineController@newAction'
]);


Route::post('domestic-airline/create', [
    'as' => 'domesticairlinecreate',
    'uses' => 'DomairlineController@createAction'
]);



Route::get('domestic-airline/id/{id}', [
    'as' => 'domesticairlineedit',
    'uses' => 'DomairlineController@editAction'
]);

Route::patch('domestic-airline/update/{id}', [
    'as' => 'domesticairlineupdate',
    'uses' => 'DomairlineController@updateAction'
]);


Route::delete('domestic-airline/delete/{id}', [
    'as' => 'domesticairlinedelete',
    'uses' => 'DomairlineController@destroyAction'
]);

//----Airline---End--

//----Dom City---Start--

Route::get('domestic-city/index', [
    'as' => 'domesticcityindex',
    'uses' => 'DomcityController@indexAction'
]);

Route::get('domestic-city/new', [
    'as' => 'domesticcitynew',
    'uses' => 'DomcityController@newAction'
]);


Route::post('domestic-city/create', [
    'as' => 'domesticcitycreate',
    'uses' => 'DomcityController@createAction'
]);



Route::get('domestic-city/id/{id}', [
    'as' => 'domesticcityedit',
    'uses' => 'DomcityController@editAction'
]);

Route::patch('domestic-city/update/{id}', [
    'as' => 'domesticcityupdate',
    'uses' => 'DomcityController@updateAction'
]);


Route::delete('domestic-city/delete/{id}', [
    'as' => 'domesticcitydelete',
    'uses' => 'DomcityController@destroyAction'
]);

//----Dom City---End--

//----Dom Flight---Start--

Route::get('domestic-flight/index', [
    'as' => 'domesticflightindex',
    'uses' => 'DomflightController@indexAction'
]);

Route::get('domestic-flight/new', [
    'as' => 'domesticflightnew',
    'uses' => 'DomflightController@newAction'
]);


Route::post('domestic-flight/create', [
    'as' => 'domesticflightcreate',
    'uses' => 'DomflightController@createAction'
]);



Route::get('domestic-flight/id/{id}', [
    'as' => 'domesticflightedit',
    'uses' => 'DomflightController@editAction'
]);

Route::patch('domestic-flight/update/{id}', [
    'as' => 'domesticflightupdate',
    'uses' => 'DomflightController@updateAction'
]);


Route::delete('domestic-flight/delete/{id}', [
    'as' => 'domesticflightdelete',
    'uses' => 'DomflightController@destroyAction'
]);

//----Dom Flight---End--






//============Facebook Login Strat=========================
//Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
//Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('auth/facebook', [
    'as' => 'tofacebook',
    'uses' => 'Auth\AuthController@redirectToProvider'
]);

//Route::get('/auth/facebook/callback', [
//    'as' => 'fromfacebook',
//    'uses' => 'Auth\AuthController@handleProviderCallback'
//]);

Route::get('/auth/facebook/callback', [
    'as' => 'fromfacebook',
    'uses' => 'CustomerController@handleProviderCallback'
]);



//============Facebook Login End===========================


//=================Send Mail================================

Route::post('/send', 'EmailController@send');

Route::get('/number', 'EmailController@numnerAction');


//=================Send Mail================================


