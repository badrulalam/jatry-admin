<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Jcountry extends Model
{

    use Sluggable;

    protected $table = 'jcountry';



    public function jcontinent()
    {
        return $this->belongsTo('App\Jcontinent');
    }

    public function tpackage()
    {
        return $this->hasMany('App\Tpackage');
    }


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }



}
