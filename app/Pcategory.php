<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pcategory extends Model
{
    protected $table = 'pcategory';

    public function tpackage()
    {
        return $this->belongsToMany('App\Tpackage', 'pcategory_tpackage', 'pcategory_id', 'tpackage_id');
    }

}
