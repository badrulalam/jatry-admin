<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saleitemair extends Model
{

    protected $table = 'saleitemair';

    public function sale()
    {
        return $this->belongsTo('App\Sale');
    }

    public function passenger()
    {
        return $this->hasMany('App\Saleitemairpassenger');
    }

    public function route()
    {
        return $this->hasMany('App\Saleairroute');
    }
}
