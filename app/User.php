<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function agency()
    {
        return $this->hasOne('App\Agency');
    }

    public function agent()
    {
        return $this->hasOne('App\Agent');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer');
    }

    public function cart()
    {
        return $this->hasOne('App\Cart');
    }

    public function passengerforselect()
    {
        return $this->hasMany('App\Passengerforselect');
    }

    public function sale()
    {
        return $this->hasMany('App\Sale');
    }

}
