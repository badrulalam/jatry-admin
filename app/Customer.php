<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $table = 'customer';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }

    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
}
