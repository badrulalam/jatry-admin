<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartairseatmap extends Model
{

    protected $table = 'cartairseatmap';


    public function cartairroutesegment()
    {
        return $this->belongsTo('App\Cartairroutesegment');
    }

    public function cartitemairpassenger()
    {
        return $this->belongsTo('App\Cartitemairpassenger');
    }



}
