<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartitemairpassenger extends Model
{

    protected $table = 'cartitemairpassenger';

    public function cartitemair()
    {
        return $this->belongsTo('App\Cartitemair');
    }

//    public function cart()
//    {
//        return $this->belongsTo('App\Cart');
//    }

//    public function cartairpassengeritem()
//    {
//        return $this->hasMany('App\Cartairpassengeritem');
//    }

    public function cartairseatmap()
    {
        return $this->hasMany('App\Cartairseatmap');
    }
}
