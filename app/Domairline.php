<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domairline extends Model
{
    protected $table = 'domairline';

    public function domflight()
    {
        return $this->hasMany('App\Domflight');
    }
}
