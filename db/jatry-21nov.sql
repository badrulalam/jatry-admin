-- phpMyAdmin SQL Dump
-- version 4.3.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2016 at 03:28 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jatry`
--

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE IF NOT EXISTS `agency` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `logo` text COLLATE utf8_unicode_ci,
  `url` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`id`, `code`, `name`, `email`, `address`, `city`, `state`, `postalcode`, `country`, `phone`, `image`, `logo`, `url`, `isactive`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'ACY-1', 'Ageny1', 'agency1@zeteq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agency1.com', 1, 2, '2016-07-28 06:42:32', '2016-07-28 06:42:32'),
(2, 'ACY-2', 'asas', 'agengcy1@zeteq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abc.ami.com', 1, 3, '2016-07-28 06:59:24', '2016-07-28 06:59:24'),
(3, 'ACY-3', 'Ageny1aaa', 'agency211@zeteq.com', 'gsdg', 'thtr', 'thtr', 'rthr', 'AF', '6565', '36816-target.jpg', '75373-hema-hover.png', 'agency121.com', 1, 4, '2016-07-28 07:33:14', '2016-08-03 03:40:16'),
(4, 'ACY-4', 'zahid', 'eshaq@kjh.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zzzz', 1, 5, '2016-07-28 07:38:31', '2016-07-28 07:38:31'),
(5, 'ACY-5', 'AbcdeAgency', 'abcd@zeteq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcde.ami.com', 1, 6, '2016-07-31 03:39:36', '2016-07-31 03:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE IF NOT EXISTS `agent` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `logo` text COLLATE utf8_unicode_ci,
  `url` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `agency_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`id`, `code`, `first_name`, `last_name`, `email`, `address`, `city`, `state`, `postalcode`, `country`, `phone`, `image`, `logo`, `url`, `isactive`, `user_id`, `agency_id`, `created_at`, `updated_at`) VALUES
(2, 'AGT-2', 'gfdgjkj', 'gfdfgkjk', 'pagent41@zeteq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ami.com', 0, 3, 2, '2016-07-28 06:59:24', '2016-07-28 06:59:24'),
(3, 'AGT-3', 'gfdg23', 'gfdfg2323', 'aaadmin@admin.com', 'gsdfg', 'sdsf', 'sdfds', '121', 'AF', '0255555', '80554-capacity-menu-mouseover.jpg', '44743-capacity-menu-mouseover.jpg', NULL, 1, 4, 3, '2016-07-28 07:33:14', '2016-08-03 03:40:16'),
(4, 'AGT-4', 'kjkjh', 'qwrewer', 'rwerwr@poppp.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cde.ami.com', 1, 5, 4, '2016-07-28 07:38:31', '2016-07-28 07:38:31'),
(5, 'AGT-5', 'AbcdeAge', 'efegh', 'abcd@zeteq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcdeag.ami.com', 1, 6, 5, '2016-07-31 03:39:36', '2016-07-31 03:39:36'),
(6, 'AGT-6', 'age first name', 'last name', 'agn@zeteq.com', 'ctg', 'ctg', 'ctg', '1231', 'AF', '01911629040', '91591-capacity-menu-mouseover.jpg', NULL, NULL, 1, 7, 3, '2016-08-01 05:24:30', '2016-08-03 03:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `bannerimage`
--

CREATE TABLE IF NOT EXISTS `bannerimage` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `agency_id` int(10) unsigned DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `isjatry` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cardinfo`
--

CREATE TABLE IF NOT EXISTS `cardinfo` (
  `id` int(10) unsigned NOT NULL,
  `billing_firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` text COLLATE utf8_unicode_ci,
  `billing_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardholder` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exp_date` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardtrak` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvv` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cardinfo`
--

INSERT INTO `cardinfo` (`id`, `billing_firstname`, `billing_lastname`, `billing_email`, `billing_address`, `billing_city`, `billing_postalcode`, `billing_country`, `billing_state`, `billing_phone`, `cardholder`, `exp_date`, `cardtrak`, `cvv`, `card_number`, `user_id`, `cart_id`, `created_at`, `updated_at`) VALUES
(1, 'Arif', 'Iftekhar', 'arif@zeteq.com', 'address', 'ctg', 'AF', 'AF', 'AF', '1911629040', 'Mohammad Arif iftekhar', '2016-11-02', 'CRD-20161102147807431511', '123', '123456', 11, NULL, '2016-11-02 02:11:55', '2016-11-02 04:18:46'),
(2, 'mahafuzur', 'rahaman', 'mahafuz@gmail.com', 'chandpur', 'chandpur', 'chandpur', '1230', 'BD', '016050505', 'mahafuzur rahaman', '2016-02-12', 'CRD-20161103147815663311', '555', '9090909090', 11, NULL, '2016-11-03 01:03:53', '2016-11-03 01:03:53');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) unsigned NOT NULL,
  `billing_firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` text COLLATE utf8_unicode_ci,
  `billing_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardholder` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exp_date` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cvv` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` text COLLATE utf8_unicode_ci,
  `shipping_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `iscurrent` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `billing_firstname`, `billing_lastname`, `billing_email`, `billing_address`, `billing_city`, `billing_postalcode`, `billing_country`, `billing_state`, `billing_phone`, `card_number`, `cardholder`, `exp_date`, `cvv`, `shipping_firstname`, `shipping_lastname`, `shipping_email`, `shipping_address`, `shipping_city`, `shipping_postalcode`, `shipping_country`, `shipping_state`, `shipping_phone`, `user_id`, `iscurrent`, `created_at`, `updated_at`) VALUES
(1, 'xczc', 'xzcxzc', 'zqarif@gmail.com', 'dfsdf', 'sdafdsa', 'dsaf', 'dsfaa', 'dsfa', 'dfsa', NULL, NULL, NULL, NULL, NULL, NULL, 'zqarif@gmail.com', 'fdsaf', 'fdsa', 'fdsa', 'dsfa', 'fdsaf', 'fdsa', 14, 0, '2016-10-17 04:10:56', '2016-10-20 04:56:15'),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 0, '2016-11-20 22:06:50', '2016-11-21 02:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `cartairpassengeritem`
--

CREATE TABLE IF NOT EXISTS `cartairpassengeritem` (
  `id` int(10) unsigned NOT NULL,
  `passenger_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_seat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `cartitemair_id` int(10) unsigned DEFAULT NULL,
  `cartitemairpassenger_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartairroute`
--

CREATE TABLE IF NOT EXISTS `cartairroute` (
  `id` int(10) unsigned NOT NULL,
  `Origin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartureTime` datetime DEFAULT NULL,
  `ArrivalTime` datetime DEFAULT NULL,
  `Duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ViaStops` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Stops` int(11) DEFAULT NULL,
  `json_content` text COLLATE utf8_unicode_ci,
  `cartitemair_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartairroutesegment`
--

CREATE TABLE IF NOT EXISTS `cartairroutesegment` (
  `id` int(10) unsigned NOT NULL,
  `Key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Carrier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FlightNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Origin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartureTime` datetime DEFAULT NULL,
  `ArrivalTime` datetime DEFAULT NULL,
  `FlightTime` decimal(5,2) DEFAULT NULL,
  `Distance` decimal(5,2) DEFAULT NULL,
  `DistanceUnits` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ETicketability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Equipment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ChangeOfPlane` tinyint(1) NOT NULL,
  `ParticipantLevel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LinkAvailability` tinyint(1) NOT NULL,
  `PolledAvailabilityOption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OptionalServicesIndicator` tinyint(1) NOT NULL,
  `AvailabilitySource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AvailabilityDisplayType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumberOfStops` int(11) DEFAULT NULL,
  `DestinationTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TravelTime` decimal(5,2) DEFAULT NULL,
  `BookingCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BookingCount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CabinClass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NotValidBefore` date DEFAULT NULL,
  `NotValidAfter` date DEFAULT NULL,
  `FareRuleKey` text COLLATE utf8_unicode_ci,
  `MaxWeight` decimal(5,2) DEFAULT NULL,
  `WeightUnits` text COLLATE utf8_unicode_ci,
  `json_content` text COLLATE utf8_unicode_ci,
  `cartairroute_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartairseatmap`
--

CREATE TABLE IF NOT EXISTS `cartairseatmap` (
  `id` int(10) unsigned NOT NULL,
  `passenger_seat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cartairroutesegment_id` int(10) unsigned DEFAULT NULL,
  `cartitemairpassenger_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartitemair`
--

CREATE TABLE IF NOT EXISTS `cartitemair` (
  `id` int(10) unsigned NOT NULL,
  `Price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalPrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BasePrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApproximateTotalPrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApproximateBasePrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EquivalentBasePrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Taxes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApproximateTaxes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PolicyExclusion` tinyint(1) NOT NULL,
  `Refundable` tinyint(1) NOT NULL,
  `PlatingCarrier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TravelTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `childs` int(11) DEFAULT NULL,
  `infant` int(11) DEFAULT NULL,
  `tripType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noOfSegments` int(11) DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json_content` text COLLATE utf8_unicode_ci,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartitemairpassenger`
--

CREATE TABLE IF NOT EXISTS `cartitemairpassenger` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_issue_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_expiry_date` date DEFAULT NULL,
  `passenger_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cartitemair_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartitempkg`
--

CREATE TABLE IF NOT EXISTS `cartitempkg` (
  `id` int(10) unsigned NOT NULL,
  `tpackage_id` int(10) unsigned DEFAULT NULL,
  `cart_id` int(10) unsigned DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `shipping_firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` text COLLATE utf8_unicode_ci,
  `shipping_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `agency_id` int(10) unsigned DEFAULT NULL,
  `agent_id` int(10) unsigned DEFAULT NULL,
  `byjatry` tinyint(1) NOT NULL,
  `byagency` tinyint(1) NOT NULL,
  `byagent` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `code`, `first_name`, `last_name`, `email`, `address`, `city`, `state`, `postalcode`, `country`, `phone`, `image`, `shipping_firstname`, `shipping_lastname`, `shipping_email`, `shipping_address`, `shipping_city`, `shipping_postalcode`, `shipping_country`, `shipping_state`, `shipping_phone`, `user_id`, `agency_id`, `agent_id`, `byjatry`, `byagency`, `byagent`, `created_at`, `updated_at`) VALUES
(2, 'CUS-2', 'Arif', 'Iftekhar', 'zqarif@gmail.com', 'address', 'Dhaka', 'Dhaka', '1230', 'AF', '01911629040', '53826-hema-hover.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 2, 2, 0, 1, 0, '2016-08-08 03:57:14', '2016-10-30 05:15:04'),
(3, 'CUS-3', 'Arif Iftekhar', NULL, 'ctgrobin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'https://graph.facebook.com/v2.6/1246534132034668/picture?type=normal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, NULL, NULL, 1, 0, 0, '2016-09-27 06:20:18', '2016-09-27 06:20:18'),
(4, 'CUS-4', NULL, NULL, 'testuser@test.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Arif', 'Iftekhar', 'testuser@test.com', 'address', 'ctg', '13456', 'BD', 'ctg', '1911629040', 14, NULL, NULL, 1, 0, 0, '2016-10-03 05:40:23', '2016-10-04 05:35:20'),
(9, 'CUS-9', NULL, NULL, 'linkon666@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, NULL, 1, 0, 0, '2016-10-23 01:41:58', '2016-10-23 01:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `jcontinent`
--

CREATE TABLE IF NOT EXISTS `jcontinent` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jcontinent`
--

INSERT INTO `jcontinent` (`id`, `name`, `image`, `description`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Asia', '58243-3.jpg', 'Asia', 'asia', '2016-08-24 05:32:23', '2016-08-24 05:32:23'),
(2, 'Europe', '97052-2.jpg', 'Europe', 'europe', '2016-08-24 05:32:41', '2016-08-24 05:32:41'),
(3, 'Amarica', '31955-1.jpg', 'Amarica', 'amarica', '2016-08-24 05:32:54', '2016-08-24 05:32:54');

-- --------------------------------------------------------

--
-- Table structure for table `jcountry`
--

CREATE TABLE IF NOT EXISTS `jcountry` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `jcontinent_id` int(10) unsigned DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jcountry`
--

INSERT INTO `jcountry` (`id`, `name`, `image`, `description`, `jcontinent_id`, `isactive`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Bangladesh', '31238-4.jpg', 'Bangladesh', 1, 1, 'bangladesh', '2016-08-24 05:33:28', '2016-08-24 05:33:28'),
(2, 'India', '68486-1.jpg', 'India', 1, 1, 'india', '2016-08-24 05:33:56', '2016-08-24 05:33:56'),
(3, 'England', '38376-2.jpg', 'England', 2, 1, 'england', '2016-08-24 05:34:29', '2016-08-24 05:34:29'),
(4, 'USA', '37493-1.jpg', 'USA', 3, 1, 'usa', '2016-08-24 05:35:04', '2016-08-24 05:35:04'),
(5, 'Spain', '85090-4.jpg', 'Cat Spain', 2, 1, 'spain', '2016-08-24 05:42:21', '2016-08-24 05:42:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_21_070457_create_agencies_table', 1),
('2016_07_21_103814_create_agents_table', 1),
('2016_07_24_110001_entrust_setup_tables', 1),
('2016_08_03_100843_create_customers_table', 1),
('2016_08_04_052851_create_carts_table', 1),
('2016_08_09_110031_create_bannerimages_table', 1),
('2016_08_16_090704_create_pcategories_table', 1),
('2016_08_17_110924_create_pcategorytpackage_table', 1),
('2016_08_18_104838_create_jcontinents_table', 1),
('2016_08_18_104903_create_jcountries_table', 1),
('2016_08_19_090738_create_tpackages_table', 1),
('2016_09_22_085539_create_cartitempkgs_table', 1),
('2016_09_29_075439_create_cartitemairs_table', 1),
('2016_09_29_095209_create_cartitemairpassengers_table', 1),
('2016_10_05_075943_create_cardinfos_table', 1),
('2016_10_16_061026_create_passengerforselects_table', 1),
('2016_10_16_061145_create_sales_table', 1),
('2016_10_16_061210_create_saleitemairs_table', 1),
('2016_10_16_061223_create_saleitemairpassengers_table', 1),
('2016_11_15_054154_create_cartairpassengeritems_table', 1),
('2016_11_16_064839_create_cartairroutes_table', 1),
('2016_11_16_093720_create_cartairroutesegments_table', 1),
('2016_11_17_034901_create_cartairseatmaps_table', 1),
('2016_11_21_042615_create_saleairroutes_table', 1),
('2016_11_21_042645_create_saleairroutesegments_table', 1),
('2016_11_21_045217_create_saleairseatmaps_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `passengerforselect`
--

CREATE TABLE IF NOT EXISTS `passengerforselect` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_issue_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_expiry_date` date DEFAULT NULL,
  `passenger_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `passengerforselect`
--

INSERT INTO `passengerforselect` (`id`, `title`, `first_name`, `middle_name`, `last_name`, `dob`, `nationality`, `passport_no`, `passport_issue_country`, `passport_expiry_date`, `passenger_type`, `passenger_order`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Mr', 'mohammad', 'arif', 'iftekhar', '0000-00-00', 'bangladeshi', '123456', 'bangladeshi', '0000-00-00', 'adults', NULL, 14, '2016-10-17 04:12:43', '2016-10-17 04:12:43'),
(2, 'Mr', 'mahfuzur', NULL, 'rahaman', '0000-00-00', 'bangladeshi', '123456', 'bangladeshi', '0000-00-00', 'childs', NULL, 14, '2016-10-17 04:12:43', '2016-10-17 04:12:43'),
(3, 'Mr', 'Mohammad', 'Arif', 'Iftekhar', '0000-00-00', 'Bangladeshi', '123456', 'Bangladesh', '0000-00-00', 'adults', NULL, 11, '2016-10-25 00:01:40', '2016-10-25 00:01:40'),
(4, 'Miss', 'Mahafuzur', NULL, 'Rahaman', '0000-00-00', 'Bangladeshi', '123456', '000', '0000-00-00', 'adults', NULL, 11, '2016-10-26 00:57:31', '2016-10-26 05:33:53'),
(6, 'Mr', 'Mohammad', '', 'Ifrad', '0000-00-00', NULL, NULL, NULL, NULL, 'childs', NULL, 11, '2016-10-30 05:29:56', '2016-11-20 22:08:29'),
(7, 'Mr', 'Mohammad', '', 'Mesbha', '0000-00-00', NULL, NULL, NULL, NULL, 'childs', NULL, 11, '2016-10-30 05:31:48', '2016-11-20 22:08:29'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adults', NULL, 11, '2016-11-03 03:06:43', '2016-11-03 03:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('zqarif@gmail.com', 'bd08437effbed1257789f2f5c9f710f56305cb00d9bcc82f07a855750f692a35', '2016-10-23 07:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `pcategory`
--

CREATE TABLE IF NOT EXISTS `pcategory` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pcategory`
--

INSERT INTO `pcategory` (`id`, `name`, `image`, `description`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Cat Asia', '11240-3.jpg', 'Cat Asia', NULL, '2016-08-24 05:30:50', '2016-08-24 05:30:50'),
(2, 'Cat Europe', '71059-2.jpg', 'Cat Europe', NULL, '2016-08-24 05:31:10', '2016-08-24 05:31:10'),
(3, 'Cat Amarica', '84083-1.jpg', 'Cat Amarica', NULL, '2016-08-24 05:31:33', '2016-08-24 05:31:33'),
(4, 'Cat Bangladesh', '72231-2.jpg', 'Cat Bangladesh', NULL, '2016-08-24 05:40:52', '2016-08-24 05:40:52'),
(5, 'Cat India', '55368-1.jpg', 'Cat India', NULL, '2016-08-24 05:41:09', '2016-08-24 05:41:09'),
(6, 'Cat England', '99229-2.jpg', 'Cat England', NULL, '2016-08-24 05:41:29', '2016-08-24 05:41:29'),
(7, 'Cat Spain', '76530-4.jpg', 'Cat Spain', NULL, '2016-08-24 05:41:52', '2016-08-24 05:41:52'),
(8, 'Cat Italy', '82176-2.jpg', 'Cat Italy', NULL, '2016-08-24 06:28:23', '2016-08-24 06:28:23'),
(9, 'Cat Netherland', '67609-4.jpg', 'Cat Netherland', NULL, '2016-08-24 06:28:50', '2016-08-24 06:28:50'),
(10, 'Cat Bulgaria', '14250-5.jpg', 'Cat Bulgaria', NULL, '2016-08-24 06:29:17', '2016-08-24 06:29:17'),
(11, 'Cat Germany', '54906-4.jpg', 'Cat Germany', NULL, '2016-08-24 06:29:41', '2016-08-24 06:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `pcategory_tpackage`
--

CREATE TABLE IF NOT EXISTS `pcategory_tpackage` (
  `pcategory_id` int(10) unsigned NOT NULL,
  `tpackage_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pcategory_tpackage`
--

INSERT INTO `pcategory_tpackage` (`pcategory_id`, `tpackage_id`) VALUES
(1, 1),
(4, 1),
(1, 2),
(5, 2),
(2, 3),
(6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user-create', 'User_Create', 'User Create', '2016-07-28 04:00:57', '2016-07-28 04:00:57'),
(3, 'user-delete', 'User_Delete', 'User Delete', '2016-07-28 04:02:10', '2016-07-28 04:18:11'),
(4, 'role-create', 'Role_Create', 'Role Create', '2016-07-28 04:02:48', '2016-07-28 04:18:26'),
(5, 'role-edit', 'Role_Edit', 'Role Edit', '2016-07-28 04:03:23', '2016-07-28 04:03:23'),
(6, 'role-delete', 'Role_Delete', 'Role Delete', '2016-07-28 04:04:01', '2016-07-28 04:04:01'),
(7, 'permission-create', 'Permission_Cretae', 'Permission Cretae', '2016-07-28 04:04:54', '2016-07-28 04:04:54'),
(8, 'permission-edit', 'Permission Edit', 'Permission Edit', '2016-07-28 04:05:26', '2016-07-28 04:05:26'),
(9, 'permission-delete', 'Permission_Delete', 'Permission Delete', '2016-07-28 04:06:01', '2016-07-28 04:06:01'),
(10, 'agency-create', 'Agency_Create', 'Agency_Create', '2016-07-28 04:10:00', '2016-07-28 04:10:00'),
(11, 'agency_edit', 'Agency_Edit', 'Agency_Edit', '2016-07-28 04:10:37', '2016-07-28 04:10:37'),
(12, 'agency_delete', 'Agency_Delete', 'Agency_Delete', '2016-07-28 04:11:14', '2016-07-28 04:11:14'),
(13, 'agent_create', 'Agent_Cretae', 'Agent_Cretae', '2016-07-28 04:11:48', '2016-07-28 04:11:48'),
(14, 'agent_edit', 'Agent_Edit', 'Agent_Edit', '2016-07-28 04:12:29', '2016-07-28 04:12:29'),
(15, 'agent-delete', 'Agent_Delete', 'Agent_Delete', '2016-07-28 04:12:57', '2016-07-28 04:12:57'),
(16, 'customer-create', 'Customer_Create', 'Customer-Create', '2016-07-28 04:16:40', '2016-07-28 04:16:40'),
(17, 'customer-edit', 'Customer_Edit', 'Customer Edit', '2016-07-28 04:17:24', '2016-07-28 04:17:24'),
(18, 'customer-delete', 'Customer-Delete', 'Customer Delete', '2016-07-28 04:17:58', '2016-07-28 04:17:58');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(11, 2),
(13, 2),
(14, 2),
(17, 2),
(14, 3),
(17, 3),
(17, 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super_Admin', 'Super Admin', '2016-07-28 04:07:19', '2016-07-28 04:07:19'),
(2, 'superagent', 'Super_Agent', 'Super_Agent', '2016-07-28 04:14:47', '2016-07-28 04:14:47'),
(3, 'agent', 'Agent', 'Agent', '2016-07-28 04:15:33', '2016-07-28 04:15:33'),
(4, 'customer', 'Customer', 'Customer', '2016-07-28 04:21:17', '2016-07-28 04:21:17');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(4, 2),
(5, 2),
(6, 2),
(1, 3),
(2, 3),
(7, 3),
(1, 4),
(11, 4),
(13, 4),
(14, 4),
(19, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sale`
--

CREATE TABLE IF NOT EXISTS `sale` (
  `id` int(10) unsigned NOT NULL,
  `billing_firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` text COLLATE utf8_unicode_ci,
  `billing_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` text COLLATE utf8_unicode_ci,
  `shipping_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_postalcode` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_billing_same` tinyint(1) NOT NULL,
  `order_number` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_method` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_cost` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` decimal(8,2) NOT NULL,
  `is_shipped` tinyint(1) NOT NULL,
  `is_viewed` tinyint(1) NOT NULL,
  `cardinfo_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sale`
--

INSERT INTO `sale` (`id`, `billing_firstname`, `billing_lastname`, `billing_email`, `billing_address`, `billing_city`, `billing_postalcode`, `billing_country`, `billing_state`, `billing_phone`, `shipping_firstname`, `shipping_lastname`, `shipping_email`, `shipping_address`, `shipping_city`, `shipping_postalcode`, `shipping_country`, `shipping_state`, `shipping_phone`, `shipping_billing_same`, `order_number`, `payment_method`, `shipping_method`, `shipping_cost`, `total`, `is_shipped`, `is_viewed`, `cardinfo_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Arif', 'Iftekhar', 'zqarif@gmail.com', 'address', 'Dhaka', '1230', 'AF', 'Dhaka', '01911629040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'INV-20161121-1479717413-11', NULL, NULL, NULL, 0.00, 0, 0, 2, 11, '2016-11-21 02:36:53', '2016-11-21 02:36:53'),
(2, 'Arif', 'Iftekhar', 'zqarif@gmail.com', 'address', 'Dhaka', '1230', 'AF', 'Dhaka', '01911629040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'INV-20161121-1479717683-11', NULL, NULL, NULL, 0.00, 0, 0, 2, 11, '2016-11-21 02:41:23', '2016-11-21 02:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `saleairroute`
--

CREATE TABLE IF NOT EXISTS `saleairroute` (
  `id` int(10) unsigned NOT NULL,
  `Origin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartureTime` datetime DEFAULT NULL,
  `ArrivalTime` datetime DEFAULT NULL,
  `Duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ViaStops` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Stops` int(11) DEFAULT NULL,
  `json_content` text COLLATE utf8_unicode_ci,
  `saleitemair_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `saleairroute`
--

INSERT INTO `saleairroute` (`id`, `Origin`, `Destination`, `DepartureTime`, `ArrivalTime`, `Duration`, `ViaStops`, `Stops`, `json_content`, `saleitemair_id`, `created_at`, `updated_at`) VALUES
(1, 'DAC', 'DEL', '2016-11-15 21:20:00', '2016-11-16 09:10:00', NULL, 'CCU', 2, '{"AirSegment":[{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAqMl1BAAAAA==","Origin":"DAC","Destination":"CCU","DepartureTime":"2016-11-15T21:20:00.000+06:00","ArrivalTime":"2016-11-15T21:50:00.000+05:30","FlightTime":"60","TravelTime":"740","Equipment":"319","DestinationTerminal":"2"}]}],"Key":"oJu1sg4R2BKApMl1BAAAAA==","Group":"0","Carrier":"AI","FlightNumber":"229","Origin":"DAC","Destination":"CCU","DepartureTime":"2016-11-15T21:20:00.000+06:00","ArrivalTime":"2016-11-15T21:50:00.000+05:30","FlightTime":"60","Distance":"146","ETicketability":"Yes","Equipment":"319","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Cached status used. Polled avail exists","OptionalServicesIndicator":"false","AvailabilitySource":"P","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"},{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAsMl1BAAAAA==","Origin":"CCU","Destination":"DEL","DepartureTime":"2016-11-16T07:00:00.000+05:30","ArrivalTime":"2016-11-16T09:10:00.000+05:30","FlightTime":"130","TravelTime":"740","Equipment":"321","OriginTerminal":"2","DestinationTerminal":"3"}]}],"Key":"oJu1sg4R2BKArMl1BAAAAA==","Group":"0","Carrier":"AI","FlightNumber":"763","Origin":"CCU","Destination":"DEL","DepartureTime":"2016-11-16T07:00:00.000+05:30","ArrivalTime":"2016-11-16T09:10:00.000+05:30","FlightTime":"130","Distance":"816","ETicketability":"Yes","Equipment":"321","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Cached status used. Polled avail exists","OptionalServicesIndicator":"false","AvailabilitySource":"P","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}],"Duration":{"y":0,"m":0,"d":0,"h":12,"i":20,"s":0,"weekday":0,"weekday_behavior":0,"first_last_day_of":0,"invert":1,"days":0,"special_type":0,"special_amount":0,"have_weekday_relative":0,"have_special_relative":0},"ArrivalTime":"2016-11-16T09:10:00.000+05:30","DepartureTime":"2016-11-15T21:20:00.000+06:00","Origin":"DAC","Destination":"DEL","ViaStops":"CCU","Stops":2}', 1, '2016-11-21 02:41:23', '2016-11-21 02:41:23'),
(2, 'DEL', 'DAC', '2016-11-20 14:25:00', '2016-11-20 20:20:00', NULL, 'CCU', 2, '{"AirSegment":[{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAuMl1BAAAAA==","Origin":"DEL","Destination":"CCU","DepartureTime":"2016-11-20T14:25:00.000+05:30","ArrivalTime":"2016-11-20T16:35:00.000+05:30","FlightTime":"130","TravelTime":"325","Equipment":"788","OriginTerminal":"3"}]}],"Key":"oJu1sg4R2BKAtMl1BAAAAA==","Group":"1","Carrier":"AI","FlightNumber":"20","Origin":"DEL","Destination":"CCU","DepartureTime":"2016-11-20T14:25:00.000+05:30","ArrivalTime":"2016-11-20T16:35:00.000+05:30","FlightTime":"130","Distance":"816","ETicketability":"Yes","Equipment":"788","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Polled avail used","OptionalServicesIndicator":"false","AvailabilitySource":"S","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"},{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAwMl1BAAAAA==","Origin":"CCU","Destination":"DAC","DepartureTime":"2016-11-20T19:00:00.000+05:30","ArrivalTime":"2016-11-20T20:20:00.000+06:00","FlightTime":"50","TravelTime":"325","Equipment":"319"}]}],"Key":"oJu1sg4R2BKAvMl1BAAAAA==","Group":"1","Carrier":"AI","FlightNumber":"230","Origin":"CCU","Destination":"DAC","DepartureTime":"2016-11-20T19:00:00.000+05:30","ArrivalTime":"2016-11-20T20:20:00.000+06:00","FlightTime":"50","Distance":"146","ETicketability":"Yes","Equipment":"319","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Polled avail used","OptionalServicesIndicator":"false","AvailabilitySource":"S","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}],"Duration":{"y":0,"m":0,"d":0,"h":5,"i":25,"s":0,"weekday":0,"weekday_behavior":0,"first_last_day_of":0,"invert":1,"days":0,"special_type":0,"special_amount":0,"have_weekday_relative":0,"have_special_relative":0},"ArrivalTime":"2016-11-20T20:20:00.000+06:00","DepartureTime":"2016-11-20T14:25:00.000+05:30","Origin":"DEL","Destination":"DAC","ViaStops":"CCU","Stops":2}', 1, '2016-11-21 02:41:24', '2016-11-21 02:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `saleairroutesegment`
--

CREATE TABLE IF NOT EXISTS `saleairroutesegment` (
  `id` int(10) unsigned NOT NULL,
  `Key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Carrier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FlightNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Origin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartureTime` datetime DEFAULT NULL,
  `ArrivalTime` datetime DEFAULT NULL,
  `FlightTime` decimal(5,2) DEFAULT NULL,
  `Distance` decimal(5,2) DEFAULT NULL,
  `DistanceUnits` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ETicketability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Equipment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ChangeOfPlane` tinyint(1) NOT NULL,
  `ParticipantLevel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LinkAvailability` tinyint(1) NOT NULL,
  `PolledAvailabilityOption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OptionalServicesIndicator` tinyint(1) NOT NULL,
  `AvailabilitySource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AvailabilityDisplayType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NumberOfStops` int(11) DEFAULT NULL,
  `DestinationTerminal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TravelTime` decimal(5,2) DEFAULT NULL,
  `BookingCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BookingCount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CabinClass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NotValidBefore` date DEFAULT NULL,
  `NotValidAfter` date DEFAULT NULL,
  `FareRuleKey` text COLLATE utf8_unicode_ci,
  `MaxWeight` decimal(5,2) DEFAULT NULL,
  `WeightUnits` text COLLATE utf8_unicode_ci,
  `json_content` text COLLATE utf8_unicode_ci,
  `saleairroute_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `saleairroutesegment`
--

INSERT INTO `saleairroutesegment` (`id`, `Key`, `Group`, `Carrier`, `FlightNumber`, `Origin`, `Destination`, `DepartureTime`, `ArrivalTime`, `FlightTime`, `Distance`, `DistanceUnits`, `ETicketability`, `Equipment`, `ChangeOfPlane`, `ParticipantLevel`, `LinkAvailability`, `PolledAvailabilityOption`, `OptionalServicesIndicator`, `AvailabilitySource`, `AvailabilityDisplayType`, `NumberOfStops`, `DestinationTerminal`, `TravelTime`, `BookingCode`, `BookingCount`, `CabinClass`, `NotValidBefore`, `NotValidAfter`, `FareRuleKey`, `MaxWeight`, `WeightUnits`, `json_content`, `saleairroute_id`, `created_at`, `updated_at`) VALUES
(1, 'oJu1sg4R2BKApMl1BAAAAA==', '0', 'AI', '229', 'DAC', 'CCU', '2016-11-15 21:20:00', '2016-11-15 21:50:00', 60.00, 146.00, NULL, 'Yes', '319', 0, 'Secure Sell', 1, 'Cached status used. Polled avail exists', 0, 'P', 'Fare Shop/Optimal Shop', NULL, '2', 740.00, 'E', '6', 'Economy', '2016-11-16', '2016-11-16', '6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG052UHcuWlOZd4QMY2jLFGN7ZVCulqMwbpNYRHrzWZukgtlsw9rgBlor7g5RjN/1RhDHq/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTuAdcY0m0a4yk4nKb2G5mOF9qrvOwm/30wMtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK/01BlpTwyo+r9GjARZYw7j/TROTkGAG0MevndHyll0GiOPZ/LoevdgzcF7TvWtaxDyk1kouXkh9S', 20.00, 'Kilograms', '{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAqMl1BAAAAA==","Origin":"DAC","Destination":"CCU","DepartureTime":"2016-11-15T21:20:00.000+06:00","ArrivalTime":"2016-11-15T21:50:00.000+05:30","FlightTime":"60","TravelTime":"740","Equipment":"319","DestinationTerminal":"2"}]}],"Key":"oJu1sg4R2BKApMl1BAAAAA==","Group":"0","Carrier":"AI","FlightNumber":"229","Origin":"DAC","Destination":"CCU","DepartureTime":"2016-11-15T21:20:00.000+06:00","ArrivalTime":"2016-11-15T21:50:00.000+05:30","FlightTime":"60","Distance":"146","ETicketability":"Yes","Equipment":"319","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Cached status used. Polled avail exists","OptionalServicesIndicator":"false","AvailabilitySource":"P","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}', 1, '2016-11-21 02:41:23', '2016-11-21 02:41:23'),
(2, 'oJu1sg4R2BKArMl1BAAAAA==', '0', 'AI', '763', 'CCU', 'DEL', '2016-11-16 07:00:00', '2016-11-16 09:10:00', 130.00, 816.00, NULL, 'Yes', '321', 0, 'Secure Sell', 1, 'Cached status used. Polled avail exists', 0, 'P', 'Fare Shop/Optimal Shop', NULL, '3', 740.00, 'V', '9', 'Economy', '2016-11-16', '2016-11-16', '6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG052UHcuWlOZd4QMY2jLFGN7ZVCulqMwbpNYRHrzWZukgtlsw9rgBlor7g5RjN/1RhDHq/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTuAdcY0m0a4yk4nKb2G5mOF9qrvOwm/30wMtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK/01BlpTwyo+r9GjARZYw7j/TROTkGAG0MevndHyll0GiOPZ/LoevdgzcF7TvWtaxDyk1kouXkh9S', 20.00, 'Kilograms', '{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAsMl1BAAAAA==","Origin":"CCU","Destination":"DEL","DepartureTime":"2016-11-16T07:00:00.000+05:30","ArrivalTime":"2016-11-16T09:10:00.000+05:30","FlightTime":"130","TravelTime":"740","Equipment":"321","OriginTerminal":"2","DestinationTerminal":"3"}]}],"Key":"oJu1sg4R2BKArMl1BAAAAA==","Group":"0","Carrier":"AI","FlightNumber":"763","Origin":"CCU","Destination":"DEL","DepartureTime":"2016-11-16T07:00:00.000+05:30","ArrivalTime":"2016-11-16T09:10:00.000+05:30","FlightTime":"130","Distance":"816","ETicketability":"Yes","Equipment":"321","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Cached status used. Polled avail exists","OptionalServicesIndicator":"false","AvailabilitySource":"P","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}', 1, '2016-11-21 02:41:24', '2016-11-21 02:41:24'),
(3, 'oJu1sg4R2BKAtMl1BAAAAA==', '1', 'AI', '20', 'DEL', 'CCU', '2016-11-20 14:25:00', '2016-11-20 16:35:00', 130.00, 816.00, NULL, 'Yes', '788', 0, 'Secure Sell', 1, 'Polled avail used', 0, 'S', 'Fare Shop/Optimal Shop', NULL, NULL, 325.00, 'V', '9', 'Economy', '2016-11-20', '2016-11-20', '6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG053toauiNSNCmHUtgllpH8EIffAIozri2AARHrzWZukgtlsw9rgBlor79TdlxQRdBHLq/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTs8sv0OdkDoiE4nKb2G5mOFogphBET+lf0MtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK/01BlpTwyo+r9GjARZYw7j/TROTkGAG0MevndHyll0GiOPZ/LoevdgzcF7TvWtaxDyk1kouXkh9S', 20.00, 'Kilograms', '{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAuMl1BAAAAA==","Origin":"DEL","Destination":"CCU","DepartureTime":"2016-11-20T14:25:00.000+05:30","ArrivalTime":"2016-11-20T16:35:00.000+05:30","FlightTime":"130","TravelTime":"325","Equipment":"788","OriginTerminal":"3"}]}],"Key":"oJu1sg4R2BKAtMl1BAAAAA==","Group":"1","Carrier":"AI","FlightNumber":"20","Origin":"DEL","Destination":"CCU","DepartureTime":"2016-11-20T14:25:00.000+05:30","ArrivalTime":"2016-11-20T16:35:00.000+05:30","FlightTime":"130","Distance":"816","ETicketability":"Yes","Equipment":"788","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Polled avail used","OptionalServicesIndicator":"false","AvailabilitySource":"S","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}', 2, '2016-11-21 02:41:24', '2016-11-21 02:41:24'),
(4, 'oJu1sg4R2BKAvMl1BAAAAA==', '1', 'AI', '230', 'CCU', 'DAC', '2016-11-20 19:00:00', '2016-11-20 20:20:00', 50.00, 146.00, NULL, 'Yes', '319', 0, 'Secure Sell', 1, 'Polled avail used', 0, 'S', 'Fare Shop/Optimal Shop', NULL, NULL, 325.00, 'E', '6', 'Economy', '2016-11-20', '2016-11-20', '6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG053toauiNSNCmHUtgllpH8EIffAIozri2AARHrzWZukgtlsw9rgBlor79TdlxQRdBHLq/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTs8sv0OdkDoiE4nKb2G5mOFogphBET+lf0MtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK/01BlpTwyo+r9GjARZYw7j/TROTkGAG0MevndHyll0GiOPZ/LoevdgzcF7TvWtaxDyk1kouXkh9S', 20.00, 'Kilograms', '{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAwMl1BAAAAA==","Origin":"CCU","Destination":"DAC","DepartureTime":"2016-11-20T19:00:00.000+05:30","ArrivalTime":"2016-11-20T20:20:00.000+06:00","FlightTime":"50","TravelTime":"325","Equipment":"319"}]}],"Key":"oJu1sg4R2BKAvMl1BAAAAA==","Group":"1","Carrier":"AI","FlightNumber":"230","Origin":"CCU","Destination":"DAC","DepartureTime":"2016-11-20T19:00:00.000+05:30","ArrivalTime":"2016-11-20T20:20:00.000+06:00","FlightTime":"50","Distance":"146","ETicketability":"Yes","Equipment":"319","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Polled avail used","OptionalServicesIndicator":"false","AvailabilitySource":"S","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}', 2, '2016-11-21 02:41:24', '2016-11-21 02:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `saleairseatmap`
--

CREATE TABLE IF NOT EXISTS `saleairseatmap` (
  `id` int(10) unsigned NOT NULL,
  `passenger_seat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saleairroutesegment_id` int(10) unsigned DEFAULT NULL,
  `saleitemairpassenger_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `saleitemair`
--

CREATE TABLE IF NOT EXISTS `saleitemair` (
  `id` int(10) unsigned NOT NULL,
  `Price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalPrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BasePrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApproximateTotalPrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApproximateBasePrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EquivalentBasePrice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Taxes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ApproximateTaxes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PolicyExclusion` tinyint(1) NOT NULL,
  `Refundable` tinyint(1) NOT NULL,
  `PlatingCarrier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TravelTime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `childs` int(11) DEFAULT NULL,
  `infant` int(11) DEFAULT NULL,
  `tripType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noOfSegments` int(11) DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json_content` text COLLATE utf8_unicode_ci,
  `saleprice` decimal(8,2) NOT NULL,
  `sale_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `saleitemair`
--

INSERT INTO `saleitemair` (`id`, `Price`, `Key`, `TotalPrice`, `BasePrice`, `ApproximateTotalPrice`, `ApproximateBasePrice`, `EquivalentBasePrice`, `Taxes`, `ApproximateTaxes`, `PolicyExclusion`, `Refundable`, `PlatingCarrier`, `TravelTime`, `adults`, `childs`, `infant`, `tripType`, `noOfSegments`, `class`, `json_content`, `saleprice`, `sale_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 'oJu1sg4R2BKAoMl1BAAAAA==', 'BDT21156', 'USD165.00', 'BDT21156', 'BDT12870', 'BDT12870', 'BDT8286', 'BDT8286', 0, 1, 'AI', 'P0DT5H25M0S', 2, 1, 0, 'R', 2, 'Economy', '{"AirPricePoint":{"Summary":[],"Journey":{"Routes":[{"AirSegment":[{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAqMl1BAAAAA==","Origin":"DAC","Destination":"CCU","DepartureTime":"2016-11-15T21:20:00.000+06:00","ArrivalTime":"2016-11-15T21:50:00.000+05:30","FlightTime":"60","TravelTime":"740","Equipment":"319","DestinationTerminal":"2"}]}],"Key":"oJu1sg4R2BKApMl1BAAAAA==","Group":"0","Carrier":"AI","FlightNumber":"229","Origin":"DAC","Destination":"CCU","DepartureTime":"2016-11-15T21:20:00.000+06:00","ArrivalTime":"2016-11-15T21:50:00.000+05:30","FlightTime":"60","Distance":"146","ETicketability":"Yes","Equipment":"319","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Cached status used. Polled avail exists","OptionalServicesIndicator":"false","AvailabilitySource":"P","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"},{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAsMl1BAAAAA==","Origin":"CCU","Destination":"DEL","DepartureTime":"2016-11-16T07:00:00.000+05:30","ArrivalTime":"2016-11-16T09:10:00.000+05:30","FlightTime":"130","TravelTime":"740","Equipment":"321","OriginTerminal":"2","DestinationTerminal":"3"}]}],"Key":"oJu1sg4R2BKArMl1BAAAAA==","Group":"0","Carrier":"AI","FlightNumber":"763","Origin":"CCU","Destination":"DEL","DepartureTime":"2016-11-16T07:00:00.000+05:30","ArrivalTime":"2016-11-16T09:10:00.000+05:30","FlightTime":"130","Distance":"816","ETicketability":"Yes","Equipment":"321","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Cached status used. Polled avail exists","OptionalServicesIndicator":"false","AvailabilitySource":"P","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}],"Duration":{"y":0,"m":0,"d":0,"h":12,"i":20,"s":0,"weekday":0,"weekday_behavior":0,"first_last_day_of":0,"invert":1,"days":0,"special_type":0,"special_amount":0,"have_weekday_relative":0,"have_special_relative":0},"ArrivalTime":"2016-11-16T09:10:00.000+05:30","DepartureTime":"2016-11-15T21:20:00.000+06:00","Origin":"DAC","Destination":"DEL","ViaStops":"CCU","Stops":2},{"AirSegment":[{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAuMl1BAAAAA==","Origin":"DEL","Destination":"CCU","DepartureTime":"2016-11-20T14:25:00.000+05:30","ArrivalTime":"2016-11-20T16:35:00.000+05:30","FlightTime":"130","TravelTime":"325","Equipment":"788","OriginTerminal":"3"}]}],"Key":"oJu1sg4R2BKAtMl1BAAAAA==","Group":"1","Carrier":"AI","FlightNumber":"20","Origin":"DEL","Destination":"CCU","DepartureTime":"2016-11-20T14:25:00.000+05:30","ArrivalTime":"2016-11-20T16:35:00.000+05:30","FlightTime":"130","Distance":"816","ETicketability":"Yes","Equipment":"788","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Polled avail used","OptionalServicesIndicator":"false","AvailabilitySource":"S","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"},{"FlightDetails":[{"FlightDetailsRef":[{"Key":"oJu1sg4R2BKAwMl1BAAAAA==","Origin":"CCU","Destination":"DAC","DepartureTime":"2016-11-20T19:00:00.000+05:30","ArrivalTime":"2016-11-20T20:20:00.000+06:00","FlightTime":"50","TravelTime":"325","Equipment":"319"}]}],"Key":"oJu1sg4R2BKAvMl1BAAAAA==","Group":"1","Carrier":"AI","FlightNumber":"230","Origin":"CCU","Destination":"DAC","DepartureTime":"2016-11-20T19:00:00.000+05:30","ArrivalTime":"2016-11-20T20:20:00.000+06:00","FlightTime":"50","Distance":"146","ETicketability":"Yes","Equipment":"319","ChangeOfPlane":"false","ParticipantLevel":"Secure Sell","LinkAvailability":"true","PolledAvailabilityOption":"Polled avail used","OptionalServicesIndicator":"false","AvailabilitySource":"S","AvailabilityDisplayType":"Fare Shop\\/Optimal Shop"}],"Duration":{"y":0,"m":0,"d":0,"h":5,"i":25,"s":0,"weekday":0,"weekday_behavior":0,"first_last_day_of":0,"invert":1,"days":0,"special_type":0,"special_amount":0,"have_weekday_relative":0,"have_special_relative":0},"ArrivalTime":"2016-11-20T20:20:00.000+06:00","DepartureTime":"2016-11-20T14:25:00.000+05:30","Origin":"DEL","Destination":"DAC","ViaStops":"CCU","Stops":2}],"TravelTime":"P0DT5H25M0S"},"Price":{"TotalPrice":"BDT21156","BasePrice":"USD165.00","ApproximateTotalPrice":"BDT21156","ApproximateBasePrice":"BDT12870","Taxes":"BDT8286","ApproximateTaxes":"BDT8286","BookingInfo":[{"BookingCode":"E","BookingCount":"6","CabinClass":"Economy","FareInfoRef":"oJu1sg4R2BKA7Ml1BAAAAA==","FareInfo":{"Key":"oJu1sg4R2BKA7Ml1BAAAAA==","FareBasis":"ERTBD","PassengerTypeCode":"ADT","Origin":"DAC","Destination":"DEL","EffectiveDate":"2016-11-13T17:29:00.000+11:00","DepartureDate":"2016-11-15","Amount":"BDT6240","NegotiatedFare":"false","NotValidBefore":"2016-11-16","NotValidAfter":"2016-11-16","BaggageAllowance":{"MaxWeight":{"Value":"20","Unit":"Kilograms"}},"FareRuleKey":"6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG052UHcuWlOZd4QMY2jLFGN7ZVCulqMwbpNYRHrzWZukgtlsw9rgBlor7g5RjN\\/1RhDHq\\/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTuAdcY0m0a4yk4nKb2G5mOF9qrvOwm\\/30wMtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+\\/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK\\/01BlpTwyo+r9GjARZYw7j\\/TROTkGAG0MevndHyll0GiOPZ\\/LoevdgzcF7TvWtaxDyk1kouXkh9S"},"SegmentRef":"oJu1sg4R2BKApMl1BAAAAA=="},{"BookingCode":"V","BookingCount":"9","CabinClass":"Economy","FareInfoRef":"oJu1sg4R2BKA7Ml1BAAAAA==","FareInfo":{"Key":"oJu1sg4R2BKA7Ml1BAAAAA==","FareBasis":"ERTBD","PassengerTypeCode":"ADT","Origin":"DAC","Destination":"DEL","EffectiveDate":"2016-11-13T17:29:00.000+11:00","DepartureDate":"2016-11-15","Amount":"BDT6240","NegotiatedFare":"false","NotValidBefore":"2016-11-16","NotValidAfter":"2016-11-16","BaggageAllowance":{"MaxWeight":{"Value":"20","Unit":"Kilograms"}},"FareRuleKey":"6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG052UHcuWlOZd4QMY2jLFGN7ZVCulqMwbpNYRHrzWZukgtlsw9rgBlor7g5RjN\\/1RhDHq\\/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTuAdcY0m0a4yk4nKb2G5mOF9qrvOwm\\/30wMtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+\\/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK\\/01BlpTwyo+r9GjARZYw7j\\/TROTkGAG0MevndHyll0GiOPZ\\/LoevdgzcF7TvWtaxDyk1kouXkh9S"},"SegmentRef":"oJu1sg4R2BKArMl1BAAAAA=="},{"BookingCode":"V","BookingCount":"9","CabinClass":"Economy","FareInfoRef":"oJu1sg4R2BKAENl1BAAAAA==","FareInfo":{"Key":"oJu1sg4R2BKAENl1BAAAAA==","FareBasis":"ERTBD","PassengerTypeCode":"ADT","Origin":"DEL","Destination":"DAC","EffectiveDate":"2016-11-13T17:29:00.000+11:00","DepartureDate":"2016-11-20","Amount":"BDT6630","NegotiatedFare":"false","NotValidBefore":"2016-11-20","NotValidAfter":"2016-11-20","BaggageAllowance":{"MaxWeight":{"Value":"20","Unit":"Kilograms"}},"FareRuleKey":"6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG053toauiNSNCmHUtgllpH8EIffAIozri2AARHrzWZukgtlsw9rgBlor79TdlxQRdBHLq\\/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTs8sv0OdkDoiE4nKb2G5mOFogphBET+lf0MtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+\\/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK\\/01BlpTwyo+r9GjARZYw7j\\/TROTkGAG0MevndHyll0GiOPZ\\/LoevdgzcF7TvWtaxDyk1kouXkh9S"},"SegmentRef":"oJu1sg4R2BKAtMl1BAAAAA=="},{"BookingCode":"E","BookingCount":"6","CabinClass":"Economy","FareInfoRef":"oJu1sg4R2BKAENl1BAAAAA==","FareInfo":{"Key":"oJu1sg4R2BKAENl1BAAAAA==","FareBasis":"ERTBD","PassengerTypeCode":"ADT","Origin":"DEL","Destination":"DAC","EffectiveDate":"2016-11-13T17:29:00.000+11:00","DepartureDate":"2016-11-20","Amount":"BDT6630","NegotiatedFare":"false","NotValidBefore":"2016-11-20","NotValidAfter":"2016-11-20","BaggageAllowance":{"MaxWeight":{"Value":"20","Unit":"Kilograms"}},"FareRuleKey":"6UUVoSldxwiyum0pRqRMZ8bKj3F8T9EyxsqPcXxP0TIjSPOlaHfQe5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA5cuasWd6i8Dly5qxZ3qLwOXLmrFneovA9EuPpZ+OIsZM3ExqSoG053toauiNSNCmHUtgllpH8EIffAIozri2AARHrzWZukgtlsw9rgBlor79TdlxQRdBHLq\\/20RpEmAXlby9IE1rxlSp7GoYpQef1iSylWp28O7S4ncc6B+IR25WSdHfWAsnTs8sv0OdkDoiE4nKb2G5mOFogphBET+lf0MtfEW+An1Pr+F729rtUMfv4Xvb2u1Qx+\\/he9va7VDH7+F729rtUMfv4Xvb2u1Qx80GqQ2wgDK\\/01BlpTwyo+r9GjARZYw7j\\/TROTkGAG0MevndHyll0GiOPZ\\/LoevdgzcF7TvWtaxDyk1kouXkh9S"},"SegmentRef":"oJu1sg4R2BKAvMl1BAAAAA=="}],"ChangePenalty":"BDT1950.0","CancelPenalty":"BDT2340.0"},"Key":"oJu1sg4R2BKAoMl1BAAAAA==","TotalPrice":"BDT21156","BasePrice":"USD165.00","ApproximateTotalPrice":"BDT21156","ApproximateBasePrice":"BDT12870","EquivalentBasePrice":"BDT12870","Taxes":"BDT8286","ApproximateTaxes":"BDT8286","PolicyExclusion":"false","Refundable":"true","PlatingCarrier":"AI"},"SearchParams":{"adults":"2","arrivalDate":"2016-11-25T18:00:00.000Z","childs":"1","class":"Economy","departureDate":"2016-11-23T18:00:00.000Z","destination":"BOM","infant":"0","noOfSegments":"2","origin":"DEL","tripType":"R"}}', 1002.00, 2, '2016-11-21 02:41:23', '2016-11-21 02:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `saleitemairpassenger`
--

CREATE TABLE IF NOT EXISTS `saleitemairpassenger` (
  `id` int(10) unsigned NOT NULL,
  `ticket_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pnr_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_issue_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport_expiry_date` date DEFAULT NULL,
  `passenger_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passenger_order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saleitemair_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `saleitemairpassenger`
--

INSERT INTO `saleitemairpassenger` (`id`, `ticket_key`, `pnr_number`, `title`, `first_name`, `middle_name`, `last_name`, `dob`, `nationality`, `passport_no`, `passport_issue_country`, `passport_expiry_date`, `passenger_type`, `passenger_order`, `saleitemair_id`, `created_at`, `updated_at`) VALUES
(1, '', '1479717683', 'Mr', 'Mohammad', 'Arif', 'Iftekhar', '0000-00-00', 'Bangladeshi', '123456', 'Bangladesh', '0000-00-00', 'adults', '0', 1, '2016-11-21 02:41:23', '2016-11-21 02:41:23'),
(2, '', '1479717683', 'Miss', 'Mahafuzur', NULL, 'Rahaman', '0000-00-00', 'Bangladeshi', '123456', '000', '0000-00-00', 'adults', '1', 1, '2016-11-21 02:41:23', '2016-11-21 02:41:23'),
(3, '', '1479717683', 'Mr', 'Mohammad', '', 'Ifrad', '0000-00-00', NULL, NULL, NULL, NULL, 'childs', '0', 1, '2016-11-21 02:41:23', '2016-11-21 02:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `tpackage`
--

CREATE TABLE IF NOT EXISTS `tpackage` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image1` text COLLATE utf8_unicode_ci,
  `image2` text COLLATE utf8_unicode_ci,
  `image3` text COLLATE utf8_unicode_ci,
  `image4` text COLLATE utf8_unicode_ci,
  `price` decimal(8,2) NOT NULL,
  `airfare_include` tinyint(1) NOT NULL DEFAULT '0',
  `airfare_exclude` tinyint(1) NOT NULL DEFAULT '0',
  `transport_include` tinyint(1) NOT NULL DEFAULT '0',
  `transport_exclude` tinyint(1) NOT NULL DEFAULT '0',
  `property_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotel_rating` int(11) NOT NULL DEFAULT '0',
  `check_in` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `check_out` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pets` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `laundry_service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `free_wifi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smoking_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resturent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dry_cleaning` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hotel_description` text COLLATE utf8_unicode_ci,
  `isactive` tinyint(1) NOT NULL DEFAULT '0',
  `isfeatured` tinyint(1) NOT NULL DEFAULT '0',
  `jcontinent_id` int(10) unsigned DEFAULT NULL,
  `jcountry_id` int(10) unsigned DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tpackage`
--

INSERT INTO `tpackage` (`id`, `code`, `title`, `description`, `image1`, `image2`, `image3`, `image4`, `price`, `airfare_include`, `airfare_exclude`, `transport_include`, `transport_exclude`, `property_type`, `hotel_rating`, `check_in`, `check_out`, `pets`, `laundry_service`, `free_wifi`, `smoking_area`, `resturent`, `dry_cleaning`, `hotel_description`, `isactive`, `isfeatured`, `jcontinent_id`, `jcountry_id`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'PKG-001', 'Chittagong Tour', 'Chittagong Tour', '87561-4.jpg', '30075-2.jpg', '54216-1.jpg', NULL, 5000.00, 1, 0, 1, 0, 'hotel', 2, '12:00 PM', '02:00 PM', 'notallow', 'yes', 'yes', 'yes', 'no', 'yes', 'Hotel', 1, 0, 1, 1, 'chittagong-tour', '2016-08-24 05:43:46', '2016-08-24 05:49:42'),
(2, 'PKG-002', 'Agra Tour', 'Agra Tour', '77965-1.jpg', '79725-2.jpg', '30677-3.jpg', NULL, 35000.00, 0, 1, 1, 0, 'resorts', 3, '12:00 PM', 'Any Time', 'allow', 'yes', 'yes', 'no', 'yes', 'yes', 'Res', 1, 0, 1, 2, 'agra-tour', '2016-08-24 05:56:57', '2016-08-24 05:56:57'),
(3, 'PKG-003', 'London Tour', 'London Tour', '35526-2.jpg', '41003-3.jpg', '18050-4.jpg', NULL, 850000.00, 1, 0, 1, 0, 'cruiseship', 3, '12:00 PM', '2:00 PM', 'allow', 'yes', 'yes', 'yes', 'yes', 'yes', 'Cr', 1, 0, 2, 3, 'london-tour', '2016-08-24 05:58:27', '2016-08-24 05:58:27'),
(4, 'PKG-004', 'Coxbazar', 'Coxbazar', '92935-4.jpg', '67586-1.jpg', '20689-3.jpg', NULL, 10000.00, 0, 1, 1, 0, 'resorts', 1, '12:00 PM', '02:00 PM', 'allow', 'no', 'yes', 'yes', 'yes', 'yes', 'dfsdf', 1, 0, 1, 1, 'coxbazar', '2016-09-04 07:17:38', '2016-09-04 07:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `cngpasscode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `facebook_id`, `name`, `email`, `password`, `remember_token`, `isactive`, `cngpasscode`, `created_at`, `updated_at`) VALUES
(1, '', 'Arif', 'admin@zeteq.com', '$2y$10$PAgWL8gOgUfP/ce0mxmMG.BL1V0lOVtuXuwWXK3wmLfKt4F2gWTKy', 'BdNXTKTRANjURtsTzf697JG0FaBHvTH3ek0EuKNaX09D2eZ3eP9Au3ODXQ0f', 0, NULL, '2016-07-28 03:58:47', '2016-10-03 07:20:13'),
(2, '', 'Agent1', 'agent1@zeteq.com', '$2y$10$G3TovKTNYwPe228ZChtbouKjmiUTuAmWyP8i3Rl0mtfl8UYs1isry', 'KoFPgim4Px9aPAxp30lkvGs60CvqOk6Jiw6cUCoYXilwrJM9IhRBkCUK8NQ5', 0, NULL, '2016-07-28 06:42:32', '2016-07-31 06:35:46'),
(3, '', 'Arifdsg', 'asa@admin.com', '$2y$10$884PzxVzZd7PG7IO0NXOROxFnCCbUHZhPuoUrmcMNs3JajTSzsme6', 'XpddI16dAC5kqhiiYaYKOIhYV1JmaCrYb4wLQcfMFuPbnPz1iQDSc3OAn6h8', 0, NULL, '2016-07-28 06:59:24', '2016-07-28 07:32:21'),
(4, '', 'Arifdsedf', 'aaadmin@admin.com', '$2y$10$F2McoSSR4ZKgZLQ4s9aN4eytPvpEZwfhEIDgOkofIG4sI35qqj/5W', 'THHUdXRniIPS84pkVRsAD66Hkd6MkChr1pTb5XWWEEEuOlb6w8NftbHuWkBH', 0, NULL, '2016-07-28 07:33:14', '2016-09-06 05:59:49'),
(5, '', 'oiuouiu', 'ze@ze.com', '$2y$10$kKtNDmPU6DpZsjn3e4zJrudvaOUH4HfAabf/OocuLusoY2jFVjey6', NULL, 0, NULL, '2016-07-28 07:38:31', '2016-07-28 07:38:31'),
(6, '', 'Abcd', 'abcd@zeteq.com', '$2y$10$BmDX7LvFd/Lrv.BpRYwjbuoNrHjeB3FZQyH9YLFTqUDiK/O4yphBK', '8Cct7jf9XZTccC09AmWsiCNoU5m8dEgQLGEPSM0kY7wIKGg0m3gok97aNpC1', 0, NULL, '2016-07-31 03:39:36', '2016-08-01 05:25:30'),
(7, '', 'dddadmin Agent', 'agn@zeteq.com', '$2y$10$uGnBMiRz0b96IE1U.4h6sekTlyPSGDoqAB.8XCvybvu38DrNcN6mu', 'IR33zKkxGa3uouAYax21LWJAR3NExb7kIESTPnqdGB3f1RgvI0f73gDJXyy7', 0, NULL, '2016-08-01 05:24:30', '2016-08-03 03:59:30'),
(11, '', 'Arif Customer', 'zqarif@gmail.com', '$2y$10$EeExjKUFmqaKGnmTHcCBcODizTe2i1zpTEWnBSVEgYvWlPQhUpOR6', 'bbOHJ8SRoGCUfrA0nibdlexaKZlSuh5WFEu6g54xz4AtQavPjXDP2AEo5H0x', 0, '201611081478607267', '2016-08-08 03:57:14', '2016-11-08 07:29:20'),
(13, '1246534132034668', 'Arif Iftekhar', 'ctgrobin@gmail.com', '', 'P0oae5Bq9yQ1RtbHtvGhlUTqn9KmeWxLl2prJVKdfcgW5qVbsNhpydbBg5Pc', 0, NULL, '2016-09-27 06:20:18', '2016-11-08 07:29:38'),
(14, NULL, 'testuser222', 'testuser@test.com', '$2y$10$Gym/KJTVTU.cq6vXWNDXh.4NDFLx54jZYIuu.qXM5igFuHowDjQm.', 'Ovf3lMsxv751t9uY5fTAWLQh0riH9YWy0EQOCYD1L2DCAcRLB9XvhX6z01dU', 0, NULL, '2016-10-03 05:40:23', '2016-10-23 00:12:15'),
(19, NULL, 'Mr Linkon', 'linkon666@gmail.com', '$2y$10$jQZ9Bf6Q74lzyMYlXy.bd.HxZSa5BZBkRcllAJKACHkHoKfHIHpJ2', NULL, 0, NULL, '2016-10-23 01:41:58', '2016-10-23 01:41:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `agencies_email_unique` (`email`), ADD KEY `agencies_user_id_foreign` (`user_id`);

--
-- Indexes for table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `agents_email_unique` (`email`), ADD KEY `agents_user_id_foreign` (`user_id`), ADD KEY `agents_agency_id_foreign` (`agency_id`);

--
-- Indexes for table `bannerimage`
--
ALTER TABLE `bannerimage`
  ADD PRIMARY KEY (`id`), ADD KEY `bannerimages_agency_id_foreign` (`agency_id`);

--
-- Indexes for table `cardinfo`
--
ALTER TABLE `cardinfo`
  ADD PRIMARY KEY (`id`), ADD KEY `cardinfo_user_id_foreign` (`user_id`), ADD KEY `cardinfo_cart_id_foreign` (`cart_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`), ADD KEY `cart_user_id_foreign` (`user_id`);

--
-- Indexes for table `cartairpassengeritem`
--
ALTER TABLE `cartairpassengeritem`
  ADD PRIMARY KEY (`id`), ADD KEY `cartairpassengeritem_cart_id_foreign` (`cart_id`), ADD KEY `cartairpassengeritem_cartitemair_id_foreign` (`cartitemair_id`), ADD KEY `cartairpassengeritem_cartitemairpassenger_id_foreign` (`cartitemairpassenger_id`);

--
-- Indexes for table `cartairroute`
--
ALTER TABLE `cartairroute`
  ADD PRIMARY KEY (`id`), ADD KEY `cartairroute_cartitemair_id_foreign` (`cartitemair_id`);

--
-- Indexes for table `cartairroutesegment`
--
ALTER TABLE `cartairroutesegment`
  ADD PRIMARY KEY (`id`), ADD KEY `cartairroutesegment_cartairroute_id_foreign` (`cartairroute_id`);

--
-- Indexes for table `cartairseatmap`
--
ALTER TABLE `cartairseatmap`
  ADD PRIMARY KEY (`id`), ADD KEY `cartairseatmap_cartairroutesegment_id_foreign` (`cartairroutesegment_id`), ADD KEY `cartairseatmap_cartitemairpassenger_id_foreign` (`cartitemairpassenger_id`);

--
-- Indexes for table `cartitemair`
--
ALTER TABLE `cartitemair`
  ADD PRIMARY KEY (`id`), ADD KEY `cartitemair_cart_id_foreign` (`cart_id`);

--
-- Indexes for table `cartitemairpassenger`
--
ALTER TABLE `cartitemairpassenger`
  ADD PRIMARY KEY (`id`), ADD KEY `cartitemairpassenger_cartitemair_id_foreign` (`cartitemair_id`);

--
-- Indexes for table `cartitempkg`
--
ALTER TABLE `cartitempkg`
  ADD PRIMARY KEY (`id`), ADD KEY `cartitempkg_tpackage_id_foreign` (`tpackage_id`), ADD KEY `cartitempkg_cart_id_foreign` (`cart_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `customers_email_unique` (`email`), ADD KEY `customers_user_id_foreign` (`user_id`), ADD KEY `customers_agency_id_foreign` (`agency_id`), ADD KEY `customers_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `jcontinent`
--
ALTER TABLE `jcontinent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jcountry`
--
ALTER TABLE `jcountry`
  ADD PRIMARY KEY (`id`), ADD KEY `jcountry_jcontinent_id_foreign` (`jcontinent_id`);

--
-- Indexes for table `passengerforselect`
--
ALTER TABLE `passengerforselect`
  ADD PRIMARY KEY (`id`), ADD KEY `passengerforselect_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pcategory`
--
ALTER TABLE `pcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pcategory_tpackage`
--
ALTER TABLE `pcategory_tpackage`
  ADD KEY `pcategory_tpackage_pcategory_id_foreign` (`pcategory_id`), ADD KEY `pcategory_tpackage_tpackage_id_foreign` (`tpackage_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`), ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`), ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`id`), ADD KEY `sale_cardinfo_id_foreign` (`cardinfo_id`), ADD KEY `sale_user_id_foreign` (`user_id`);

--
-- Indexes for table `saleairroute`
--
ALTER TABLE `saleairroute`
  ADD PRIMARY KEY (`id`), ADD KEY `saleairroute_saleitemair_id_foreign` (`saleitemair_id`);

--
-- Indexes for table `saleairroutesegment`
--
ALTER TABLE `saleairroutesegment`
  ADD PRIMARY KEY (`id`), ADD KEY `saleairroutesegment_saleairroute_id_foreign` (`saleairroute_id`);

--
-- Indexes for table `saleairseatmap`
--
ALTER TABLE `saleairseatmap`
  ADD PRIMARY KEY (`id`), ADD KEY `saleairseatmap_saleairroutesegment_id_foreign` (`saleairroutesegment_id`), ADD KEY `saleairseatmap_saleitemairpassenger_id_foreign` (`saleitemairpassenger_id`);

--
-- Indexes for table `saleitemair`
--
ALTER TABLE `saleitemair`
  ADD PRIMARY KEY (`id`), ADD KEY `saleitemair_sale_id_foreign` (`sale_id`);

--
-- Indexes for table `saleitemairpassenger`
--
ALTER TABLE `saleitemairpassenger`
  ADD PRIMARY KEY (`id`), ADD KEY `saleitemairpassenger_saleitemair_id_foreign` (`saleitemair_id`);

--
-- Indexes for table `tpackage`
--
ALTER TABLE `tpackage`
  ADD PRIMARY KEY (`id`), ADD KEY `tpackage_jcontinent_id_foreign` (`jcontinent_id`), ADD KEY `tpackage_jcountry_id_foreign` (`jcountry_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `agent`
--
ALTER TABLE `agent`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bannerimage`
--
ALTER TABLE `bannerimage`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cardinfo`
--
ALTER TABLE `cardinfo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cartairpassengeritem`
--
ALTER TABLE `cartairpassengeritem`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cartairroute`
--
ALTER TABLE `cartairroute`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cartairroutesegment`
--
ALTER TABLE `cartairroutesegment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `cartairseatmap`
--
ALTER TABLE `cartairseatmap`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cartitemair`
--
ALTER TABLE `cartitemair`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cartitemairpassenger`
--
ALTER TABLE `cartitemairpassenger`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `cartitempkg`
--
ALTER TABLE `cartitempkg`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jcontinent`
--
ALTER TABLE `jcontinent`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jcountry`
--
ALTER TABLE `jcountry`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `passengerforselect`
--
ALTER TABLE `passengerforselect`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pcategory`
--
ALTER TABLE `pcategory`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sale`
--
ALTER TABLE `sale`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `saleairroute`
--
ALTER TABLE `saleairroute`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `saleairroutesegment`
--
ALTER TABLE `saleairroutesegment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `saleairseatmap`
--
ALTER TABLE `saleairseatmap`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `saleitemair`
--
ALTER TABLE `saleitemair`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `saleitemairpassenger`
--
ALTER TABLE `saleitemairpassenger`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tpackage`
--
ALTER TABLE `tpackage`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `agency`
--
ALTER TABLE `agency`
ADD CONSTRAINT `agencies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `agent`
--
ALTER TABLE `agent`
ADD CONSTRAINT `agents_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`),
ADD CONSTRAINT `agents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `bannerimage`
--
ALTER TABLE `bannerimage`
ADD CONSTRAINT `bannerimages_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`);

--
-- Constraints for table `cartairroute`
--
ALTER TABLE `cartairroute`
ADD CONSTRAINT `cartairroute_cartitemair_id_foreign` FOREIGN KEY (`cartitemair_id`) REFERENCES `cartitemair` (`id`);

--
-- Constraints for table `cartairroutesegment`
--
ALTER TABLE `cartairroutesegment`
ADD CONSTRAINT `cartairroutesegment_cartairroute_id_foreign` FOREIGN KEY (`cartairroute_id`) REFERENCES `cartairroute` (`id`);

--
-- Constraints for table `cartairseatmap`
--
ALTER TABLE `cartairseatmap`
ADD CONSTRAINT `cartairseatmap_cartairroutesegment_id_foreign` FOREIGN KEY (`cartairroutesegment_id`) REFERENCES `cartairroutesegment` (`id`),
ADD CONSTRAINT `cartairseatmap_cartitemairpassenger_id_foreign` FOREIGN KEY (`cartitemairpassenger_id`) REFERENCES `cartitemairpassenger` (`id`);

--
-- Constraints for table `cartitemair`
--
ALTER TABLE `cartitemair`
ADD CONSTRAINT `cartitemair_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`);

--
-- Constraints for table `cartitemairpassenger`
--
ALTER TABLE `cartitemairpassenger`
ADD CONSTRAINT `cartitemairpassenger_cartitemair_id_foreign` FOREIGN KEY (`cartitemair_id`) REFERENCES `cartitemair` (`id`);

--
-- Constraints for table `cartitempkg`
--
ALTER TABLE `cartitempkg`
ADD CONSTRAINT `cartitempkg_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `cartitempkg_tpackage_id_foreign` FOREIGN KEY (`tpackage_id`) REFERENCES `tpackage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
ADD CONSTRAINT `customers_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`),
ADD CONSTRAINT `customers_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`),
ADD CONSTRAINT `customers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `jcountry`
--
ALTER TABLE `jcountry`
ADD CONSTRAINT `jcountry_jcontinent_id_foreign` FOREIGN KEY (`jcontinent_id`) REFERENCES `jcontinent` (`id`);

--
-- Constraints for table `passengerforselect`
--
ALTER TABLE `passengerforselect`
ADD CONSTRAINT `passengerforselect_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pcategory_tpackage`
--
ALTER TABLE `pcategory_tpackage`
ADD CONSTRAINT `pcategory_tpackage_pcategory_id_foreign` FOREIGN KEY (`pcategory_id`) REFERENCES `pcategory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `pcategory_tpackage_tpackage_id_foreign` FOREIGN KEY (`tpackage_id`) REFERENCES `tpackage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `saleairroutesegment`
--
ALTER TABLE `saleairroutesegment`
ADD CONSTRAINT `saleairroutesegment_saleairroute_id_foreign` FOREIGN KEY (`saleairroute_id`) REFERENCES `saleairroute` (`id`);

--
-- Constraints for table `tpackage`
--
ALTER TABLE `tpackage`
ADD CONSTRAINT `tpackage_jcontinent_id_foreign` FOREIGN KEY (`jcontinent_id`) REFERENCES `jcontinent` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `tpackage_jcountry_id_foreign` FOREIGN KEY (`jcountry_id`) REFERENCES `jcountry` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
