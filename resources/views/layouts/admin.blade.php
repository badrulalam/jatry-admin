<!doctype html>
<html class="no-js" lang="" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>travel port</title>

    <link rel="stylesheet" href="{{asset('/css/vendor.css') }}">
    <link rel="stylesheet" href="{{asset('/css/resources.css') }}">
    <link rel="stylesheet" href="{{asset('/css/app.css') }}">
</head>

<body id="app-layout">


    @include('includes.adminheader')
    @yield('content')
    @yield('fotter')



<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/resources.js') }}"></script>

</body>
</html>
