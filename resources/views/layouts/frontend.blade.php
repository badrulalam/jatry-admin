<!doctype html>
<html class="no-js" lang="" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>travel port</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/vendor.css') }}">
    <link rel="stylesheet" href="{{asset('/css/resources.css') }}">
    <link rel="stylesheet" href="{{asset('/css/app.css') }}">
</head>

<body id="app-layout">
@include('includes.frontheader')
<div class="wrapper">
    @yield('content')
    @yield('fotter')

    {{--@yield('page-script')--}}
</div>


<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="//bowercdn.net/c/jquery-bridget-2.0.0/jquery-bridget.js"></script>
<script src="{{ asset('/js/resources.js') }}"></script>
{{--<script src={{asset('./vendor/bower_components/owl.carousel/dist/owl.carousel.min.js')}}></script>--}}

@yield('page-script')
</body>
</html>
