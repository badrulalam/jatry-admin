@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Users Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('newrole') }}"> Create New Role</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div>
                    @if (Auth::user()->hasRole('agency'))
                        <b>Yes yes</b>
                    @else
                        <b>No No</b>
                    @endif
                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Description</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($roles as $key => $role)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->display_name }}</td>
                                <td>{{ $role->description }}</td>
                                <td>
                                    {{--<a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>--}}
                                    @permission('role-edit')
                                    <a class="btn btn-primary" href="{{ route('roleedit',$role->id) }}">Edit</a>
                                    @endpermission
                                    @permission('role-delete')
                                    {!! Form::open(['method' => 'DELETE','route' => ['roledelete', $role->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    @endpermission
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $roles->render() !!}

            </div>
        </div>
    </div>
@endsection
