@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Agent Register</div>

                <div class="panel-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    {!! Form::open(array('route' => 'agentregistersave','method'=>'POST', 'class' => 'form-horizontal')) !!}
                    <fieldset>
                        <legend>Login Information</legend>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                        <div class="col-md-6">
                            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    </fieldset>


                    <fieldset>
                        <legend>Agency Information</legend>
                        <div class="form-group{{ $errors->has('agency_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                {!! Form::text('agency_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                @if ($errors->has('agency_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('agency_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('agency_email') ? ' has-error' : '' }}">
                            <label for="agency_email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                {!! Form::text('agency_email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                @if ($errors->has('agency_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('agency_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('agency_url') ? ' has-error' : '' }}">
                            <label for="agency_url" class="col-md-4 control-label">Agency URL</label>
                            <div class="col-md-6">
                                {!! Form::text('agency_url', null, array('placeholder' => 'Agency URL','class' => 'form-control')) !!}
                                @if ($errors->has('agency_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('agency_url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                    </fieldset>

                    <fieldset>
                        <legend>Agent Information</legend>

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">First Name</label>
                            <div class="col-md-6">
                                {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Last Name</label>
                            <div class="col-md-6">
                                {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('agent_email') ? ' has-error' : '' }}">
                            <label for="agent_email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                {!! Form::text('agent_email', null, array('placeholder' => 'Agent Email','class' => 'form-control')) !!}

                                @if ($errors->has('agent_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('agent_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('agent_url') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Agent url</label>
                            <div class="col-md-6">
                                {!! Form::text('agent_url', null, array('placeholder' => 'Agent URL','class' => 'form-control')) !!}
                                @if ($errors->has('agent_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('agent_url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                      </fieldset>
                    <br/>
                    <fieldset>
                        {{--<legend></legend>--}}
                    <div class="form-group">

                        <div class="col-md-6 col-md-offset-4">
                            <br/>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Save
                            </button>
                        </div>
                    </div>
                    </fieldset>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
