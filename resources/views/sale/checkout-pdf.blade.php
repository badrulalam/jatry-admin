<html>
<head>
    <style>
        table.border {
            border-collapse: collapse;
        }

        table.border th, table.border td {
            border: 1px solid black;
            padding: 5px;
        }

        .logo {
            width: 200px;
        }
    </style>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <title>Example 2</title>

</head>


<body>

<div class="container">
    <div>
        <img src="img/bg-logo.png" alt="" class="logo">
        <div class="panel-heading">
            <div style="font-size: 24px;font-weight: bold;text-align: center;padding: 5px">Invoice
                : {{$sale->order_number}}</div>
        </div>
    </div>
    <div class="panel panel-default">


        <h2>Route List</h2>
        <div class="panel-body">
            @foreach ($sale->ticket->route as $route_index => $route)
                <div>
                    <div>
                        <h4>{{$route_index+1}}. {{$route->Origin}} -> {{$route->Destination}}</h4>
                    </div>
                    <table class="border" width="100%">
                        <thead>
                        <tr>
                            <th width="15%">Airline</th>
                            <th width="25%">Depart</th>
                            <th width="25%">Arrive</th>
                            <th width="25%">Details</th>
                        </tr>
                        </thead>
                        @for ($i = 0; $i < count($route->segment); $i++)
                            <tr>
                                <td width="15%">
                                <span><img src="{{'api.travelport/images/' . $route->segment[$i]->Carrier . '.png'}}"
                                           alt="not found" style="height: 40px;"></span>

                                    {{$route->segment[$i]->Carrier}}
                                </td>
                                <td width="25%">

                                    {{$route->segment[$i]->Origin}}<br>
                                    {{ Carbon\Carbon::parse($route->segment[$i]->DepartureTime)->format('H:i') }}<br>
                                    {{ Carbon\Carbon::parse($route->segment[$i]->DepartureTime)->format('D, d M, Y') }}
                                    <br>
                                </td>

                                <td width="25%">
                                    {{$route->segment[$i]->Destination}}<br>
                                    {{ Carbon\Carbon::parse($route->segment[$i]->ArrivalTime)->format('H:i') }}<br>
                                    {{ Carbon\Carbon::parse($route->segment[$i]->ArrivalTime)->format('D, d M, Y') }}
                                    <br>
                                </td>
                                <td width="35%">
                                    Flight Time : {{number_format($route->segment[$i]->FlightTime,0)}} minutes
                                    <br> Class : {{$route->segment[$i]->CabinClass}}
                                    <br> FlightNumber : {{$route->segment[$i]->Carrier}} - {{$route->segment[$i]->FlightNumber}}
                                    @if(isset($route->segment[$i]->DestinationTerminal))
                                        <br> Destination Terminal : {{$route->segment[$i]->DestinationTerminal}}
                                    @endif
                                    <br> MaxWeight : {{number_format($route->segment[$i]->MaxWeight,0)}}  {{$route->segment[$i]->WeightUnits}}

                                </td>

                            </tr>
                            @if ($i < count($route->segment[$i]) && isset($route->segment[$i]) && isset($route->segment[$i+1]))

                                <tr>
                                    <td colspan="4" align="center">
                                        <div class="bg-warning pm-border center">
                                            <p class="text-center">Layover: {{$route->segment[$i]->Destination}}
                                                |
                                                {{ date('G:i', strtotime($route->segment[$i+1]->DepartureTime) - strtotime($route->segment[$i]->ArrivalTime)) }}
                                            </p>
                                        </div>

                                    </td>
                                </tr>
                            @endif
                        @endfor
                    </table>

                </div>
            @endforeach
        </div>
    </div>

    <div class="panel panel-default">

        <h2>Traveller List</h2>

        <div class="panel-body">

            <table class="border">
                <tr>
                    <th></th>
                    <th>Name</th>
                    {{--<th>First Name</th>--}}
                    {{--<th>Middle Name</th>--}}
                    {{--<th>Last Name</th>--}}
                    <th>DOB</th>
                    <th>Nationality</th>
                    <th>Passport No.</th>
                    <th>Issuing Country</th>
                    <th>Passport Expiry Date</th>

                </tr>
                @foreach ($sale->ticket->passenger as $person)
                    <tr>
                        <td>{{$person->passenger_type.' '.($person->passenger_order + 1)}}</td>

                        <td>{{$person->title.' '.$person->first_name.' '.$person->middle_name.' '.$person->last_name}}</td>
                        <td> {{ Carbon\Carbon::parse($person->dob)->format('d M, Y') }}</td>
                        <td>{{$person->nationality}}</td>
                        <td>{{$person->passport_no}}</td>
                        <td>{{$person->passport_issue_country}}</td>
                        <td> {{ Carbon\Carbon::parse($person->passport_expiry_date)->format('d M, Y') }}</td>

                    </tr>
                @endforeach

            </table>
            {{--<a href="/checkout-final" class="btn btn-default">Continue</a>--}}
        </div>
    </div>
    <div>
        <h2>Price</h2>
        <span>
            Base Price : {{$sale->ticket->ApproximateBasePrice}}
            <br>Taxes : {{$sale->ticket->ApproximateTaxes}}
            <br>Total Price : {{$sale->ticket->ApproximateTotalPrice}}
        </span>
    </div>
</div>
</body>