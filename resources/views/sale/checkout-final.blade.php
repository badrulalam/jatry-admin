@extends('layouts.frontend')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">


            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-heading">  <div style="font-size: 24px;font-weight: bold;text-align: center;padding: 5px">Invoice : {{$sale->order_number}}</div></div>
                    <div class="panel-heading">Route List</div>
                    <div class="panel-body">
                        @foreach ($sale->ticket->route as $route_index => $route)
                            <div>
                                <div>
                                    <h4>{{$route_index+1}}. {{$route->Origin}} -> {{$route->Destination}}</h4>
                                </div>
                                @for ($i = 0; $i < count($route->segment); $i++)
                                    <div>
                                        <div class="row pm-border">
                                            <div class="col-sm-3">
                                <span><img src="{{'/api.travelport/images/' . $route->segment[$i]->Carrier . '.png'}}"
                                           alt="not found" style="height: 40px;"></span>

                                                {{$route->segment[$i]->Carrier}}
                                            </div>
                                            <div class="col-sm-3">

                                                {{$route->segment[$i]->Origin}}<br>
                                                {{ Carbon\Carbon::parse($route->segment[$i]->DepartureTime)->format('H:i') }}<br>
                                                {{ Carbon\Carbon::parse($route->segment[$i]->DepartureTime)->format('D, d M, Y') }}<br>
                                            </div>
                                            <div class="col-sm-3">
                                                {{$route->segment[$i]->FlightTime}} minutes | {{'Non Stop'}} | {{'free meal'}}
                                                {{--<hr>--}}
                                                {{--{{getBookingClass($route->segment[$i]->Key)}} | {{parse.Refundable == 'true'?--}}
                                                {{--'Refundable':'Non Refundable'}}--}}

                                            </div>
                                            <div class="col-sm-3">
                                                {{$route->segment[$i]->Destination}}<br>
                                                {{ Carbon\Carbon::parse($route->segment[$i]->ArrivalTime)->format('H:i') }}<br>
                                                {{ Carbon\Carbon::parse($route->segment[$i]->ArrivalTime)->format('D, d M, Y') }}<br>
                                            </div>

                                        </div>
                                        <div>
                                            @if ($i < count($route->segment[$i]) && isset($route->segment[$i]) && isset($route->segment[$i+1]))
                                                <div class="bg-warning pm-border center">
                                                    <p class="text-center">Layover: {{$route->segment[$i]->Destination}}
                                                        |
                                                        {{ date('G:i', strtotime($route->segment[$i+1]->DepartureTime) - strtotime($route->segment[$i]->ArrivalTime)) }}
                                                    </p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-default" >

                    <div class="panel-heading">Traveller List</div>

                    <div class="panel-body">

                        <table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>DOB</th>
                                <th>Nationality</th>
                                <th>Passport No.</th>
                                <th>Issuing Country</th>
                                <th>Passport Expiry Date</th>

                            </tr>
                            @foreach ($sale->ticket->passenger as $person)
                                <tr>
                                    <td>{{$person->passenger_type.' '.($person->passenger_order + 1)}}</td>
                                    <td>{{$person->title}}</td>
                                    <td>{{$person->first_name}}</td>
                                    <td>{{$person->middle_name}}</td>
                                    <td>{{$person->last_name}}</td>
                                    <td>{{$person->dob}}</td>
                                    <td>{{$person->nationality}}</td>
                                    <td>{{$person->passport_no}}</td>
                                    <td>{{$person->passport_issue_country}}</td>
                                    <td>{{$person->passport_expiry_date}}</td>

                                </tr>
                            @endforeach

                        </table>
                        {{--<a href="/checkout-final" class="btn btn-default">Continue</a>--}}
                    </div>
                </div>
            </div>



            <a href="{{ route('salepdf',$sale->order_number) }}" class="btn btn-primary">Pdf</a>

        </div>
    </div>

@endsection
