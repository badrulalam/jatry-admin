<nav class="navbar navbar-static-top" id="main_navbar" >
    <div class="container">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{Route('home')}}">
                    <span><img src="/img/bg-logo.png" alt="JATRY" style="height: 60px"></span>
                </a>
            </div>
            {{--<!-- Left Side Of Navbar -->--}}
            {{--<ul class="nav navbar-nav">--}}
                {{--<li><a href="{{ Route('home') }}">Home</a></li>--}}
            {{--</ul>--}}

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li style="display: none;"><a href="{{ Route('country-tour') }}">Tour</a></li>
                <li style="display: none;"><a href="{{ Route('all-country-tour') }}">Country</a></li>

                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li style="display: none;"><a href="{{ Route('frontagentlogin') }}">Agent Login</a></li>
                    <li style="display: none;"><a href="{{ Route('agentregister') }}">Agent Register</a></li>
                    <li><a href="{{ Route('customerregister') }}">Register</a></li>
                    <li><a href="{{ Route('customerlogin') }}">Login</a></li>
                    {{--<li><a href="{{ Route('customerregister') }}">{{dd(MyFuncs::Hostname())}}</a></li>--}}
{{--                    <li><a href="{{ Route('customerregister') }}">{{MyFuncs::Hostname()}}</a></li>--}}
                    {{--<li><a href="{{ Route('country-tour') }}">Tour</a></li>--}}
{{--                    <li><a href="{{ Route('customerregister') }}">{{dd(gettype(MyFuncs::getAgency()))}}</a></li>--}}
                    {{--<li><a href="{{ Route('customerregister') }}">{{MyFuncs::getAgency()? MyFuncs::getAgency()->user->id : 'No'}}</a></li>--}}
                @else

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            My account<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @role('agent')
                            <li><a href="{{ Route('customermyaccount').'#/billingAddress' }}">Ag Profile</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Ag Travelers</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Ag Change password</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Ag Payment info</a></li>

                            @endrole
                            @role('customer')
                            <li><a href="{{ Route('customermyaccount').'#/billingAddress' }}">Profile</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Travelers</a></li>
                            <li><a href="{{ Route('userpasschange') }}">Change password</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Payment info</a></li>
                            <li><a href="{{ Route('customermyaccount') }}">Billing info</a></li>
                            @endrole

                        </ul>
                    </li>





                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }}<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

