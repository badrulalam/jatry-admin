<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Jatry
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="#" class="for-border">Dashboard<span class="sr-only">(current)</span></a></li>

                @if (Auth::user()->hasRole('superadmin'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">User Management<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{Route('allusers')}}">User</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('allroles')}}">Role</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('allpermission')}}">Permission</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>

                @endif



                @if (Auth::user()->hasRole('superadmin'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Agency<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{Route('allagencylist')}}">Index</a></li>
                            <li role="separator" class="divider"></li>
                            @if (Auth::user()->hasRole(['superadmin', 'superagent']))
                                <li><a href="{{Route('agencyaddagent')}}">New (For Agency)</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{Route('allagencylist')}}">Index</a></li>
                                <li><a href="{{Route('agencyagentlist')}}">Index(For Agency)</a></li>
                                <li role="separator" class="divider"></li>
                            @endif

                            <li><a href="{{Route('allcustomerlist')}}">Customer</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                @endif


                @if (Auth::user()->hasRole(['superadmin', 'superagent']))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Banner<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{Route('allbannerimage')}}">Index</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                @endif

                @if (Auth::user()->hasRole('superadmin'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Package<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{Route('allpcategory')}}">Category</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('alljcontinent')}}">Continent</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('alljcountry')}}">Country</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('allpcategory')}}">Package</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                @endif


                {{--@if (Auth::user()->hasRole('superadmin'))--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                           {{--aria-haspopup="true" aria-expanded="false">Sale<span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="{{Route('newpermission')}}">New</a></li>--}}
                            {{--<li role="separator" class="divider"></li>--}}
                            {{--<li><a href="{{Route('allpermission')}}">Index</a></li>--}}
                            {{--<li role="separator" class="divider"></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--@endif--}}

                @if (Auth::user()->hasRole('superadmin'))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Domestic<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{Route('domesticairlineindex')}}">Airline</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('domesticcityindex')}}">City</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{Route('domesticflightindex')}}">Flight</a></li>
                            <li role="separator" class="divider"></li>

                        </ul>
                    </li>
                @endif


                @if (Auth::user()->hasRole('superadmin'))
                    <li><a href="{{Route('admin-search')}}">Travelport Search</a></li>
                    <li><a href="{{Route('amadeus-search')}}">Amadeus Search</a></li>
                    <li><a href="{{Route('qpx-search')}}">Google Flight Search</a></li>
                @endif


                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                       {{--aria-haspopup="true" aria-expanded="false">Logs<span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="#">Action</a></li>--}}
                        {{--<li><a href="#">Another action</a></li>--}}
                        {{--<li><a href="#">Something else here</a></li>--}}
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li><a href="#">Separated link</a></li>--}}
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li><a href="#">One more separated link</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>