@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-4">
                            <div class="dash-block">
                        <h4>User Management</h4>
                          <ul>
                            <li><a href="{{Route('allusers')}}">User</a></li>
                            <li><a href="{{Route('allroles')}}">Role</a></li>
                            <li><a href="{{Route('allpermission')}}">Permission</a></li>
                          </ul>

                         </div>
                         </div>
                        <div class="col-md-4">
                            <div class="dash-block">
                                <h4>Agency</h4>
                                <ul>
                                    <li><a href="{{Route('allagencylist')}}">Index</a></li>
                                    @if (Auth::user()->hasRole(['superadmin', 'superagent']))
                                        <li><a href="{{Route('agencyaddagent')}}">New (For Agency)</a></li>
                                        <li><a href="{{Route('allagencylist')}}">Index</a></li>
                                        <li><a href="{{Route('agencyagentlist')}}">Index(For Agency)</a></li>
                                    @endif
                                    <li><a href="{{Route('allcustomerlist')}}">Customer</a></li>
                                </ul>

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="dash-block">
                                <h4>Domestic</h4>
                                <ul>
                                    <li><a href="{{Route('domesticairlineindex')}}">Airline</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{Route('domesticcityindex')}}">City</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{Route('domesticflightindex')}}">Flight</a></li>
                                    <li role="separator" class="divider"></li>

                                </ul>
                            </div>
                        </div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
