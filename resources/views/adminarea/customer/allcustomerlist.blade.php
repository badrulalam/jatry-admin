@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Agency Management</h2>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div>
                    @if (Auth::user()->hasRole('agency'))
                        <b>Yes yes</b>
                    @else
                        <b>No No</b>
                    @endif
                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Jatry</th>
                            <th>Agency</th>
                            <th>Agent</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($data as $key => $customer)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $customer->code }}</td>
                                <td>{{ $customer->first_name.' '.$customer->name }}</td>
                                <td>{{ $customer->email }}</td>
                                <td>{{ $customer->address }}</td>
                                <td>{{ $customer->byjatry ? 'Yes':'No' }}</td>
                                <td>{{ $customer->byagency ? 'Yes':'No' }}</td>
                                <td>{{ $customer->byagent ? 'Yes':'No' }}</td>

                                <td>
                                    <a class="btn btn-primary" href="{{ route('allcustomersale',$customer->user->id) }}">Show Invoice({{count($customer->user->sale)}})</a>

                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
