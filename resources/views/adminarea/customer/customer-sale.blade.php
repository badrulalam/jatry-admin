@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Sale Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('allcustomerlist') }}"> Back </a>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div>

                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Invoice</th>
                            <th>Date</th>
                            <th>Total</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($data as $key => $sale)
                            <tr>

                                <td>{{ ++$i }}</td>
                                <td>{{ $sale->order_number }}</td>
                                <td>{{ $sale->created_at }}</td>
                                <td>{{ $sale->total }}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('saleinvoice',$sale->order_number) }}">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
