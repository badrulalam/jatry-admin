@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Banner Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('newbannerimage') }}"> Create New banner</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($data as $key => $bimage)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $bimage->title }}</td>
                            <td>{{ $bimage->isactive ? 'Yes':'No' }}</td>
                            <td>
                                @if ($bimage->image)
                                    {{ Html::image('/api/image/banner-image/'.$bimage->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 70)) }}
                                @endif
                            </td>
                            <td>

                                <a class="btn btn-primary" href="{{ route('bannerimageedit',$bimage->id) }}">Edit</a>

                                {!! Form::open(['method' => 'DELETE','route' => ['bannerimagedelete', $bimage->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
