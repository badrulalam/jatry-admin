@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Users Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('allpermission') }}"> Back</a>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit permission</div>
                    <div class="panel-body">

                        {!! Form::model($bannerimage, ['method' => 'PATCH','route' => ['bannerimageupdate', $bannerimage->id], 'enctype'=>'multipart/form-data', 'class' => 'form-horizontal']) !!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                {!! Form::text('title', $bannerimage->title, array('placeholder' => 'Title','class' => 'form-control')) !!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Image</label>
                            <div class="col-md-4">
                                {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                {{ Form::file('image', ['class' => 'form-control1', 'id' => 'image']) }}
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-2">
                                @if ($bannerimage->image)
                                      {{ Html::image('/api/image/banner-image/'.$bannerimage->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 50)) }}
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('isactive') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <br/>

                                <label>{{ Form::checkbox('isactive', 1 ,  $bannerimage->isactive, array('class' => 'name')) }}
                                    Active</label>


                                @if ($errors->has('isactive'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('isactive') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
