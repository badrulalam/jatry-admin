@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Package Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('alljcountry') }}"> Back</a>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Country</div>
                    <div class="panel-body">

                        {!! Form::model($jcountry, ['method' => 'PATCH','route' => ['jcountryupdate', $jcountry->id], 'enctype'=>'multipart/form-data', 'class' => 'form-horizontal']) !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                {!! Form::text('name', $jcountry->name, array('placeholder' => 'Title','class' => 'form-control')) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                {!! Form::textarea('description', $jcountry->description, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Image</label>
                            <div class="col-md-4">
                                {{--<input id="customer_image" type="file" class="form-control1" name="customer_image" >--}}
                                {{ Form::file('image', ['class' => 'form-control1', 'id' => 'image']) }}
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-2">
                                @if ($jcountry->image)
                                      {{ Html::image('/api/image/jcountry-image/'.$jcountry->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 50)) }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('continent') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Continent</label>
                            <div class="col-md-6">
                                {!! Form::select('continent', $continent, $jcountry->jcontinent_id, array('class' => 'form-control', 'id'=>'continent')) !!}
                                @if ($errors->has('continent'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('continent') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>





                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
