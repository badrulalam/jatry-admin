@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Domestic Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('domesticflightnew') }}">Create New</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Orgin</th>
                        <th>Destination</th>

                        <th>Days</th>
                        <th>Price</th>
                        <th width="150px">Action</th>
                    </tr>
                    @foreach ($data as $key => $flight)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $flight->name }}</td>
                            <td>
                                {{ $flight->orgincity->name }}<br/>
                                {{ $flight->Deperture_time }}
                            </td>
                            <td>
                                {{ $flight->destinationcity->name }}<br/>
                                {{ $flight->Arrival_time }}
                            </td>

                            <td>
                                @if ($flight->daily)
                                    Daily

                                @else

                                    @if ($flight->sat)
                                        Satarday,
                                    @endif
                                    @if ($flight->sun)
                                        Sunday,
                                    @endif
                                    @if ($flight->mon)
                                        Monday,
                                    @endif
                                     @if ($flight->tue)
                                           Tuesday,
                                        @endif
                                        @if ($flight->wed)
                                           Wednesday,
                                        @endif
                                        @if ($flight->thu)
                                            Thusday,
                                        @endif
                                        @if ($flight->fri)
                                            Friday
                                        @endif


                                @endif

                            </td>
                            <td>{{ $flight->price }}</td>
                            <td style=" vertical-align: middle;">

                                <a class="btn btn-primary" href="{{ route('domesticflightedit',$flight->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['domesticflightdelete', $flight->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
