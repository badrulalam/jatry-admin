@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Domestic Management</h2>
                </div>
                <div class="pull-right">
                    {{--<a class="btn btn-success" href="{{ route('allusers') }}"> Back </a>--}}
                    <a class="btn btn-primary" href="{{ route('domesticflightindex') }}"> Back</a>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new Flight</div>
                    <div class="panel-body">

                        {!! Form::open(array('route' => 'domesticflightcreate','method'=>'POST','enctype'=>'multipart/form-data', 'class' => 'form-horizontal')) !!}
                        <fieldset>
                            <legend>Flight</legend>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Flight No</label>
                            <div class="col-md-6">
                                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            <div class="form-group{{ $errors->has('airline') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Airline</label>
                                <div class="col-md-6">
                                    {!! Form::select('airline', $airlines, null, array('class' => 'form-control', 'id'=>'airline')) !!}
                                    @if ($errors->has('airline'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('airline') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('orgin') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Orgin</label>
                                <div class="col-md-6">
                                    {!! Form::select('orgin', $orgin, null, array('class' => 'form-control', 'id'=>'orgin')) !!}
                                    @if ($errors->has('orgin'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('orgin') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Destination</label>
                                <div class="col-md-6">
                                    {!! Form::select('destination', $destination, null, array('class' => 'form-control', 'id'=>'destination')) !!}
                                    @if ($errors->has('destination'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('destination') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('Deperture_time') ? ' has-error' : '' }}">
                                <label for="Deperture_time" class="col-md-4 control-label">DepertureTime</label>
                                <div class="col-md-6">
                                    {!! Form::text('Deperture_time', null, array('placeholder' => 'DepertureTime','class' => 'form-control')) !!}
                                    @if ($errors->has('Deperture_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('Deperture_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('Arrival_time') ? ' has-error' : '' }}">
                                <label for="Arrival_time" class="col-md-4 control-label">ArrivalTime</label>
                                <div class="col-md-6">
                                    {!! Form::text('Arrival_time', null, array('placeholder' => 'ArrivalTime','class' => 'form-control')) !!}
                                    @if ($errors->has('Arrival_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('Arrival_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('price', null, array('placeholder' => 'Price','class' => 'form-control')) !!}
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{-----------------------------------------------------}}

                            <div class="form-group{{ $errors->has('sat_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Saturday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('sat_economy_price', null, array('placeholder' => 'Saturday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('sat_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sat_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sat_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Saturday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('sat_business_price', null, array('placeholder' => 'Saturday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('sat_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sat_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sun_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Sunday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('sun_economy_price', null, array('placeholder' => 'Sunday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('sun_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sun_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sun_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Sunday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('sun_business_price', null, array('placeholder' => 'Sunday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('sun_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sun_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('mon_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Monday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('mon_economy_price', null, array('placeholder' => 'Monday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('mon_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mon_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('mon_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Monday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('mon_business_price', null, array('placeholder' => 'Monday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('mon_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mon_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tue_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Tuesday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('tue_economy_price', null, array('placeholder' => 'Tuesday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('tue_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tue_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tue_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Tuesday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('tue_business_price', null, array('placeholder' => 'Tuesday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('tue_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tue_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('wed_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Wednesday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('wed_economy_price', null, array('placeholder' => 'Wednesday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('wed_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('wed_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('wed_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Wednesday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('wed_business_price', null, array('placeholder' => 'Wednesday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('wed_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('wed_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('thu_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Thursday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('thu_economy_price', null, array('placeholder' => 'Thursday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('thu_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('thu_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('thu_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Thursday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('thu_business_price', null, array('placeholder' => 'Thursday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('thu_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('thu_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('fri_economy_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Friday Economy Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('fri_economy_price', null, array('placeholder' => 'Friday Economy Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('fri_economy_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fri_economy_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('fri_business_price') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Friday Business Class Price</label>
                                <div class="col-md-4">
                                    {!! Form::text('fri_business_price', null, array('placeholder' => 'Friday Business Class Price','class' => 'form-control')) !!}
                                    @if ($errors->has('fri_business_price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fri_business_price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            {{-----------------------------------------------------}}

                            <div class="form-group{{ $errors->has('daily') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <label>{{ Form::checkbox('daily', 1 ,  false, array('class' => 'daily')) }}
                                        Daily</label>
                                    @if ($errors->has('daily'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('daily') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('sat') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <label>{{ Form::checkbox('sat', 1 ,  false, array('class' => 'sat')) }}
                                        satur Day</label>
                                    @if ($errors->has('sat'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sat') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sun') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-3">
                                    <label>{{ Form::checkbox('sun', 1 ,  false, array('class' => 'sun')) }}
                                        Sun Day</label>
                                    @if ($errors->has('sun'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sun') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('mon') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-3">
                                    <label>{{ Form::checkbox('mon', 1 ,  false, array('class' => 'mon')) }}
                                        Mon Day</label>
                                    @if ($errors->has('mon'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mon') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tue') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-3">
                                    <label>{{ Form::checkbox('tue', 1 ,  false, array('class' => 'tue')) }}
                                        Tue Day</label>
                                    @if ($errors->has('tue'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tue') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('wed') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-3">
                                    <label>{{ Form::checkbox('wed', 1 ,  false, array('class' => 'wed')) }}
                                        Wed Day</label>
                                    @if ($errors->has('wed'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('wed') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('thu') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-3">
                                    <label>{{ Form::checkbox('thu', 1 ,  false, array('class' => 'thu')) }}
                                        Thu Day</label>
                                    @if ($errors->has('thu'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('thu') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('fri') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-3">
                                    <label>{{ Form::checkbox('fri', 1 ,  false, array('class' => 'fri')) }}
                                        Fri Day</label>
                                    @if ($errors->has('fri'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fri') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                        </fieldset>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
