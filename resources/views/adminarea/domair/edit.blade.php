@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Domestic Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('domesticairlineindex') }}"> Back</a>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit airline</div>
                    <div class="panel-body">

                        {!! Form::model($airline, ['method' => 'PATCH','route' => ['domesticairlineupdate', $airline->id], 'enctype'=>'multipart/form-data', 'class' => 'form-horizontal']) !!}

                        <fieldset>
                            <legend>Airline</legend>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    {!! Form::text('name', $airline->name, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label for="image" class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    {!! Form::text('image', $airline->image, array('placeholder' => 'Image','class' => 'form-control')) !!}
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                        </fieldset>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
