@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Domestic Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('domesticairlinenew') }}">Create New</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Logo</th>
                        <th width="150px">Action</th>
                    </tr>
                    @foreach ($data as $key => $air)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $air->name }}</td>
                            <td>
                                @if ($air->image)
{{--                                    {{ Html::image('/api/image/tpackage-image/'.$air->image, 'a picture', array('class' => 'thumb','width' => 150, 'height' => 70)) }}--}}
                                    <img src="{{'/domestic-air/' . $air->image . '.png'}}" alt="not found" style="height: 40px;">
                                @endif
                            </td>
                            <td style=" vertical-align: middle;">

                                <a class="btn btn-primary" href="{{ route('domesticairlineedit',$air->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['domesticairlinedelete', $air->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
