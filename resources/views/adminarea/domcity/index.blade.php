@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Domestic Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('domesticcitynew') }}">Create New</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif


                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th width="150px">Action</th>
                    </tr>
                    @foreach ($data as $key => $city)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $city->name }}</td>

                            <td style=" vertical-align: middle;">

                                <a class="btn btn-primary" href="{{ route('domesticcityedit',$city->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['domesticcitydelete', $city->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>
                        </tr>
                    @endforeach
                </table>
                {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
