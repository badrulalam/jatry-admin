@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Users Management</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('agencyaddagent') }}"> Create New Agent</a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div>
                    @if (Auth::user()->hasRole('agency'))
                        <b>Yes yes</b>
                    @else
                        <b>No No</b>
                    @endif
                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>isactive</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($data as $key => $agent)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $agent->code }}</td>
                                <td>{{ $agent->first_name.' '.$agent->last_name }}</td>
                                <td>{{ $agent->email }}</td>
                                <td>{{ $agent->address }}</td>
                                <td>{{ $agent->isactive ? 'Yes':'No' }}</td>

                                <td>
                                    {{--                                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>--}}
                                    <a class="btn btn-primary" href="{{ route('agencyagentedit',$agent->id) }}">Edit</a>
                                    {{--{!! Form::open(['method' => 'DELETE','route' => ['userdelete', $agent->id],'style'=>'display:inline']) !!}--}}
                                    {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
