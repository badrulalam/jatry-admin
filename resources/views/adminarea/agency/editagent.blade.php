@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Agent Management</h2>
                </div>
                <div class="pull-right">
                    {{--<a class="btn btn-success" href="{{ route('allusers') }}"> Back </a>--}}
                    <a class="btn btn-primary" href="{{ route('allpermission') }}"> Back</a>
                </div>
            </div>
        </div>




        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit agent</div>
                    <div class="panel-body">

                        {!! Form::model($agent, ['method' => 'PATCH','route' => ['agencyagentupdate', $agent->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}

                        <fieldset>
                            <legend>Login Information</legend>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    {!! Form::text('name', $agent->user->name, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    {!! Form::text('email', $agent->user->email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </fieldset>


                        <fieldset>
                            <legend>Agent Information</legend>

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">First Name</label>
                                <div class="col-md-6">
                                    {!! Form::text('first_name', $agent->first_name, array('placeholder' => 'First Name','class' => 'form-control')) !!}
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">Last Name</label>
                                <div class="col-md-6">
                                    {!! Form::text('last_name', $agent->last_name, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('agent_email') ? ' has-error' : '' }}">
                                <label for="agent_email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    {!! Form::text('agent_email', $agent->email, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                    @if ($errors->has('agent_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('agent_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Address</label>
                                <div class="col-md-6">
                                    {!! Form::textarea('address', $agent->address, array('placeholder' => 'Address','class' => 'form-control','style'=>'height:100px')) !!}
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">City</label>
                                <div class="col-md-6">
                                    {!! Form::text('city', $agent->city, array('placeholder' => 'City','class' => 'form-control')) !!}
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                <label for="state" class="col-md-4 control-label">State</label>
                                <div class="col-md-6">
                                    {!! Form::text('state', $agent->state, array('placeholder' => 'State','class' => 'form-control')) !!}
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('postalcode') ? ' has-error' : '' }}">
                                <label for="postalcode" class="col-md-4 control-label">Postalcode</label>
                                <div class="col-md-6">
                                    {!! Form::text('postalcode', $agent->postalcode, array('placeholder' => 'Postalcode','class' => 'form-control')) !!}
                                    @if ($errors->has('postalcode'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('postalcode') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label for="country" class="col-md-4 control-label">Country</label>
                                <div class="col-md-6">
                                    {!! Form::select('country', Countries::getList('en', 'php', 'icu'), $agent->country, array('class' => 'form-control')) !!}
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Phone</label>
                                <div class="col-md-6">
                                    {!! Form::text('phone', $agent->phone, array('placeholder' => 'Phone','class' => 'form-control')) !!}
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label for="image" class="col-md-4 control-label">Image</label>
                                <div class="col-md-4">
                                    {!! Form::file('image', null, array('files' => true, 'class' => 'form-control')) !!}
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-2">

                                    @if ($agent->image)
{{--                                        {{ Html::image('../storage/uploads/agent-image/'.$agent->image, 'a picture', array('class' => 'thumb')) }}--}}
{{--                                        <img src="{{ storage_path().'/uploads/agent-image/'.$agent->image }}">--}}
                                        {{--<img src="{{ asset('../storage/uploads/agent-image/'). '/' .$agent->image }}">--}}
                                        {{--<img src="{{ asset('/uploads/agent/'). '/' .'CIRCULAR-LOGO-black.png' }}">--}}

                                        {{ Html::image('/api/image/agent-image/'.$agent->image, 'a picture', array('class' => 'thumb','width' => 70, 'height' => 70)) }}
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('isactive') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <br/>

                                    <label>{{ Form::checkbox('isactive', 1 ,  $agent->isactive, array('class' => 'name')) }}
                                        Active</label>


                                    @if ($errors->has('isactive'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('isactive') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </fieldset>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
