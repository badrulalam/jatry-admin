@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 margin-tb">
                <div class="pull-left">
                    <h2>Agency Management</h2>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div>
                    @if (Auth::user()->hasRole('agency'))
                        <b>Yes yes</b>
                    @else
                        <b>No No</b>
                    @endif
                </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>isactive</th>
                            <th>Agent</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($data as $key => $agency)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $agency->code }}</td>
                                <td>{{ $agency->first_name.' '.$agency->name }}</td>
                                <td>{{ $agency->email }}</td>
                                <td>{{ $agency->address }}</td>
                                <td>{{ $agency->isactive ? 'Yes':'No' }}</td>
                                <td>{{ count($agency->agent)}}</td>

                                <td>
                                    <a class="btn btn-primary" href="">Show</a>
                                    <a class="btn btn-primary" href="{{ route('adminagencyagentlist',$agency->id) }}">Show Agent</a>

                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $data->render() !!}

            </div>
        </div>
    </div>
@endsection
