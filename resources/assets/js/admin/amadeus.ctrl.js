/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('AmadeusCtrl', ["$scope",  "$http", function ($scope, $http) {
$scope.searchObj = {origin: "DAC", destination: "DEL", departureDate: new Date("2017-02-22T18:00:00.000Z")};
            $scope.searchTrigger = function () {


                $http.post('/api/trigger/amadeus/air-availability',$scope.searchObj)
                    .then(function (data) {
                        var x2js = new X2JS();
                        $scope.data = x2js.xml_str2json(data.data);
                       $scope.airavailable = (data.data);
                        console.log($scope.airavailable );
                        
                    },function (err) {
                        $scope.errorData = err;
                        console.log($scope.errorData );

                    });


                // $window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);




            };
            // $scope.airavailable = {"currency":"USD","results":[{"itineraries":[{"outbound":{"flights":[{"departs_at":"2017-02-25T14:15","arrives_at":"2017-02-25T16:19","origin":{"airport":"BOS","terminal":"A"},"destination":{"airport":"YYZ","terminal":"3"},"marketing_airline":"WS","operating_airline":"WS","flight_number":"3605","aircraft":"DH4","booking_info":{"travel_class":"ECONOMY","booking_code":"D","seats_remaining":7}},{"departs_at":"2017-02-25T20:25","arrives_at":"2017-02-26T08:35","origin":{"airport":"YYZ","terminal":"3"},"destination":{"airport":"LGW","terminal":"N"},"marketing_airline":"WS","operating_airline":"WS","flight_number":"3","aircraft":"76W","booking_info":{"travel_class":"ECONOMY","booking_code":"P","seats_remaining":4}}]}}],"fare":{"total_price":"462.06","price_per_adult":{"total_fare":"462.06","tax":"39.06"},"restrictions":{"refundable":false,"change_penalties":true}}},{"itineraries":[{"outbound":{"flights":[{"departs_at":"2017-02-25T18:00","arrives_at":"2017-02-26T04:35","origin":{"airport":"BOS","terminal":"E"},"destination":{"airport":"KEF"},"marketing_airline":"WW","operating_airline":"WW","flight_number":"126","aircraft":"321","booking_info":{"travel_class":"ECONOMY","booking_code":"X","seats_remaining":9}},{"departs_at":"2017-02-26T06:15","arrives_at":"2017-02-26T09:25","origin":{"airport":"KEF"},"destination":{"airport":"LGW","terminal":"S"},"marketing_airline":"WW","operating_airline":"WW","flight_number":"810","aircraft":"320","booking_info":{"travel_class":"ECONOMY","booking_code":"K","seats_remaining":2}}]}}],"fare":{"total_price":"636.34","price_per_adult":{"total_fare":"636.34","tax":"104.34"},"restrictions":{"refundable":false,"change_penalties":true}}},{"itineraries":[{"outbound":{"flights":[{"departs_at":"2017-02-25T22:50","arrives_at":"2017-02-26T16:15","origin":{"airport":"BOS","terminal":"E"},"destination":{"airport":"IST","terminal":"I"},"marketing_airline":"TK","operating_airline":"TK","flight_number":"82","aircraft":"333","booking_info":{"travel_class":"ECONOMY","booking_code":"S","seats_remaining":9}},{"departs_at":"2017-02-26T19:45","arrives_at":"2017-02-26T21:05","origin":{"airport":"IST","terminal":"I"},"destination":{"airport":"LHR","terminal":"2"},"marketing_airline":"TK","operating_airline":"TK","flight_number":"1983","aircraft":"32B","booking_info":{"travel_class":"ECONOMY","booking_code":"S","seats_remaining":9}}]}}],"fare":{"total_price":"637.41","price_per_adult":{"total_fare":"637.41","tax":"203.41"},"restrictions":{"refundable":true,"change_penalties":true}}},{"itineraries":[{"outbound":{"flights":[{"departs_at":"2017-02-25T19:41","arrives_at":"2017-02-25T20:58","origin":{"airport":"BOS","terminal":"C"},"destination":{"airport":"JFK","terminal":"5"},"marketing_airline":"TP","operating_airline":"B6","flight_number":"8001","aircraft":"E90","booking_info":{"travel_class":"ECONOMY","booking_code":"Q","seats_remaining":4}},{"departs_at":"2017-02-25T23:40","arrives_at":"2017-02-26T11:20","origin":{"airport":"JFK","terminal":"5"},"destination":{"airport":"LIS","terminal":"1"},"marketing_airline":"TP","operating_airline":"TP","flight_number":"208","aircraft":"332","booking_info":{"travel_class":"ECONOMY","booking_code":"Q","seats_remaining":9}},{"departs_at":"2017-02-26T12:40","arrives_at":"2017-02-26T15:30","origin":{"airport":"LIS","terminal":"1"},"destination":{"airport":"LHR","terminal":"2"},"marketing_airline":"TP","operating_airline":"TP","flight_number":"368","aircraft":"319","booking_info":{"travel_class":"ECONOMY","booking_code":"Q","seats_remaining":9}}]}}],"fare":{"total_price":"710.43","price_per_adult":{"total_fare":"710.43","tax":"175.43"},"restrictions":{"refundable":false,"change_penalties":true}}},{"itineraries":[{"outbound":{"flights":[{"departs_at":"2017-02-25T21:40","arrives_at":"2017-02-26T09:00","origin":{"airport":"BOS","terminal":"E"},"destination":{"airport":"LHR","terminal":"3"},"marketing_airline":"DL","operating_airline":"VS","flight_number":"4379","aircraft":"789","booking_info":{"travel_class":"ECONOMY","booking_code":"H","seats_remaining":9}}]}}],"fare":{"total_price":"1541.10","price_per_adult":{"total_fare":"1541.10","tax":"153.10"},"restrictions":{"refundable":true,"change_penalties":true}}}]};
            $scope.getArray = function (obj) {
                if(Array.isArray(obj))
                    return obj;
                else return [obj]
            };
            function formatXml(xml) {
                var formatted = '';
                var reg = /(>)(<)(\/*)/g;
                xml = xml.replace(reg, '$1\r\n$2$3');
                var pad = 0;
                jQuery.each(xml.split('\r\n'), function(index, node)
                {
                    var indent = 0;
                    if (node.match( /.+<\/\w[^>]*>$/ ))
                    {
                        indent = 0;
                    }
                    else if (node.match( /^<\/\w/ ))
                    {
                        if (pad != 0)
                        {
                            pad -= 1;
                        }
                    }
                    else if (node.match( /^<\w[^>]*[^\/]>.*$/ ))
                    {
                        indent = 1;
                    }
                    else
                    {
                        indent = 0;
                    }
                    var padding = '';
                    for (var i = 0; i < pad; i++)
                    {
                        padding += '  ';
                    }
                    formatted += padding + node + '\r\n';
                    pad += indent;
                });
                return formatted;
            }

        }]);
})();
