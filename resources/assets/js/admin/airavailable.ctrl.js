/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('AirAvailableCtrl', ["$scope",  "$http", function ($scope, $http) {
$scope.searchObj = {origin: "DAC", destination: "DEL", departureDate: new Date("2017-02-22T18:00:00.000Z")};
            $scope.searchTrigger = function () {


                $http.post('/api/trigger/air-availability',$scope.searchObj)
                    .then(function (data) {
                        var x2js = new X2JS();
                        $scope.data = x2js.xml_str2json(data.data);
                       $scope.airavailable = formatXml(data.data);
                        console.log($scope.data );
                        
                    },function (err) {
                        $scope.errorData = err;
                        console.log($scope.errorData );

                    });


                // $window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);



            };
            $scope.getArray = function (obj) {
                if(Array.isArray(obj))
                    return obj;
                else return [obj]
            };
            function formatXml(xml) {
                var formatted = '';
                var reg = /(>)(<)(\/*)/g;
                xml = xml.replace(reg, '$1\r\n$2$3');
                var pad = 0;
                jQuery.each(xml.split('\r\n'), function(index, node)
                {
                    var indent = 0;
                    if (node.match( /.+<\/\w[^>]*>$/ ))
                    {
                        indent = 0;
                    }
                    else if (node.match( /^<\/\w/ ))
                    {
                        if (pad != 0)
                        {
                            pad -= 1;
                        }
                    }
                    else if (node.match( /^<\w[^>]*[^\/]>.*$/ ))
                    {
                        indent = 1;
                    }
                    else
                    {
                        indent = 0;
                    }
                    var padding = '';
                    for (var i = 0; i < pad; i++)
                    {
                        padding += '  ';
                    }
                    formatted += padding + node + '\r\n';
                    pad += indent;
                });
                return formatted;
            }


        }]);
})();
