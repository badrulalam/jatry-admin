/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('QpxCtrl', ["$scope",  "$http", function ($scope, $http) {
$scope.searchObj = {origin: "DAC", destination: "DEL", departureDate: new Date("2017-02-22T18:00:00.000Z")};
            $scope.searchTrigger = function () {

                var sobj = {
                            "request": {
                            "slice": [
                            {
                            "origin": $scope.searchObj.origin,
                            "destination": $scope.searchObj.destination,
                            "date": $scope.searchObj.departureDate.yyyymmdd()
                            }
                            ],
                            "passengers": {
                               "adultCount": 1,
                                 "infantInLapCount": 0,
                                 "infantInSeatCount": 0,
                                 "childCount": 0,
                                 "seniorCount": 0
                               },
                               "solutions": 20,
                               "refundable": false
                             }
                            };

                console.log("s obj", sobj.request.slice[0]);

                $http.post('https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyAYSNsJO3Md5zsYqxYe9hs6ohncuHBlV1w',sobj)
                    .then(function (data) {

                       $scope.airavailable = data.data;
                        console.log($scope.data );
                        
                    },function (err) {
                        $scope.errorData = err;
                        console.log($scope.errorData );

                    });


                // $window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);



            };
            $scope.getArray = function (obj) {
                if(Array.isArray(obj))
                    return obj;
                else return [obj]
            };
            function formatXml(xml) {
                var formatted = '';
                var reg = /(>)(<)(\/*)/g;
                xml = xml.replace(reg, '$1\r\n$2$3');
                var pad = 0;
                jQuery.each(xml.split('\r\n'), function(index, node)
                {
                    var indent = 0;
                    if (node.match( /.+<\/\w[^>]*>$/ ))
                    {
                        indent = 0;
                    }
                    else if (node.match( /^<\/\w/ ))
                    {
                        if (pad != 0)
                        {
                            pad -= 1;
                        }
                    }
                    else if (node.match( /^<\w[^>]*[^\/]>.*$/ ))
                    {
                        indent = 1;
                    }
                    else
                    {
                        indent = 0;
                    }
                    var padding = '';
                    for (var i = 0; i < pad; i++)
                    {
                        padding += '  ';
                    }
                    formatted += padding + node + '\r\n';
                    pad += indent;
                });
                return formatted;
            }

// $scope.airavailable = loadData();
// console.log($scope.airavailable );
        }]);

    function loadData(){

       return {
            "kind": "qpxExpress#tripsSearch",
            "trips": {
            "kind": "qpxexpress#tripOptions",
                "requestId": "L9Zn8xsYvrIptCwXD0Pzxa",
                "data": {
                "kind": "qpxexpress#data",
                    "airport": [
                    {
                        "kind": "qpxexpress#airportData",
                        "code": "BKK",
                        "city": "BKK",
                        "name": "Bangkok Suvarnabhumi International"
                    },
                    {
                        "kind": "qpxexpress#airportData",
                        "code": "BOM",
                        "city": "BOM",
                        "name": "Mumbai/Bombay Chhatrapati Shivaji Int'l"
                    },
                    {
                        "kind": "qpxexpress#airportData",
                        "code": "CCU",
                        "city": "CCU",
                        "name": "Kolkata/Calcutta Netaji Subhas Chandra"
                    },
                    {
                        "kind": "qpxexpress#airportData",
                        "code": "DAC",
                        "city": "DAC",
                        "name": "Dhaka Hazrat Shahjalal International"
                    },
                    {
                        "kind": "qpxexpress#airportData",
                        "code": "DEL",
                        "city": "DEL",
                        "name": "Delhi Indira Gandhi International"
                    }
                ],
                    "city": [
                    {
                        "kind": "qpxexpress#cityData",
                        "code": "BKK",
                        "name": "Bangkok"
                    },
                    {
                        "kind": "qpxexpress#cityData",
                        "code": "BOM",
                        "name": "Mumbai"
                    },
                    {
                        "kind": "qpxexpress#cityData",
                        "code": "CCU",
                        "name": "Calcutta"
                    },
                    {
                        "kind": "qpxexpress#cityData",
                        "code": "DAC",
                        "name": "Dhaka"
                    },
                    {
                        "kind": "qpxexpress#cityData",
                        "code": "DEL",
                        "name": "Delhi"
                    }
                ],
                    "aircraft": [
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "319",
                        "name": "Airbus A319"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "321",
                        "name": "Airbus A321"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "332",
                        "name": "Airbus A330"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "738",
                        "name": "Boeing 737"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "73H",
                        "name": "Boeing 737"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "73W",
                        "name": "Boeing 737"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "773",
                        "name": "Boeing 777"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "777",
                        "name": "Boeing 777"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "787",
                        "name": "Boeing 787"
                    },
                    {
                        "kind": "qpxexpress#aircraftData",
                        "code": "788",
                        "name": "Boeing 787"
                    }
                ],
                    "tax": [
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "IN_003",
                        "name": "India User Development Fee Arrivals"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "E7_002",
                        "name": "Thailand Advance Passenger Processing User Charge"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "UT_001",
                        "name": "Bangladesh Travel Tax"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "YQ_F",
                        "name": "AI YQ surcharge"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "E5_001",
                        "name": "Bangladesh Value Added Tax on Embarkation Fees"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "YR_F",
                        "name": "9W YR surcharge"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "YR_I",
                        "name": "AI YR surcharge"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "G8_001",
                        "name": "Thailand International Departure Fee"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "G8_002",
                        "name": "Thailand International Arrival Fee"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "OW_001",
                        "name": "Bangladesh Domestic And International Excise Duty Tax"
                    },
                    {
                        "kind": "qpxexpress#taxData",
                        "id": "BD_001",
                        "name": "Bangladesh Embarkation Fee"
                    }
                ],
                    "carrier": [
                    {
                        "kind": "qpxexpress#carrierData",
                        "code": "9W",
                        "name": "Jet Airways"
                    },
                    {
                        "kind": "qpxexpress#carrierData",
                        "code": "AI",
                        "name": "Air India Limited"
                    },
                    {
                        "kind": "qpxexpress#carrierData",
                        "code": "BG",
                        "name": "Biman Bangladesh Airlines Limited"
                    },
                    {
                        "kind": "qpxexpress#carrierData",
                        "code": "TG",
                        "name": "Thai Airways International Public"
                    },
                    {
                        "kind": "qpxexpress#carrierData",
                        "code": "YY",
                        "name": "YY"
                    }
                ]
            },
            "tripOption": [
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT12994",
                    "id": "8Ch3keGdt21OmsqPe5AAWR009",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 1380,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "229"
                                    },
                                    "id": "GSt5g0vNHr91BwVr",
                                    "cabin": "COACH",
                                    "bookingCode": "E",
                                    "bookingCodeCount": 5,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L9YhHTELn7ERmlvI",
                                            "aircraft": "319",
                                            "arrivalTime": "2017-02-17T21:50+05:30",
                                            "departureTime": "2017-02-17T21:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 1180
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 140,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "701"
                                    },
                                    "id": "G0xoly3AiF17LpmT",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 9,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "La5FT1NFaaFGRduH",
                                            "aircraft": "788",
                                            "arrivalTime": "2017-02-18T19:50+05:30",
                                            "departureTime": "2017-02-18T17:30+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 140,
                                            "mileage": 815,
                                            "meal": "Snack or Brunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "carrier": "AI",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "EOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "GSt5g0vNHr91BwVr"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "G0xoly3AiF17LpmT"
                                }
                            ],
                            "baseFareTotal": "USD85.00",
                            "saleFareTotal": "BDT6673",
                            "saleTaxTotal": "BDT6321",
                            "saleTotal": "BDT12994",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_I",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT83"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YQ_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YQ",
                                    "salePrice": "BDT3140"
                                }
                            ],
                            "fareCalculation": "DAC AI X/CCU AI DEL 85.00EOWBD NUC 85.00 END ROE 1.00 FARE USD 85.00 EQU BDT 6673 XT 500BD 75E5 500OW 1200UT 823IN 3140YQ 83YR",
                            "latestTicketingTime": "2017-02-17T10:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT12994",
                    "id": "8Ch3keGdt21OmsqPe5AAWR004",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 740,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "229"
                                    },
                                    "id": "GSt5g0vNHr91BwVr",
                                    "cabin": "COACH",
                                    "bookingCode": "E",
                                    "bookingCodeCount": 5,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L9YhHTELn7ERmlvI",
                                            "aircraft": "319",
                                            "arrivalTime": "2017-02-17T21:50+05:30",
                                            "departureTime": "2017-02-17T21:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 550
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 130,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "763"
                                    },
                                    "id": "GNq9A-UNCqSTzshq",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 9,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Lr4V+til8F8sl3CJ",
                                            "aircraft": "321",
                                            "arrivalTime": "2017-02-18T09:10+05:30",
                                            "departureTime": "2017-02-18T07:00+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 130,
                                            "mileage": 815,
                                            "meal": "Breakfast"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "carrier": "AI",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "EOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "GNq9A-UNCqSTzshq"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "GSt5g0vNHr91BwVr"
                                }
                            ],
                            "baseFareTotal": "USD85.00",
                            "saleFareTotal": "BDT6673",
                            "saleTaxTotal": "BDT6321",
                            "saleTotal": "BDT12994",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_I",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT83"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YQ_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YQ",
                                    "salePrice": "BDT3140"
                                }
                            ],
                            "fareCalculation": "DAC AI X/CCU AI DEL 85.00EOWBD NUC 85.00 END ROE 1.00 FARE USD 85.00 EQU BDT 6673 XT 500BD 75E5 500OW 1200UT 823IN 3140YQ 83YR",
                            "latestTicketingTime": "2017-02-17T10:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT12994",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00F",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 1550,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "229"
                                    },
                                    "id": "GSt5g0vNHr91BwVr",
                                    "cabin": "COACH",
                                    "bookingCode": "E",
                                    "bookingCodeCount": 5,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L9YhHTELn7ERmlvI",
                                            "aircraft": "319",
                                            "arrivalTime": "2017-02-17T21:50+05:30",
                                            "departureTime": "2017-02-17T21:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 1345
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 145,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "23"
                                    },
                                    "id": "GZ8HmUG2an9+mSdg",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 9,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LSTS9UEB4WvDXsE0",
                                            "aircraft": "321",
                                            "arrivalTime": "2017-02-18T22:40+05:30",
                                            "departureTime": "2017-02-18T20:15+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 145,
                                            "mileage": 815,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "carrier": "AI",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "EOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "GZ8HmUG2an9+mSdg"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "GSt5g0vNHr91BwVr"
                                }
                            ],
                            "baseFareTotal": "USD85.00",
                            "saleFareTotal": "BDT6673",
                            "saleTaxTotal": "BDT6321",
                            "saleTotal": "BDT12994",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_I",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT83"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YQ_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YQ",
                                    "salePrice": "BDT3140"
                                }
                            ],
                            "fareCalculation": "DAC AI X/CCU AI DEL 85.00EOWBD NUC 85.00 END ROE 1.00 FARE USD 85.00 EQU BDT 6673 XT 500BD 75E5 500OW 1200UT 823IN 3140YQ 83YR",
                            "latestTicketingTime": "2017-02-17T10:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT12994",
                    "id": "8Ch3keGdt21OmsqPe5AAWR005",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 925,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "229"
                                    },
                                    "id": "GSt5g0vNHr91BwVr",
                                    "cabin": "COACH",
                                    "bookingCode": "E",
                                    "bookingCodeCount": 5,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L9YhHTELn7ERmlvI",
                                            "aircraft": "319",
                                            "arrivalTime": "2017-02-17T21:50+05:30",
                                            "departureTime": "2017-02-17T21:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 730
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 135,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "21"
                                    },
                                    "id": "G9SAyD01Sl0ZJ3WD",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 9,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LxVL7PxtxbSfORAJ",
                                            "aircraft": "788",
                                            "arrivalTime": "2017-02-18T12:15+05:30",
                                            "departureTime": "2017-02-18T10:00+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 135,
                                            "mileage": 815,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "carrier": "AI",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "EOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "G9SAyD01Sl0ZJ3WD"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AuJDKIMGVdzGyePmkTg8qObJhbuqMvolxmegqCH1CLKJqMSjFnMp/iiFT+JPPzBZ86NfIyv7gvupgNZpsf69S/PXk+xPt",
                                    "segmentId": "GSt5g0vNHr91BwVr"
                                }
                            ],
                            "baseFareTotal": "USD85.00",
                            "saleFareTotal": "BDT6673",
                            "saleTaxTotal": "BDT6321",
                            "saleTotal": "BDT12994",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_I",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT83"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YQ_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YQ",
                                    "salePrice": "BDT3140"
                                }
                            ],
                            "fareCalculation": "DAC AI X/CCU AI DEL 85.00EOWBD NUC 85.00 END ROE 1.00 FARE USD 85.00 EQU BDT 6673 XT 500BD 75E5 500OW 1200UT 823IN 3140YQ 83YR",
                            "latestTicketingTime": "2017-02-17T10:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT18877",
                    "id": "8Ch3keGdt21OmsqPe5AAWR001",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 125,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 125,
                                    "flight": {
                                        "carrier": "AI",
                                        "number": "9011"
                                    },
                                    "id": "G3PjuVB1d92v3GzO",
                                    "cabin": "COACH",
                                    "bookingCode": "S",
                                    "bookingCodeCount": 9,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L-wNkzR4axoCCOWc",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T09:05+05:30",
                                            "departureTime": "2017-02-17T07:30+06:00",
                                            "origin": "DAC",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 125,
                                            "operatingDisclosure": "OPERATED BY AIR-INDIA EXPRESS",
                                            "mileage": 885,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AXeRnoZq1oPvZW5moLXVP93jcHtdrs3HvrWfag+lHvRQ",
                                    "carrier": "AI",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "SOBDIXCS"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AXeRnoZq1oPvZW5moLXVP93jcHtdrs3HvrWfag+lHvRQ",
                                    "segmentId": "G3PjuVB1d92v3GzO"
                                }
                            ],
                            "baseFareTotal": "USD176.00",
                            "saleFareTotal": "BDT13816",
                            "saleTaxTotal": "BDT5061",
                            "saleTotal": "BDT18877",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YQ_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YQ",
                                    "salePrice": "BDT1963"
                                }
                            ],
                            "fareCalculation": "DAC AI DEL 176.00SOBDIXCS NUC 176.00 END ROE 1.00 FARE USD 176.00 EQU BDT 13816 XT 500BD 75E5 500OW 1200UT 823IN 1963YQ",
                            "latestTicketingTime": "2017-02-16T20:29-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT25078",
                    "id": "8Ch3keGdt21OmsqPe5AAWR002",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 160,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 160,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "271"
                                    },
                                    "id": "GMQ3-KVmnkh8TIr6",
                                    "cabin": "COACH",
                                    "bookingCode": "H",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LVK2axFoCH16EthE",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T12:15+05:30",
                                            "departureTime": "2017-02-17T10:05+06:00",
                                            "origin": "DAC",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 160,
                                            "mileage": 885,
                                            "meal": "Snack or Brunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "A1JkBROcdd4n4SB3KWODacnBsPD1zbsqJP4xl1031g4g",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "H2SPOBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "A1JkBROcdd4n4SB3KWODacnBsPD1zbsqJP4xl1031g4g",
                                    "segmentId": "GMQ3-KVmnkh8TIr6"
                                }
                            ],
                            "baseFareTotal": "USD280.00",
                            "saleFareTotal": "BDT21980",
                            "saleTaxTotal": "BDT3098",
                            "saleTotal": "BDT25078",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                }
                            ],
                            "fareCalculation": "DAC 9W DEL 280.00H2SPOBD NUC 280.00 END ROE 1.00 FARE USD 280.00 EQU BDT 21980 XT 500BD 75E5 500OW 1200UT 823IN",
                            "latestTicketingTime": "2017-02-16T23:04-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT28748",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00A",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 1015,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "273"
                                    },
                                    "id": "GIlvAEBhEAzotzYz",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LTUAYvzdYidaeuOq",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T17:50+05:30",
                                            "departureTime": "2017-02-17T17:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 820
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 135,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "7143"
                                    },
                                    "id": "Gz76Isxf+5M-ZF7d",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LyBADAHDmjKBFY8J",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-18T09:45+05:30",
                                            "departureTime": "2017-02-18T07:30+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 135,
                                            "operatingDisclosure": "OPERATED BY JETKONNECT",
                                            "mileage": 815,
                                            "meal": "Breakfast"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "CCU",
                                    "basisCode": "K2WOW1BD"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "carrier": "9W",
                                    "origin": "CCU",
                                    "destination": "DEL",
                                    "basisCode": "Q2VIFOW"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "segmentId": "GIlvAEBhEAzotzYz"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "segmentId": "Gz76Isxf+5M-ZF7d"
                                }
                            ],
                            "baseFareTotal": "USD326.00",
                            "saleFareTotal": "BDT25591",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT28748",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/CCU Q3.00 103.00K2WOW1BD 9W DEL 220.00Q2VIFOW NUC 326.00 END ROE 1.00 FARE USD 326.00 EQU BDT 25591 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T06:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT28748",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00H",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 1420,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "273"
                                    },
                                    "id": "GIlvAEBhEAzotzYz",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LTUAYvzdYidaeuOq",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T17:50+05:30",
                                            "departureTime": "2017-02-17T17:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 1230
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 130,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "946"
                                    },
                                    "id": "GmQRFvzck+7H088u",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L+BBPS5GK9WPjAUF",
                                            "aircraft": "332",
                                            "arrivalTime": "2017-02-18T16:30+05:30",
                                            "departureTime": "2017-02-18T14:20+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 130,
                                            "mileage": 815,
                                            "meal": "Lunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "CCU",
                                    "basisCode": "K2WOW1BD"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "carrier": "9W",
                                    "origin": "CCU",
                                    "destination": "DEL",
                                    "basisCode": "Q2VIFOW"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "segmentId": "GIlvAEBhEAzotzYz"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "segmentId": "GmQRFvzck+7H088u"
                                }
                            ],
                            "baseFareTotal": "USD326.00",
                            "saleFareTotal": "BDT25591",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT28748",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/CCU Q3.00 103.00K2WOW1BD 9W DEL 220.00Q2VIFOW NUC 326.00 END ROE 1.00 FARE USD 326.00 EQU BDT 25591 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T06:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT28748",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00G",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 1235,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "273"
                                    },
                                    "id": "GIlvAEBhEAzotzYz",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LTUAYvzdYidaeuOq",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T17:50+05:30",
                                            "departureTime": "2017-02-17T17:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 1025
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 150,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "964"
                                    },
                                    "id": "Gm++VxsAv+WBKHey",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L3U+FNY0mXNVGnhz",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-18T13:25+05:30",
                                            "departureTime": "2017-02-18T10:55+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 150,
                                            "mileage": 815,
                                            "meal": "Snack or Brunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "CCU",
                                    "basisCode": "K2WOW1BD"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "carrier": "9W",
                                    "origin": "CCU",
                                    "destination": "DEL",
                                    "basisCode": "Q2VIFOW"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "segmentId": "GIlvAEBhEAzotzYz"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "segmentId": "Gm++VxsAv+WBKHey"
                                }
                            ],
                            "baseFareTotal": "USD326.00",
                            "saleFareTotal": "BDT25591",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT28748",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/CCU Q3.00 103.00K2WOW1BD 9W DEL 220.00Q2VIFOW NUC 326.00 END ROE 1.00 FARE USD 326.00 EQU BDT 25591 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T06:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT28748",
                    "id": "8Ch3keGdt21OmsqPe5AAWR003",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 370,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "273"
                                    },
                                    "id": "GIlvAEBhEAzotzYz",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LTUAYvzdYidaeuOq",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T17:50+05:30",
                                            "departureTime": "2017-02-17T17:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 155
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 155,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "902"
                                    },
                                    "id": "G+zgXr52gBJYwL-+",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LjFY+7FqsKNPr8YC",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T23:00+05:30",
                                            "departureTime": "2017-02-17T20:25+05:30",
                                            "origin": "CCU",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 155,
                                            "mileage": 815,
                                            "meal": "Dinner"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "CCU",
                                    "basisCode": "K2WOW1BD"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "carrier": "9W",
                                    "origin": "CCU",
                                    "destination": "DEL",
                                    "basisCode": "Q2VIFOW"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "segmentId": "GIlvAEBhEAzotzYz"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ACyPvoZrHj+kC5Ltc4gMuHDvDo5rpRw5s7zxZHZ/DZk3R",
                                    "segmentId": "G+zgXr52gBJYwL-+"
                                }
                            ],
                            "baseFareTotal": "USD326.00",
                            "saleFareTotal": "BDT25591",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT28748",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/CCU Q3.00 103.00K2WOW1BD 9W DEL 220.00Q2VIFOW NUC 326.00 END ROE 1.00 FARE USD 326.00 EQU BDT 25591 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T06:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT42878",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00K",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 980,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 205,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "275"
                                    },
                                    "id": "GUR8wOkwfwDEo2Pj",
                                    "cabin": "COACH",
                                    "bookingCode": "L",
                                    "bookingCodeCount": 5,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LcgnhVa8w5VFJbvy",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T16:10+05:30",
                                            "departureTime": "2017-02-17T13:15+06:00",
                                            "origin": "DAC",
                                            "destination": "BOM",
                                            "destinationTerminal": "2",
                                            "duration": 205,
                                            "mileage": 1172,
                                            "meal": "Lunch"
                                        }
                                    ],
                                    "connectionDuration": 650
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 125,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "339"
                                    },
                                    "id": "Gt43HwX9mizqgHNq",
                                    "cabin": "COACH",
                                    "bookingCode": "L",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Lppb06mG1FmqaX-O",
                                            "aircraft": "73W",
                                            "arrivalTime": "2017-02-18T05:05+05:30",
                                            "departureTime": "2017-02-18T03:00+05:30",
                                            "origin": "BOM",
                                            "destination": "DEL",
                                            "originTerminal": "2",
                                            "destinationTerminal": "3",
                                            "duration": 125,
                                            "mileage": 706,
                                            "meal": "Snack or Brunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "A4W/UpZbbFpOWP9D/zh6MhHFwxSXjvWA8SAAsopnBVls",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "L2WOW1BD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "A4W/UpZbbFpOWP9D/zh6MhHFwxSXjvWA8SAAsopnBVls",
                                    "segmentId": "GUR8wOkwfwDEo2Pj"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "A4W/UpZbbFpOWP9D/zh6MhHFwxSXjvWA8SAAsopnBVls",
                                    "segmentId": "Gt43HwX9mizqgHNq"
                                }
                            ],
                            "baseFareTotal": "USD506.00",
                            "saleFareTotal": "BDT39721",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT42878",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/BOM S55.00 9W DEL Q DACDEL3.00 448.00L2WOW1BD NUC 506.00 END ROE 1.00 FARE USD 506.00 EQU BDT 39721 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T02:14-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT42878",
                    "id": "8Ch3keGdt21OmsqPe5AAWR007",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 605,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 205,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "275"
                                    },
                                    "id": "GUR8wOkwfwDEo2Pj",
                                    "cabin": "COACH",
                                    "bookingCode": "L",
                                    "bookingCodeCount": 5,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LcgnhVa8w5VFJbvy",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T16:10+05:30",
                                            "departureTime": "2017-02-17T13:15+06:00",
                                            "origin": "DAC",
                                            "destination": "BOM",
                                            "destinationTerminal": "2",
                                            "duration": 205,
                                            "mileage": 1172,
                                            "meal": "Lunch"
                                        }
                                    ],
                                    "connectionDuration": 280
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 120,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "361"
                                    },
                                    "id": "Ghm8o1liLj8ywf26",
                                    "cabin": "COACH",
                                    "bookingCode": "L",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LFHqp2l9pgtRl9ya",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T22:50+05:30",
                                            "departureTime": "2017-02-17T20:50+05:30",
                                            "origin": "BOM",
                                            "destination": "DEL",
                                            "originTerminal": "2",
                                            "destinationTerminal": "3",
                                            "duration": 120,
                                            "mileage": 706,
                                            "meal": "Dinner"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "A4W/UpZbbFpOWP9D/zh6MhHFwxSXjvWA8SAAsopnBVls",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "L2WOW1BD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "A4W/UpZbbFpOWP9D/zh6MhHFwxSXjvWA8SAAsopnBVls",
                                    "segmentId": "GUR8wOkwfwDEo2Pj"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "A4W/UpZbbFpOWP9D/zh6MhHFwxSXjvWA8SAAsopnBVls",
                                    "segmentId": "Ghm8o1liLj8ywf26"
                                }
                            ],
                            "baseFareTotal": "USD506.00",
                            "saleFareTotal": "BDT39721",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT42878",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/BOM S55.00 9W DEL Q DACDEL3.00 448.00L2WOW1BD NUC 506.00 END ROE 1.00 FARE USD 506.00 EQU BDT 39721 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T02:14-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT44507",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00J",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 735,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 60,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "273"
                                    },
                                    "id": "GIlvAEBhEAzotzYz",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LTUAYvzdYidaeuOq",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T17:50+05:30",
                                            "departureTime": "2017-02-17T17:20+06:00",
                                            "origin": "DAC",
                                            "destination": "CCU",
                                            "duration": 60,
                                            "mileage": 148,
                                            "meal": "Snack or Brunch"
                                        }
                                    ],
                                    "connectionDuration": 200
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 175,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "628"
                                    },
                                    "id": "GC9TibS0tInxA2RT",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LTXYe4oOqbTlVySX",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-18T00:05+05:30",
                                            "departureTime": "2017-02-17T21:10+05:30",
                                            "origin": "CCU",
                                            "destination": "BOM",
                                            "destinationTerminal": "2",
                                            "duration": 175,
                                            "mileage": 1034,
                                            "meal": "Dinner"
                                        }
                                    ],
                                    "connectionDuration": 175
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 125,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "339"
                                    },
                                    "id": "Gt43HwX9mizqgHNq",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "2",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Lppb06mG1FmqaX-O",
                                            "aircraft": "73W",
                                            "arrivalTime": "2017-02-18T05:05+05:30",
                                            "departureTime": "2017-02-18T03:00+05:30",
                                            "origin": "BOM",
                                            "destination": "DEL",
                                            "originTerminal": "2",
                                            "destinationTerminal": "3",
                                            "duration": 125,
                                            "mileage": 706,
                                            "meal": "Snack or Brunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "CCU",
                                    "basisCode": "K2WOW1BD"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AMmcQK2Ujlc4QO0UTn0kOYUcIXWcBpBeckV4RA6bnTvco",
                                    "carrier": "9W",
                                    "origin": "CCU",
                                    "destination": "BOM",
                                    "basisCode": "Q2VIFOW"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AtDfGMjCVdAZYmffPchLYpF1NiBtzQnn7K7Z5jKJ8v9Hk",
                                    "carrier": "9W",
                                    "origin": "BOM",
                                    "destination": "DEL",
                                    "basisCode": "Q2VIFOW"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AgIDmD0H8MgXJLaKDPjzxjKLlIXOk4BC7+VhNkcAPnFM",
                                    "segmentId": "GIlvAEBhEAzotzYz"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AMmcQK2Ujlc4QO0UTn0kOYUcIXWcBpBeckV4RA6bnTvco",
                                    "segmentId": "GC9TibS0tInxA2RT"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AtDfGMjCVdAZYmffPchLYpF1NiBtzQnn7K7Z5jKJ8v9Hk",
                                    "segmentId": "Gt43HwX9mizqgHNq"
                                }
                            ],
                            "baseFareTotal": "USD526.00",
                            "saleFareTotal": "BDT41291",
                            "saleTaxTotal": "BDT3216",
                            "saleTotal": "BDT44507",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT118"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/CCU Q3.00 103.00K2WOW1BD 9W X/BOM 220.00Q2VIFOW 9W DEL 200.00Q2VIFOW NUC 526.00 END ROE 1.00 FARE USD 526.00 EQU BDT 41291 XT 500BD 75E5 500OW 1200UT 823IN 118YR",
                            "latestTicketingTime": "2017-02-17T06:19-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT48138",
                    "id": "8Ch3keGdt21OmsqPe5AAWR006",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 435,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 205,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "275"
                                    },
                                    "id": "GUR8wOkwfwDEo2Pj",
                                    "cabin": "COACH",
                                    "bookingCode": "N",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LcgnhVa8w5VFJbvy",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T16:10+05:30",
                                            "departureTime": "2017-02-17T13:15+06:00",
                                            "origin": "DAC",
                                            "destination": "BOM",
                                            "destinationTerminal": "2",
                                            "duration": 205,
                                            "mileage": 1172,
                                            "meal": "Lunch"
                                        }
                                    ],
                                    "connectionDuration": 105
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 125,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "311"
                                    },
                                    "id": "GkB4uuqSwWSFC9qR",
                                    "cabin": "COACH",
                                    "bookingCode": "N",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Ltc2GgBy6BI58ONS",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T20:00+05:30",
                                            "departureTime": "2017-02-17T17:55+05:30",
                                            "origin": "BOM",
                                            "destination": "DEL",
                                            "originTerminal": "2",
                                            "destinationTerminal": "3",
                                            "duration": 125,
                                            "mileage": 706,
                                            "meal": "Snack or Brunch"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "ATcVX++k4Zt6utatpFbZ4pg4QEQCoc0oWTLBtPV8p582",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "N2WOW1BD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ATcVX++k4Zt6utatpFbZ4pg4QEQCoc0oWTLBtPV8p582",
                                    "segmentId": "GUR8wOkwfwDEo2Pj"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ATcVX++k4Zt6utatpFbZ4pg4QEQCoc0oWTLBtPV8p582",
                                    "segmentId": "GkB4uuqSwWSFC9qR"
                                }
                            ],
                            "baseFareTotal": "USD573.00",
                            "saleFareTotal": "BDT44981",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT48138",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/BOM S55.00 9W DEL Q DACDEL3.00 515.00N2WOW1BD NUC 573.00 END ROE 1.00 FARE USD 573.00 EQU BDT 44981 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T02:14-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT48138",
                    "id": "8Ch3keGdt21OmsqPe5AAWR008",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 525,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 205,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "275"
                                    },
                                    "id": "GUR8wOkwfwDEo2Pj",
                                    "cabin": "COACH",
                                    "bookingCode": "N",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LcgnhVa8w5VFJbvy",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T16:10+05:30",
                                            "departureTime": "2017-02-17T13:15+06:00",
                                            "origin": "DAC",
                                            "destination": "BOM",
                                            "destinationTerminal": "2",
                                            "duration": 205,
                                            "mileage": 1172,
                                            "meal": "Lunch"
                                        }
                                    ],
                                    "connectionDuration": 190
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 130,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "309"
                                    },
                                    "id": "G3M-w6XpHXPWhbHn",
                                    "cabin": "COACH",
                                    "bookingCode": "N",
                                    "bookingCodeCount": 7,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "L68P52vDF4amMVxp",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T21:30+05:30",
                                            "departureTime": "2017-02-17T19:20+05:30",
                                            "origin": "BOM",
                                            "destination": "DEL",
                                            "originTerminal": "2",
                                            "destinationTerminal": "3",
                                            "duration": 130,
                                            "mileage": 706,
                                            "meal": "Dinner"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "ATcVX++k4Zt6utatpFbZ4pg4QEQCoc0oWTLBtPV8p582",
                                    "carrier": "9W",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "N2WOW1BD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ATcVX++k4Zt6utatpFbZ4pg4QEQCoc0oWTLBtPV8p582",
                                    "segmentId": "GUR8wOkwfwDEo2Pj"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "ATcVX++k4Zt6utatpFbZ4pg4QEQCoc0oWTLBtPV8p582",
                                    "segmentId": "G3M-w6XpHXPWhbHn"
                                }
                            ],
                            "baseFareTotal": "USD573.00",
                            "saleFareTotal": "BDT44981",
                            "saleTaxTotal": "BDT3157",
                            "saleTotal": "BDT48138",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "YR_F",
                                    "chargeType": "CARRIER_SURCHARGE",
                                    "code": "YR",
                                    "salePrice": "BDT59"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                }
                            ],
                            "fareCalculation": "DAC 9W X/BOM S55.00 9W DEL Q DACDEL3.00 515.00N2WOW1BD NUC 573.00 END ROE 1.00 FARE USD 573.00 EQU BDT 44981 XT 500BD 75E5 500OW 1200UT 823IN 59YR",
                            "latestTicketingTime": "2017-02-17T02:14-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT55364",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00B",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 590,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 150,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "322"
                                    },
                                    "id": "GSbUl5FZIEG9B0bx",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Llq-gmq8JCW3HycI",
                                            "aircraft": "777",
                                            "arrivalTime": "2017-02-17T17:10+07:00",
                                            "departureTime": "2017-02-17T13:40+06:00",
                                            "origin": "DAC",
                                            "destination": "BKK",
                                            "duration": 150,
                                            "mileage": 973,
                                            "meal": "Meal"
                                        }
                                    ],
                                    "connectionDuration": 170
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 270,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "315"
                                    },
                                    "id": "G1NZhMk+GKppraFm",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LycOmwqHQmqHC6KD",
                                            "aircraft": "773",
                                            "arrivalTime": "2017-02-17T23:00+05:30",
                                            "departureTime": "2017-02-17T20:00+07:00",
                                            "origin": "BKK",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 270,
                                            "mileage": 1831,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AEGhbrtOnov2YdhD42Hha028rFQBYHPP+Qjpvq6R+uYA",
                                    "carrier": "TG",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "KRLOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AEGhbrtOnov2YdhD42Hha028rFQBYHPP+Qjpvq6R+uYA",
                                    "segmentId": "GSbUl5FZIEG9B0bx"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AEGhbrtOnov2YdhD42Hha028rFQBYHPP+Qjpvq6R+uYA",
                                    "segmentId": "G1NZhMk+GKppraFm"
                                }
                            ],
                            "baseFareTotal": "USD658.00",
                            "saleFareTotal": "BDT51653",
                            "saleTaxTotal": "BDT3711",
                            "saleTotal": "BDT55364",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "G8_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "G8",
                                    "country": "TH",
                                    "salePrice": "BDT34"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E7_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E7",
                                    "country": "TH",
                                    "salePrice": "BDT79"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT1000"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                }
                            ],
                            "fareCalculation": "DAC TG X/BKK TG DEL 658.00KRLOWBD NUC 658.00 END ROE 1.00 FARE USD 658.00 EQU BDT 51653 XT 500BD 75E5 1000OW 1200UT 79E7 34G8 823IN",
                            "latestTicketingTime": "2017-02-17T02:39-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT55364",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00I",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 790,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 150,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "322"
                                    },
                                    "id": "GSbUl5FZIEG9B0bx",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Llq-gmq8JCW3HycI",
                                            "aircraft": "777",
                                            "arrivalTime": "2017-02-17T17:10+07:00",
                                            "departureTime": "2017-02-17T13:40+06:00",
                                            "origin": "DAC",
                                            "destination": "BKK",
                                            "duration": 150,
                                            "mileage": 973,
                                            "meal": "Meal"
                                        }
                                    ],
                                    "connectionDuration": 375
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 265,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "331"
                                    },
                                    "id": "GVLCv7mD7RgwiDZS",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LQGgJ0Px-NPHEYU2",
                                            "aircraft": "787",
                                            "arrivalTime": "2017-02-18T02:20+05:30",
                                            "departureTime": "2017-02-17T23:25+07:00",
                                            "origin": "BKK",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 265,
                                            "mileage": 1831,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AEGhbrtOnov2YdhD42Hha028rFQBYHPP+Qjpvq6R+uYA",
                                    "carrier": "TG",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "KRLOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AEGhbrtOnov2YdhD42Hha028rFQBYHPP+Qjpvq6R+uYA",
                                    "segmentId": "GSbUl5FZIEG9B0bx"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AEGhbrtOnov2YdhD42Hha028rFQBYHPP+Qjpvq6R+uYA",
                                    "segmentId": "GVLCv7mD7RgwiDZS"
                                }
                            ],
                            "baseFareTotal": "USD658.00",
                            "saleFareTotal": "BDT51653",
                            "saleTaxTotal": "BDT3711",
                            "saleTotal": "BDT55364",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "G8_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "G8",
                                    "country": "TH",
                                    "salePrice": "BDT34"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E7_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E7",
                                    "country": "TH",
                                    "salePrice": "BDT79"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT1000"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                }
                            ],
                            "fareCalculation": "DAC TG X/BKK TG DEL 658.00KRLOWBD NUC 658.00 END ROE 1.00 FARE USD 658.00 EQU BDT 51653 XT 500BD 75E5 1000OW 1200UT 79E7 34G8 823IN",
                            "latestTicketingTime": "2017-02-17T02:39-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT66747",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00C",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 505,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 150,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "340"
                                    },
                                    "id": "GZngXBiM7zJpkt14",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LMP2lnC-qjmjMjfE",
                                            "aircraft": "777",
                                            "arrivalTime": "2017-02-17T05:30+07:00",
                                            "departureTime": "2017-02-17T02:00+06:00",
                                            "origin": "DAC",
                                            "destination": "BKK",
                                            "duration": 150,
                                            "mileage": 973,
                                            "meal": "Meal"
                                        }
                                    ],
                                    "connectionDuration": 90
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 265,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "323"
                                    },
                                    "id": "GEPiZ6gNRh-3wFFL",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LssY8BF2s+MyPRn1",
                                            "aircraft": "777",
                                            "arrivalTime": "2017-02-17T09:55+05:30",
                                            "departureTime": "2017-02-17T07:00+07:00",
                                            "origin": "BKK",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 265,
                                            "mileage": 1831,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AjZI6eUU7+iha65tctGLeNLq2hg2652xgzbFIG+mPZ5A",
                                    "carrier": "TG",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "QRLOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AjZI6eUU7+iha65tctGLeNLq2hg2652xgzbFIG+mPZ5A",
                                    "segmentId": "GZngXBiM7zJpkt14"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AjZI6eUU7+iha65tctGLeNLq2hg2652xgzbFIG+mPZ5A",
                                    "segmentId": "GEPiZ6gNRh-3wFFL"
                                }
                            ],
                            "baseFareTotal": "USD803.00",
                            "saleFareTotal": "BDT63036",
                            "saleTaxTotal": "BDT3711",
                            "saleTotal": "BDT66747",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "G8_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "G8",
                                    "country": "TH",
                                    "salePrice": "BDT34"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E7_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E7",
                                    "country": "TH",
                                    "salePrice": "BDT79"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT1000"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                }
                            ],
                            "fareCalculation": "DAC TG X/BKK TG DEL 803.00QRLOWBD NUC 803.00 END ROE 1.00 FARE USD 803.00 EQU BDT 63036 XT 500BD 75E5 1000OW 1200UT 79E7 34G8 823IN",
                            "latestTicketingTime": "2017-02-16T14:59-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT66860",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00D",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 1290,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 150,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "340"
                                    },
                                    "id": "GZngXBiM7zJpkt14",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LMP2lnC-qjmjMjfE",
                                            "aircraft": "777",
                                            "arrivalTime": "2017-02-17T05:30+07:00",
                                            "departureTime": "2017-02-17T02:00+06:00",
                                            "origin": "DAC",
                                            "destination": "BKK",
                                            "duration": 150,
                                            "mileage": 973,
                                            "meal": "Meal"
                                        }
                                    ],
                                    "connectionDuration": 870
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 270,
                                    "flight": {
                                        "carrier": "TG",
                                        "number": "315"
                                    },
                                    "id": "G1NZhMk+GKppraFm",
                                    "cabin": "COACH",
                                    "bookingCode": "Q",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "LycOmwqHQmqHC6KD",
                                            "aircraft": "773",
                                            "arrivalTime": "2017-02-17T23:00+05:30",
                                            "departureTime": "2017-02-17T20:00+07:00",
                                            "origin": "BKK",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 270,
                                            "mileage": 1831,
                                            "meal": "Meal"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AjZI6eUU7+iha65tctGLeNLq2hg2652xgzbFIG+mPZ5A",
                                    "carrier": "TG",
                                    "origin": "DAC",
                                    "destination": "DEL",
                                    "basisCode": "QRLOWBD"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AjZI6eUU7+iha65tctGLeNLq2hg2652xgzbFIG+mPZ5A",
                                    "segmentId": "G1NZhMk+GKppraFm"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AjZI6eUU7+iha65tctGLeNLq2hg2652xgzbFIG+mPZ5A",
                                    "segmentId": "GZngXBiM7zJpkt14"
                                }
                            ],
                            "baseFareTotal": "USD803.00",
                            "saleFareTotal": "BDT63036",
                            "saleTaxTotal": "BDT3824",
                            "saleTotal": "BDT66860",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "G8_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "G8",
                                    "country": "TH",
                                    "salePrice": "BDT34"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E7_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E7",
                                    "country": "TH",
                                    "salePrice": "BDT158"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT1000"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "G8_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "G8",
                                    "country": "TH",
                                    "salePrice": "BDT34"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                }
                            ],
                            "fareCalculation": "DAC TG X/BKK TG DEL 803.00QRLOWBD NUC 803.00 END ROE 1.00 FARE USD 803.00 EQU BDT 63036 XT 500BD 75E5 1000OW 1200UT 158E7 68G8 823IN",
                            "latestTicketingTime": "2017-02-16T14:59-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                },
                {
                    "kind": "qpxexpress#tripOption",
                    "saleTotal": "BDT84645",
                    "id": "8Ch3keGdt21OmsqPe5AAWR00E",
                    "slice": [
                        {
                            "kind": "qpxexpress#sliceInfo",
                            "duration": 910,
                            "segment": [
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 150,
                                    "flight": {
                                        "carrier": "BG",
                                        "number": "88"
                                    },
                                    "id": "GIV3g3LuzzXxDyAn",
                                    "cabin": "COACH",
                                    "bookingCode": "K",
                                    "bookingCodeCount": 4,
                                    "marriedSegmentGroup": "0",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Lpiekjcgjh1Wzb68",
                                            "aircraft": "738",
                                            "arrivalTime": "2017-02-17T12:15+07:00",
                                            "departureTime": "2017-02-17T08:45+06:00",
                                            "origin": "DAC",
                                            "destination": "BKK",
                                            "duration": 150,
                                            "mileage": 973,
                                            "meal": "Meal"
                                        }
                                    ],
                                    "connectionDuration": 480
                                },
                                {
                                    "kind": "qpxexpress#segmentInfo",
                                    "duration": 280,
                                    "flight": {
                                        "carrier": "9W",
                                        "number": "63"
                                    },
                                    "id": "G9S4nL+CCy6OmNvk",
                                    "cabin": "COACH",
                                    "bookingCode": "Y",
                                    "bookingCodeCount": 2,
                                    "marriedSegmentGroup": "1",
                                    "leg": [
                                        {
                                            "kind": "qpxexpress#legInfo",
                                            "id": "Ltk+poAdNPNqiPnW",
                                            "aircraft": "73H",
                                            "arrivalTime": "2017-02-17T23:25+05:30",
                                            "departureTime": "2017-02-17T20:15+07:00",
                                            "origin": "BKK",
                                            "destination": "DEL",
                                            "destinationTerminal": "3",
                                            "duration": 280,
                                            "mileage": 1831,
                                            "meal": "Dinner"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "pricing": [
                        {
                            "kind": "qpxexpress#pricingInfo",
                            "fare": [
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "Ak+9l/pxJvw6SR305WRpVyeg+6FoBgHB7ElWgZ1M",
                                    "carrier": "BG",
                                    "origin": "DAC",
                                    "destination": "BKK",
                                    "basisCode": "KBDO"
                                },
                                {
                                    "kind": "qpxexpress#fareInfo",
                                    "id": "AeWuvIgCOpdGJ5Fvxua4YWBbJoZPosbSFjEMDNOfEf32",
                                    "carrier": "YY",
                                    "origin": "BKK",
                                    "destination": "DEL",
                                    "basisCode": "YIF"
                                }
                            ],
                            "segmentPricing": [
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "Ak+9l/pxJvw6SR305WRpVyeg+6FoBgHB7ElWgZ1M",
                                    "segmentId": "GIV3g3LuzzXxDyAn"
                                },
                                {
                                    "kind": "qpxexpress#segmentPricing",
                                    "fareId": "AeWuvIgCOpdGJ5Fvxua4YWBbJoZPosbSFjEMDNOfEf32",
                                    "segmentId": "G9S4nL+CCy6OmNvk"
                                }
                            ],
                            "baseFareTotal": "USD1031.00",
                            "saleFareTotal": "BDT80934",
                            "saleTaxTotal": "BDT3711",
                            "saleTotal": "BDT84645",
                            "passengers": {
                                "kind": "qpxexpress#passengerCounts",
                                "adultCount": 1
                            },
                            "tax": [
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "G8_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "G8",
                                    "country": "TH",
                                    "salePrice": "BDT34"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E7_002",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E7",
                                    "country": "TH",
                                    "salePrice": "BDT79"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "BD_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "BD",
                                    "country": "BD",
                                    "salePrice": "BDT500"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "E5_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "E5",
                                    "country": "BD",
                                    "salePrice": "BDT75"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "OW_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "OW",
                                    "country": "BD",
                                    "salePrice": "BDT1000"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "UT_001",
                                    "chargeType": "GOVERNMENT",
                                    "code": "UT",
                                    "country": "BD",
                                    "salePrice": "BDT1200"
                                },
                                {
                                    "kind": "qpxexpress#taxInfo",
                                    "id": "IN_003",
                                    "chargeType": "GOVERNMENT",
                                    "code": "IN",
                                    "country": "IN",
                                    "salePrice": "BDT823"
                                }
                            ],
                            "fareCalculation": "DAC BG X/BKK 196.00KBDO 9W DEL M 835.25YIFOW2 NUC 1031.25 END ROE 1.00 FARE USD 1031.00 EQU BDT 80934 XT 500BD 75E5 1000OW 1200UT 79E7 34G8 823IN",
                            "latestTicketingTime": "2017-02-14T22:58-05:00",
                            "ptc": "ADT",
                            "refundable": true
                        }
                    ]
                }
            ]
        }
        };

    }
})();
