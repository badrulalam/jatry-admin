/**
 * Created by mahfuz on 11/30/15.
 */
(function(){
    'use strict';

    var app = angular.module('app', ['angular-loading-bar', 'ngAnimate','ui.bootstrap', 'angular.filter', 'ngSanitize', 'ngMaterial', 'directives.rangeSlider', 'rzModule']);
    app.controller('SearchCtrl', ["$scope", "$http" ,"$uibModal", "$log", function ($scope, $http,$uibModal, $log) {

        /*
         *material
         *
         * */
//

        /*
         *
         * end
         * */
        var _selected;
        $scope.airportJson = airportJson;
        $scope.selected = undefined;
        $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming', 'ngMdIcons'];
        // Any function returning a promise object can be used to load values asynchronously
        $scope.getLocation = function(val) {
            return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: val,
                    sensor: false
                }
            }).then(function(response){
                return response.data.results.map(function(item){
                    return item.formatted_address;
                });
            });
        };

        $scope.ngModelOptionsSelected = function(value) {
            if (arguments.length) {
                _selected = value;
            } else {
                return _selected;
            }
        };

        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };





        /*
         *
         * sidebar
         *
         * */
        $scope.openMe = function () {
            console.log("ioen");
        };
        $scope.sidebarStatus = false;


        /*
         *
         * start modal
         *
         * */
        $scope.items = ['item1', 'item2', 'item3'];

        $scope.animationsEnabled = true;

        $scope.openCart = function (size) { console.log(size)

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '/template/cart.modal.html',
                controller: 'CartModalCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $http.get('/api/air-ticket')
                            .then(function (data) {
                                console.log(data);
                                return data.data;
                            })
                    },
                    airlines: function () {
                        return $scope.airlines;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        /*
         *
         *  end modal
         *
         * */
        $scope.itemArray = [
            {id: 1, name: 'first'},
            {id: 2, name: 'second'},
            {id: 3, name: 'third'},
            {id: 4, name: 'fourth'},
            {id: 5, name: 'fifth'},
        ];
        $scope.placement = {
            options: [
                'Economy',
                'Premium Economy',
                'Business',
                'First Class'
            ],
            selected: 'Economy'
        };

        $scope.selected = { value: $scope.itemArray[0] };

        $scope.obj = {};
        $scope.obj.adults = 1;
        $scope.obj.childs = 0;
        $scope.obj.infant = 0;
        $scope.counterIncrease = function (data) {
            data++;
        }
        $scope.state = false;

        $scope.toggleState = function () {
            $scope.state = !$scope.state;
        };
        $scope.fuzz = "";

        $scope.search = function (myObj, type) {

            var searchObj = {};

            searchObj.adults = myObj.adults;
            searchObj.childs = myObj.childs;
            searchObj.infant = myObj.infant;

            if (type == 'O'){
                searchObj.fromAirport = myObj.fromAirport.ac;
                searchObj.toAirport = myObj.toAirport.ac;
                searchObj.departureDate = myObj.departureDate;
            }
            else if (type == 'R'){

                searchObj.fromAirport = myObj.fromAirport.ac;
                searchObj.toAirport = myObj.toAirport.ac;
                searchObj.departureDate = myObj.departureDate;
                searchObj.returnDate = myObj.returnDate;
            }
            else if (type == 'multicity_2'){
                searchObj.fromAirport1 = myObj.fromAirport1.ac;
                searchObj.toAirport1 = myObj.toAirport1.ac;
                searchObj.departureDate1 = myObj.departureDate1;
                searchObj.returnDate1 = myObj.returnDate1;

                searchObj.fromAirport2 = myObj.fromAirport2.ac;
                searchObj.toAirport2 = myObj.toAirport2.ac;
                searchObj.departureDate2 = myObj.departureDate2;
                searchObj.returnDate2 = myObj.returnDate2;
            }
            else if (type == 'multicity_3'){
                searchObj.fromAirport1 = myObj.fromAirport1.ac;
                searchObj.toAirport1 = myObj.toAirport1.ac;
                searchObj.departureDate1 = myObj.departureDate1;
                searchObj.returnDate1 = myObj.returnDate1;

                searchObj.fromAirport2 = myObj.fromAirport2.ac;
                searchObj.toAirport2 = myObj.toAirport2.ac;
                searchObj.departureDate2 = myObj.departureDate2;
                searchObj.returnDate2 = myObj.returnDate2;

                searchObj.fromAirport3 = myObj.fromAirport3.ac;
                searchObj.toAirport3 = myObj.toAirport3.ac;
                searchObj.departureDate3 = myObj.departureDate3;
                searchObj.returnDate3 = myObj.returnDate3;


            }
            else if (type == 'multicity_4'){
                searchObj.fromAirport1 = myObj.fromAirport1.ac;
                searchObj.toAirport1 = myObj.toAirport1.ac;
                searchObj.departureDate1 = myObj.departureDate1;
                searchObj.returnDate1 = myObj.returnDate1;

                searchObj.fromAirport2 = myObj.fromAirport2.ac;
                searchObj.toAirport2 = myObj.toAirport2.ac;
                searchObj.departureDate2 = myObj.departureDate2;
                searchObj.returnDate2 = myObj.returnDate2;

                searchObj.fromAirport3 = myObj.fromAirport3.ac;
                searchObj.toAirport3 = myObj.toAirport3.ac;
                searchObj.departureDate3 = myObj.departureDate3;
                searchObj.returnDate3 = myObj.returnDate3;

                searchObj.fromAirport4 = myObj.fromAirport4.ac;
                searchObj.toAirport4 = myObj.toAirport4.ac;
                searchObj.departureDate4 = myObj.departureDate4;
                searchObj.returnDate4 = myObj.returnDate4;
            }
            else if (type == 'multicity_5'){
                searchObj.fromAirport1 = myObj.fromAirport1.ac;
                searchObj.toAirport1 = myObj.toAirport1.ac;
                searchObj.departureDate1 = myObj.departureDate1;
                searchObj.returnDate1 = myObj.returnDate1;

                searchObj.fromAirport2 = myObj.fromAirport2.ac;
                searchObj.toAirport2 = myObj.toAirport2.ac;
                searchObj.departureDate2 = myObj.departureDate2;
                searchObj.returnDate2 = myObj.returnDate2;

                searchObj.fromAirport3 = myObj.fromAirport3.ac;
                searchObj.toAirport3 = myObj.toAirport3.ac;
                searchObj.departureDate3 = myObj.departureDate3;
                searchObj.returnDate3 = myObj.returnDate3;

                searchObj.fromAirport4 = myObj.fromAirport4.ac;
                searchObj.toAirport4 = myObj.toAirport4.ac;
                searchObj.departureDate4 = myObj.departureDate4;
                searchObj.returnDate4 = myObj.returnDate4;

                searchObj.fromAirport5 = myObj.fromAirport5.ac;
                searchObj.toAirport5 = myObj.toAirport5.ac;
                searchObj.departureDate5 = myObj.departureDate5;
                searchObj.returnDate5 = myObj.returnDate5;
            }
            else{
                return 'type not match';
            }

            $scope.isArray = angular.isArray;
            //console.log("search clicked!", myObj);
            searchObj.tripType = type;

            $http.post('/api/location/test', searchObj)
            // $http.get('http://192.168.0.111/blog/public/index.php/location/dummy')
            // $http.get('http://www.prothom-alo.com/')
                .success(function (data) { //console.log(data)
                    $scope.data = formatXml(data);
                    console.log($scope.data)
                })
                .error(function (data) {
                    //console.log("err", data)
                })
        };
        $scope.getSegmentByKey = function (segmentRef) {
            return $scope.data.Envelope.Body.LowFareSearchRsp.AirSegmentList.AirSegment.find(function (row) {
                return row._Key == segmentRef;
            });
        };
        $scope.getFlightDetailsByFlightDetailsRef = function (FlightDetailsRef) {
            return $scope.data.Envelope.Body.LowFareSearchRsp.FlightDetailsList.FlightDetails.find(function (row) {
                return row._Key == FlightDetailsRef;
            });
        };
        $scope.getArray = function (obj) {
            if(Array.isArray(obj))
                return obj;
            else return [obj]
        };
        $scope.showHide = {};
        $scope.counter = {};
        $scope.legList = {};
        $scope.toggleSelect = function (segmentKey, pricePoint, rawData) {
            if(!$scope.showHide.hasOwnProperty(pricePoint)){
                $scope.showHide[pricePoint] = {};
            }
            $scope.showHide[pricePoint][segmentKey] = !$scope.showHide[pricePoint][segmentKey];
            $scope.counter[pricePoint]++;
            $scope.legList[pricePoint].push({
                Key:segmentKey,
                Option: rawData,
                Segment: $scope.getSegmentByKey(rawData.BookingInfo._SegmentRef)
            })
        };
        $scope.getCounterValueByKey = function (key) {

            $scope.counter[key] = 0;
            $scope.legList[key] = [];
        };
        $scope.cartList = {};
        $scope.addToCart = function (AirPricePoint) {
            $scope.cartList[AirPricePoint._Key] = {
                AirPricePointKey: AirPricePoint._Key,
                legList: $scope.legList[AirPricePoint._Key],
                rawData: AirPricePoint,
                ApproximateTotalPrice: AirPricePoint._ApproximateTotalPrice,
                PlatingCarrier: AirPricePoint.AirPricingInfo._PlatingCarrier
            };
            $scope.setToCart($scope.cartList);
        };
        $scope.setToCart = function (data) {
            $http.put('/api/cart', {"content": JSON.stringify(data)})
                .then(function (data) {
                    console.log(data);
                })
        }
        $scope.removeFromCart = function (segmentKey) {
            delete $scope.cartList[segmentKey];
        };
        $scope.multicityTotal = 2;
        // $scope.totalSetect = 0;

        $scope.addAnotherFlight = function () {
            if($scope.multicityTotal < 4){
                $scope.multicityTotal++;
            }
        };

        $scope.location = {};
        $scope.locationShow = {};
        $scope.getName = function(name, input){
            if(name.length >= 3) {
                $scope.locationShow[input] = true;
                $http.get('/api/location/name?name=' + name)
                    .success(function (data) {
                        $scope.location[input] = data;

                    });
            }else if(name.length === 0){
                $scope.location[input] = {};
            }
        };
        $scope.selectName = function (index, input) {
            $scope.obj[input] = index;
            $scope.locationShow[input] = false;
        }
        $http.get('/xml/data.xml')
            .success(function (data) {
                // $scope.data = formatXml(data);

            })
    }]);
    app.directive('sidebarDirective', function () {
        return {
            link: function (scope, element, attr) {
                scope.$watch(attr.sidebarDirective, function (newVal) {
                    if (newVal) {
                        element.addClass('show');
                        return;
                    }
                    element.removeClass('show');
                });
            }
        };
    });
    app.filter('mydate', function () {
        return function (input, option1, option2) {
            var date = new Date(input);
            if(option1 === 'date')
                return date.toLocaleDateString();
            if(option1 === 'time')
                return date.toLocaleTimeString();
            return date.toLocaleString();
        }
    });
    app.filter('reverse', function() {
        return function(input, uppercase) {
            input = input || '';
            var out = '';
            for (var i = 0; i < input.length; i++) {
                out = input.charAt(i) + out;
            }
            // conditional based on optional argument
            if (uppercase) {
                out = out.toUpperCase();
            }
            return out;
        };
    });

    app.filter('airport', function() {
        return function(input) {
            if(!input){
                return;
            }
            var airport = airportJson.find(function (row) {
                return row.ac === input;
            });

            return airport ? airport : {};
        };
    });
    app.filter('airline', function() {
        return function(input) {
            if(!input){
                return;
            }
            var airport = airlinesJson.find(function (row) {
                return row.iata === input;
            });

            return airport ? airport : {};
        };
    });


    app.directive('ngSparkline', function() {
        return {
            restrict: 'A',
            template: '<div class="sparkline"></div>'
        }
    });
    app.filter('propsFilter', function() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function(item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });
    function formatXml(data) {
        var x2js = new X2JS();
        var msgJson = x2js.xml_str2json(data);
        return msgJson;
    }
    app.directive('masonryWrapper', function () {
        return{
            restrict: 'A',
            link: function (scope,element) {
                console.log("element",element)
                $(element).imagesLoaded(function(){
                    $(element).masonry({
                        // options...
                        itemSelector: '.grid-item',
                        columnWidth: '.grid-sizer',
                        percentPosition: true
                    });
                });
            }
        }
    })
})();
