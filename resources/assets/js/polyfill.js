/**
 * Created by mahfuz on 10/17/16.
 */
if (!Array.prototype.find) {
    Array.prototype.find = function(predicate) {
        'use strict';
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}
function hasObject(myArray, key, value) {

    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][key] === value) return true;
    }
    return false;
}
function getObject(myArray, key, value) {

    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][key] === value) return myArray[i];
    }
    return {};
}

Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm>9 ? '' : '0') + mm,
        (dd>9 ? '' : '0') + dd
    ].join('-');
};