(function () {
    'use strict';
    AppCtrl.$inject = ["$scope", "$http", "$timeout", "$q", "$mdPanel", "$log", "$window", "$location", "$httpParamSerializer"];
    PanelMenuCtrl.$inject = ["mdPanelRef", "$timeout", "$scope", "searchObj"];

    angular.module('app')
        .controller('HomeMaterialCtrl', AppCtrl)
        .controller('PanelMenuCtrl', PanelMenuCtrl)
        .directive('ddSpinner', ddSpinner)
        .directive('ddLocation', ddLocation)
        .directive('myCustomer', function () {
            return {
                restrict: 'E',
                template: 'Name: {{customer.name}} Address: {{customer.address}}'
            };
        });
    function AppCtrl($scope, $http, $timeout, $q, $mdPanel, $log, $window, $location, $httpParamSerializer) {









        /*
         *
         * */

        // $scope.tAirlines = ["Air India", "Alitalia", "Biman Bangladesh Airlines", "Gulf Air", "Jet Airways"];
        $scope.tLayoverAirport = ["Kolkata", "Hyderabad", "Mumbai", "Kualalumpur", "Chennai"];
        $scope.items = [1, 2, 3, 4, 5];
        $scope.tAirlines_selected = [];
        $scope.tLayoverAirport_selected = [];
        $scope.tStops = [{value: 1, view: "Non Stop"}, {value: 2, view: "1 Stop"}, {value: 3, view: "2 Stops+"}];
        $scope.tStopsSelected = [];


        $scope.tLayoverTime = [
            {min: 0, max: 5, view: "0h - 5h"},
            {min: 5, max: 10, view: "5h - 10h"},
            {min: 10, max: 15, view: "10h - 15h"},
            {min: 15, max: 999, view: "15h+"}
        ];
        $scope.tLayoverTimeSelected = [];


        $scope.tFareType = [
            {value: "true", view: "Refundable"},
            {value: "false", view: "Non Refundable"}
        ];
        $scope.tFareTypeSelected = [];

        $scope.tDepartureTime = [
            {min: 0, max: 6, view: "00 - 06"},
            {min: 6, max: 12, view: "6 - 12"},
            {min: 12, max: 18, view: "12 - 18"},
            {min: 18, max: 24, view: "18 - 24"}
        ];
        $scope.tDepartureTimeSelected = [];


        $scope.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
        };
        $scope.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };
        /*
         *
         *
         * */
        $scope.currentDate = new Date();
        var self = this;
        this._mdPanel = $mdPanel;

        $scope.customer = {
            name: 'Naomi',
            address: '1600 Amphitheatre'
        };
        $scope.searchObj = {};
        $scope.searchObj.adults = 1;
        $scope.searchObj.childs = 0;
        $scope.searchObj.infant = 0;
        $scope.searchObj.class = "Economy";
        $scope.searchObj.tripType = "O";
        $scope.currentNavItem = 'FLIGHTS';
        $scope.airportJson = airportJson;
        $scope.multicity = [{},{}];
        $scope.addMulticity = function () {
            $scope.multicity.push({});
        };
        $scope.removeMulticity = function (index) {
            $scope.multicity.splice(index, 1);
        };
        var self = this;
        // list of `state` value/display objects
        self.states = loadAll();
        self.selectedItem = null;
        self.searchText = null;
        self.querySearch = querySearch;
        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch(query) {


            var results = query ? self.states.filter(createFilterFor(query)) : self.states;
            console.log(results);
            var deferred = $q.defer();
            //$timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            $timeout(function () {
                deferred.resolve(results);
            }, 50, false);
            return deferred.promise;
        }

        /**
         * Build `states` list of key/value pairs
         */

        function loadAll() {
            return airportJson.map(function (state) {
                return {
                    value: state.ct.toLowerCase(),
                    display: state.ct + ', ' + state.cn + ' (' + state.ac + ')',
                    ac: state.ac,
                    an: state.an,
                    han: state.han,
                    cn: state.cn,
                    hcn: state.hcn,
                    cc: state.cc,
                    ct: state.ct,
                    hct: state.hct
                };
            });
            //return airportJson;
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };
        }

        $scope.searchTrigger_old = function () {
            console.log($scope.searchObj);
            var triggerObj = {};

            triggerObj.adults = $scope.searchObj.adults;
            triggerObj.childs = $scope.searchObj.childs;
            triggerObj.infant = $scope.searchObj.infant;

            if ($scope.searchObj.tripType == "O") {
                triggerObj.fromAirport = $scope.searchObj.origin.ac;
                triggerObj.toAirport = $scope.searchObj.destination.ac;
                triggerObj.departureDate = $scope.searchObj.departureDate;
            }
            else if ($scope.searchObj.tripType == "R") {

                triggerObj.fromAirport = $scope.searchObj.fromAirport.ac;
                triggerObj.toAirport = $scope.searchObj.toAirport.ac;
                triggerObj.departureDate = $scope.searchObj.departureDate;
                triggerObj.returnDate = $scope.searchObj.returnDate;
            }
            else if ($scope.searchObj.tripType == "M") {
                triggerObj.fromAirport1 = $scope.searchObj.fromAirport1.ac;
                triggerObj.toAirport1 = $scope.searchObj.toAirport1.ac;
                triggerObj.departureDate1 = $scope.searchObj.departureDate1;
                triggerObj.returnDate1 = $scope.searchObj.returnDate1;

                triggerObj.fromAirport2 = $scope.searchObj.fromAirport2.ac;
                triggerObj.toAirport2 = $scope.searchObj.toAirport2.ac;
                triggerObj.departureDate2 = $scope.searchObj.departureDate2;
                triggerObj.returnDate2 = $scope.searchObj.returnDate2;
            }
            else if ($scope.searchObj.tripType == 'multicity_3') {
                triggerObj.fromAirport1 = $scope.searchObj.fromAirport1.ac;
                triggerObj.toAirport1 = $scope.searchObj.toAirport1.ac;
                triggerObj.departureDate1 = $scope.searchObj.departureDate1;
                triggerObj.returnDate1 = $scope.searchObj.returnDate1;

                triggerObj.fromAirport2 = $scope.searchObj.fromAirport2.ac;
                triggerObj.toAirport2 = $scope.searchObj.toAirport2.ac;
                triggerObj.departureDate2 = $scope.searchObj.departureDate2;
                triggerObj.returnDate2 = $scope.searchObj.returnDate2;

                triggerObj.fromAirport3 = $scope.searchObj.fromAirport3.ac;
                triggerObj.toAirport3 = $scope.searchObj.toAirport3.ac;
                triggerObj.departureDate3 = $scope.searchObj.departureDate3;
                triggerObj.returnDate3 = $scope.searchObj.returnDate3;


            }
            else if ($scope.searchObj.tripType == 'multicity_4') {
                triggerObj.fromAirport1 = $scope.searchObj.fromAirport1.ac;
                triggerObj.toAirport1 = $scope.searchObj.toAirport1.ac;
                triggerObj.departureDate1 = $scope.searchObj.departureDate1;
                triggerObj.returnDate1 = $scope.searchObj.returnDate1;

                triggerObj.fromAirport2 = $scope.searchObj.fromAirport2.ac;
                triggerObj.toAirport2 = $scope.searchObj.toAirport2.ac;
                triggerObj.departureDate2 = $scope.searchObj.departureDate2;
                triggerObj.returnDate2 = $scope.searchObj.returnDate2;

                triggerObj.fromAirport3 = $scope.searchObj.fromAirport3.ac;
                triggerObj.toAirport3 = $scope.searchObj.toAirport3.ac;
                triggerObj.departureDate3 = $scope.searchObj.departureDate3;
                triggerObj.returnDate3 = $scope.searchObj.returnDate3;

                triggerObj.fromAirport4 = $scope.searchObj.fromAirport4.ac;
                triggerObj.toAirport4 = $scope.searchObj.toAirport4.ac;
                triggerObj.departureDate4 = $scope.searchObj.departureDate4;
                triggerObj.returnDate4 = $scope.searchObj.returnDate4;
            }
            else if ($scope.searchObj.tripType == 'multicity_5') {
                triggerObj.fromAirport1 = $scope.searchObj.fromAirport1.ac;
                triggerObj.toAirport1 = $scope.searchObj.toAirport1.ac;
                triggerObj.departureDate1 = $scope.searchObj.departureDate1;
                triggerObj.returnDate1 = $scope.searchObj.returnDate1;

                triggerObj.fromAirport2 = $scope.searchObj.fromAirport2.ac;
                triggerObj.toAirport2 = $scope.searchObj.toAirport2.ac;
                triggerObj.departureDate2 = $scope.searchObj.departureDate2;
                triggerObj.returnDate2 = $scope.searchObj.returnDate2;

                triggerObj.fromAirport3 = $scope.searchObj.fromAirport3.ac;
                triggerObj.toAirport3 = $scope.searchObj.toAirport3.ac;
                triggerObj.departureDate3 = $scope.searchObj.departureDate3;
                triggerObj.returnDate3 = $scope.searchObj.returnDate3;

                triggerObj.fromAirport4 = $scope.searchObj.fromAirport4.ac;
                triggerObj.toAirport4 = $scope.searchObj.toAirport4.ac;
                triggerObj.departureDate4 = $scope.searchObj.departureDate4;
                triggerObj.returnDate4 = $scope.searchObj.returnDate4;

                triggerObj.fromAirport5 = $scope.searchObj.fromAirport5.ac;
                triggerObj.toAirport5 = $scope.searchObj.toAirport5.ac;
                triggerObj.departureDate5 = $scope.searchObj.departureDate5;
                triggerObj.returnDate5 = $scope.searchObj.returnDate5;
            }
            else {
                return 'type not match';
            }
            console.log("in last");
            $scope.isArray = angular.isArray;
            //console.log("search clicked!", myObj);
            triggerObj.tripType = $scope.searchObj.tripType;

            $http.post('/api/location/test', triggerObj)
            // $http.get('http://192.168.0.111/blog/public/index.php/location/dummy')
            // $http.get('http://www.prothom-alo.com/')
                .success(function (data) { //console.log(data)
                    $scope.data = formatXml(data);
                    console.log($scope.data)
                })
                .error(function (data) {
                    //console.log("err", data)
                })
        };

        $scope.searchTrigger_old2 = function () {


            console.log($httpParamSerializer($scope.searchObj));
            var obj = {};
            obj.tripType = $scope.searchObj.tripType;
            obj.origin = $scope.searchObj.origin.ac;
            obj.destination = $scope.searchObj.destination.ac;
            obj.departureDate = $scope.searchObj.departureDate;
            obj.arrivalDate = $scope.searchObj.arrivalDate;
            obj.adults = $scope.searchObj.adults;
            obj.childs = $scope.searchObj.childs;
            obj.infant = $scope.searchObj.infant;
            obj.class = $scope.searchObj.class;

            var changeLocation = function (url, forceReload) {
                $scope = $scope || angular.element(document).scope();
                // alert(forceReload)
                // alert($scope.$$phase)
                if (forceReload || $scope.$$phase) {
                    window.location = url;

                }
                else {
                    //only use this if you want to replace the history stack
                    //$location.path(url).replace();

                    //this this if you want to change the URL and add it to the history stack
                    $location.path(url);
                    $scope.$apply();
                }
            };
            changeLocation("/flights#?" + $httpParamSerializer(obj));
            // $window.location.reload();

            // obj.origin_1 = $scope.searchObj.origin_1.ac;
            // obj.destination_1 = $scope.searchObj.destination_1.ac;
            // obj.departureDate_1 = $scope.searchObj.departureDate_1;
            //
            // obj.origin_2 = $scope.searchObj.origin_2.ac;
            // obj.destination_2 = $scope.searchObj.destination_2.ac;
            // obj.departureDate_2 = $scope.searchObj.departureDate_2;
            //
            // obj.origin_3 = $scope.searchObj.origin_3.ac;
            // obj.destination_3 = $scope.searchObj.destination_3.ac;
            // obj.departureDate_3 = $scope.searchObj.departureDate_3;


        };
        $scope.searchTrigger = function () {
            var triggerObj = {};
            triggerObj.adults = $scope.searchObj.adults;
            triggerObj.childs = $scope.searchObj.childs;
            triggerObj.infant = $scope.searchObj.infant;
            triggerObj.class = $scope.searchObj.class;
            if($scope.searchObj.tripType === 'O'){
                triggerObj.tripType = $scope.searchObj.tripType;
                triggerObj.noOfSegments = 1;

                if($scope.searchObj.origin && $scope.searchObj.origin.ac){
                    triggerObj.origin = $scope.searchObj.origin.ac;
                }

                if($scope.searchObj.destination && $scope.searchObj.destination.ac){
                    triggerObj.destination = $scope.searchObj.destination.ac;
                }

                triggerObj.departureDate = $scope.searchObj.departureDate;
            }
            if($scope.searchObj.tripType === 'R'){
                triggerObj.tripType = $scope.searchObj.tripType;
                triggerObj.noOfSegments = 2;

                if($scope.searchObj.origin && $scope.searchObj.origin.ac){
                    triggerObj.origin = $scope.searchObj.origin.ac;
                }

                if($scope.searchObj.destination && $scope.searchObj.destination.ac){
                    triggerObj.destination = $scope.searchObj.destination.ac;
                }

                triggerObj.departureDate = $scope.searchObj.departureDate;
                triggerObj.arrivalDate = $scope.searchObj.arrivalDate;
            }
            if($scope.searchObj.tripType === 'M'){ console.log($scope.multicity)
                triggerObj.tripType = $scope.searchObj.tripType;
                triggerObj.noOfSegments = $scope.multicity.length;

                for(var i = 0; i < $scope.multicity.length; i++){
                    if($scope.multicity[i].origin && $scope.multicity[i].origin.ac){
                        triggerObj["origin_"+(i+1)] = $scope.multicity[i].origin.ac;
                    }

                    if($scope.multicity[i].destination && $scope.multicity[i].destination.ac){
                        triggerObj["destination_"+(i+1)] = $scope.multicity[i].destination.ac;
                    }

                    triggerObj["departureDate_"+(i+1)] = $scope.multicity[i].departureDate;
                }

            }
            var changeLocation = function (url, forceReload) {
                $scope = $scope || angular.element(document).scope();
                // alert(forceReload)
                // alert($scope.$$phase)
                if (forceReload || $scope.$$phase) {
                    window.location = url;

                }
                else {
                    //only use this if you want to replace the history stack
                    //$location.path(url).replace();

                    //this this if you want to change the URL and add it to the history stack
                    $location.path(url);
                    $scope.$apply();
                }
            };
            $scope.successData = {};
            $scope.errorData = {};
            $http.post('/api/trigger/validate',triggerObj)
                .then(function (data) {
                    // changeLocation("/flights#?" + $httpParamSerializer(triggerObj));
                    var prev = $window.location.href;
                    $window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);

                    if(prev != $window.location.href){
                        location.reload();

                    }

                },function (err) {
                    $scope.errorData = err;
                });


            // $window.location.href = '/flights#?' + $httpParamSerializer(triggerObj, true);



        };

        $scope.getData = function (obj) {
            $scope.isLoading = true;
            $http.post('/api/trigger/validate',obj)
                .then(function (data) {

                    $http.post('/api/trigger/sample', obj)
                    // $http.get('/xml/temp.json')
                        .success(function (res_data) {
                            $scope.allResponseData = res_data;
                            var data = res_data.Parse;
                            $scope.isLoading = false;

                            console.log(data);

                            if(!Array.isArray(data)){
                                $scope.fault = data;
                                return;
                            }


                            var airlinesList = [];
                            data.forEach(function (obj) {
                                airlinesList.push(obj.PlatingCarrier);
                            });
                            $scope.tAirlines = airlinesList.filter(function (item, i, ar) {
                                return ar.indexOf(item) === i;
                            });

                            var priceList = [];

                            data.forEach(function (obj) {
                                priceList.push(Number(obj.Price.TotalPrice.replace(/[^0-9\.]+/g, "")));
                            });

                            console.log(priceList);
                            $scope.rangeMax = Math.max.apply(null, priceList);
                            $scope.rangeMin = Math.min.apply(null, priceList);


                            if ($scope.searchObj.tripType === 'O') {
                                console.log("ooooooooooooooooooooooooooo")
                                $scope.tUrl = '/template/include/one-way.html';

                            }
                            if ($scope.searchObj.tripType === 'R') {
                                console.log("rrrrrrrrrrrrrrrrrrrrrrrrr")
                                $scope.tUrl = '/template/include/round-trip.html';

                            }
                            if ($scope.searchObj.tripType === 'M') {
                                $scope.tUrl = '/template/include/multicity.html';

                            }
                            $scope.pricingSolution = data;
                        });

                },function (err) {
                    $scope.errorData = err;
                    $scope.isLoading = false;

                });

        };
        // // temp.json
        // /************************/
        // $http.get('/xml/temp.json')
        //     .success(function (data) {
        //         console.log(data);
        //         var airlinesList = [];
        //         data.forEach(function (obj) {
        //             airlinesList.push(obj.PlatingCarrier);
        //         });
        //         $scope.tAirlines = airlinesList.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        //
        //         var priceList = [];
        //
        //         data.forEach(function (obj) {
        //             priceList.push(Number(obj.Price.TotalPrice.replace(/[^0-9\.]+/g,"")));
        //         });
        //
        //         console.log(priceList);
        //         $scope.rangeMax = Math.max.apply(null, priceList);
        //         $scope.rangeMin = Math.min.apply(null, priceList);
        //
        //
        //         if($scope.searchObj.tripType === 'O'){
        //             $scope.tUrl = '/template/include/one-way.html';
        //
        //         }if($scope.searchObj.tripType === 'R'){
        //             $scope.tUrl = '/template/include/round-trip.html';
        //
        //         }if($scope.searchObj.tripType === 'M'){
        //             $scope.tUrl = '/template/include/multicity.html';
        //
        //         }
        //         $scope.pricingSolution = data;
        //
        //
        //     });
        // //end
        self.showMenu = function (ev) {
            var position = this._mdPanel.newPanelPosition()
                .relativeTo('.demo-menu-open-button')
                .addPanelPosition(this._mdPanel.xPosition.ALIGN_START, this._mdPanel.yPosition.BELOW);
            var config = {
                attachTo: angular.element(document.body),
                controller: PanelMenuCtrl,
                controllerAs: 'ctrl',
                disableParentScroll: false,
                templateUrl: '/template/include/custom-dropdown-menu.html',

                panelClass: 'demo-menu-example',
                position: position,
                locals: {
                    'searchObj': $scope.searchObj

                },
                openFrom: ev,
                clickOutsideToClose: true,
                escapeToClose: true,
                focusOnOpen: false,
                zIndex: 2
            };
            this._mdPanel.open(config);
        };
        $http.get('/api.travelport/airlines.json')
            .success(function (data) {
                $scope.airlines = data;
            });
       
        console.log($location.search());
        if ($location.search() && $location.search().tripType) {
            /**************/
            $scope.searchObj.adults = Number($location.search().adults);
            $scope.searchObj.childs = Number($location.search().childs);
            $scope.searchObj.infant = Number($location.search().infant);
            $scope.searchObj.tripType = $location.search().tripType;
            $scope.searchObj.class = $location.search().class;
            $scope.searchObj.arrivalDate = new Date($location.search().arrivalDate);
            $scope.searchObj.departureDate = new Date($location.search().departureDate);
            if($location.search().origin){
                $scope.searchObj.origin = getObject(self.states, "ac", $location.search().origin);
            }
            if($location.search().destination){
                $scope.searchObj.destination = getObject(self.states, "ac", $location.search().destination);
            }
            if($location.search().tripType === 'M'){
                for(var i = 0; i< $location.search().noOfSegments; i++){
                    $scope.multicity[i] = {};
                    if($location.search()["origin_"+(i+1)]){
                        $scope.multicity[i].origin = getObject(self.states, "ac", $location.search()["origin_"+(i+1)]);
                    }
                    if($location.search()["destination_" + (i + 1)]) {
                        $scope.multicity[i].destination = getObject(self.states, "ac", $location.search()["destination_" + (i + 1)]);
                    }
                    $scope.multicity[i].departureDate = new Date($location.search()["departureDate_"+(i+1)]);
                }
            }
            /**************/
            $scope.searchParams = $location.search();
            $scope.getData($location.search());
        }
        $scope.$watch('searchObj', function (newValue, oldValue) {
            // console.log(newValue, oldValue);
            if (newValue.adults + newValue.childs > 9) {
                newValue.adults = oldValue.adults;
                newValue.childs = oldValue.childs;
            }
            if (newValue.adults < 1) {
                newValue.adults = 1;
            }
            if (newValue.adults < newValue.infant) {
                newValue.infant = newValue.adults;
            }
        }, true);

    }

    function PanelMenuCtrl(mdPanelRef, $timeout, $scope, searchObj) {
        $scope.searchObj = searchObj;
        this._mdPanelRef = mdPanelRef;
        // this.favoriteDessert = this.selected.favoriteDessert;
        $timeout(function () {
            var selected = document.querySelector('.demo-menu-item.selected');
            if (selected) {
                angular.element(selected).focus();
            } else {
                angular.element(document.querySelectorAll('.demo-menu-item')[0]).focus();
            }
        });
    }

    function ddSpinner() {
        return {
            restrict: "AE",
            scope: {
                bindedModel: "=ngModel"
            },
            template: '<div class="btn-group " role="group" aria-label="..." ng-init="bindedModel = 1*bindedModel">' +
            '<button class="btn btn-default" ng-click="bindedModel > 0 ? bindedModel = 1*bindedModel - 1 : null">-</button>' +
            '<button class="btn btn-default" >{{bindedModel}}</button>' +
            '<button class="btn btn-default" ng-click="bindedModel = 1*bindedModel + 1">+</button>' +
            '</div>',
            link: function (scope, element, attrs) {
                console.log(attrs);
            }
        }
    }

    function ddLocation() {
        return {
            restrict: "AE",
            scope: {
                bindedModel: "=ngModel"
            },
            template: '<div class="btn-group pull-right" role="group" aria-label="...">' +
            '<button class="btn btn-default" ng-click="bindedModel = bindedModel - 1">-</button>' +
            '<button class="btn btn-default" >{{bindedModel}}</button>' +
            '<button class="btn btn-default" ng-click="bindedModel = bindedModel + 1">+</button>' +
            '</div>',
            link: function (scope, element, attrs) {
                console.log(attrs);
            }
        }
    }

    function formatXml(data) {
        var x2js = new X2JS();
        var msgJson = x2js.xml_str2json(data);
        return msgJson;
    }

})();
(function () {
    'use strict';
    angular.module('app')
        .controller('PanelDialogCtrl', PanelDialogCtrl);

    function PanelDialogCtrl(mdPanelRef) {
        this._mdPanelRef = mdPanelRef;
    }

    PanelDialogCtrl.prototype.closeDialog = function () {
        var panelRef = this._mdPanelRef;
        panelRef && panelRef.close().then(function () {
            angular.element(document.querySelector('.demo-dialog-open-button')).focus();
            panelRef.destroy();
        });
    };


})();
(function () {
    angular.module('directives.rangeSlider', ['ngMaterial'])
        .directive('rangeSlider', function () {
            return {
                restrict: "E",
                scope: {
                    max: '=',
                    min: '=',
                    gap: '=?',
                    step: '=?',
                    lowerValue: "=",
                    upperValue: "="
                },
                templateUrl: '/template/include/range-slider.html',
                controller: ["$scope", function ($scope) {
                    var COMFORTABLE_STEP = $scope.step, // whether the step is comfortable that depends on u
                        tracker = $scope.tracker = {    // track style
                            width: 0,
                            left: 0,
                            right: 0
                        };

                    function updateSliders() {
                        if ($scope.upperValue - $scope.lowerValue > $scope.gap) {
                            $scope.lowerMaxLimit = $scope.lowerValue + COMFORTABLE_STEP;
                            $scope.upperMinLimit = $scope.upperValue - COMFORTABLE_STEP;
                        } else {
                            $scope.lowerMaxLimit = $scope.lowerValue;
                            $scope.upperMinLimit = $scope.upperValue;
                        }
                        updateSlidersStyle();
                    }

                    function updateSlidersStyle() {
                        // update sliders style
                        $scope.lowerWidth = $scope.lowerMaxLimit / $scope.max * 100;
                        $scope.upperWidth = ($scope.max - $scope.upperMinLimit) / $scope.max * 100;
                        // update tracker line style
                        tracker.width = 100 - $scope.lowerWidth - $scope.upperWidth;
                        tracker.left = $scope.lowerWidth || 0;
                        tracker.right = $scope.upperWidth || 0;
                    }

                    // watch lowerValue & upperValue to update sliders
                    $scope.$watchGroup(["lowerValue", "upperValue"], function (newVal) {
                        // filter the default initialization
                        if (newVal !== undefined) {
                            updateSliders();
                        }
                    });
                    // init
                    $scope.step = $scope.step || 1;
                    $scope.gap = $scope.gap || 0;
                    $scope.lowerMaxLimit = $scope.lowerValue + COMFORTABLE_STEP;
                    $scope.upperMinLimit = $scope.upperValue - COMFORTABLE_STEP;
                    updateSlidersStyle();
                }]
            };
        })
})();

