/**
 * Created by mahfuz on 11/30/15.
 */
(function () {
    'use strict';
    var app = angular.module('app');
    app.controller('SegmentCtrl', ["$scope", "$http", "$uibModal", "$log", "$httpParamSerializer", "$window", "$location", function ($scope, $http, $uibModal, $log, $httpParamSerializer, $window, $location) {
        $scope.pricingSolution.forEach(function (data) {
            function getDuration(d) {
                return d.d*24*60 + d.h*60 + d.i;
            }
            data.sorting = {
                Airline: data.PlatingCarrier,
                Depart:data.Journey.Routes[0].DepartureTime,
                Arrive:data.Journey.Routes[0].ArrivalTime,
                Duration: getDuration(data.Journey.Routes[0].Duration),
                Price:data.Price.ApproximateTotalPrice
            };

        });
        function initSorting() {
            return {
                Airline:{
                    up: false,
                    down: false
                },
                Depart:{
                    up: false,
                    down: false
                },
                Arrive:{
                    up: false,
                    down: false
                },
                Duration:{
                    up: false,
                    down: false
                },
                Price:{
                    up: false,
                    down: false
                }
            };
        }
        $scope.sorting = initSorting();
        $scope.sortingKey = "";
        $scope.getSortingRequest = function(obj){
            if(obj.up && obj.down){
                return false;
            }else if(obj.up){
                return 'down';
            }else if(obj.down){
                return 'up';
            }else{
                return 'up';
            }
        };
        $scope.setSortingRequest = function(obj, type, key){
            if(type === 'up') {
                obj.up = true;
                $scope.sortingKey = "" + "sorting." + key;
            }
            else if(type === 'down') {
                obj.down = true;
                $scope.sortingKey = "-" + "sorting." + key;
            }

        };
        $scope.sortItem = function (key) {
            var type = $scope.getSortingRequest($scope.sorting[key]);
            $scope.sorting = initSorting();
            $scope.setSortingRequest($scope.sorting[key], type, key)
        };



        $scope.slider = {
            minValue: 10,
            maxValue: 90,
            options: {
                floor: $scope.rangeMin,
                ceil: $scope.rangeMax,
                step: 1
            }
        };
        $scope.animationsEnabled = true;
        $scope.openFlightDetailsModal = function (row, size) {
            var index = $scope.pricingSolution.indexOf(row);
            var ps = $scope.pricingSolution[index];

            if (!size) {
                size = 'lg';
            }

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '/template/modal/flight-details.modal.html',
                controller: 'FlightDetailsModalCtrl',
                size: size,
                resolve: {
                    data: function () {
                        return angular.copy($scope.$parent.allResponseData);
                    },
                    key: function () {
                        return angular.copy($scope.pricingSolution[index]["Key"]);
                    },
                    parse: function () {
                        return angular.copy($scope.pricingSolution[index]);
                    },
                    airlines: function () {
                        return $scope.airlines;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.demo = function (row) {
            var index = $scope.pricingSolution.indexOf(row);
            console.log($scope.pricingSolution[index]);
        };
        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        $scope.getSegmentByKey = function (segmentRef) {
            return $scope.data.Envelope.Body.LowFareSearchRsp.AirSegmentList.AirSegment.find(function (row) {
                return row._Key == segmentRef;
            });
        };

        $scope.getFlightDetailsByFlightDetailsRef = function (FlightDetailsRef) {
            return $scope.data.Envelope.Body.LowFareSearchRsp.FlightDetailsList.FlightDetails.find(function (row) {
                return row._Key == FlightDetailsRef;
            });
        };

        $scope.getArray = function (obj) {
            if (Array.isArray(obj))
                return obj;
            else return [obj]
        };
        $scope.showHide = {};
        $scope.counter = {};
        $scope.legList = {};
        $scope.toggleSelect = function (segmentKey, pricePoint, rawData) {
            if (!$scope.showHide.hasOwnProperty(pricePoint)) {
                $scope.showHide[pricePoint] = {};
            }
            $scope.showHide[pricePoint][segmentKey] = !$scope.showHide[pricePoint][segmentKey];
            $scope.counter[pricePoint]++;
            $scope.legList[pricePoint].push({
                Key: segmentKey,
                Option: rawData,
                Segment: $scope.getSegmentByKey(rawData.BookingInfo._SegmentRef)
            })
        };
        $scope.getCounterValueByKey = function (key) {

            $scope.counter[key] = 0;
            $scope.legList[key] = [];
        };
        $scope.cartList = {};
        $scope.addToCart = function (AirPricePoint) {
            // console.log($scope.searchParams);
            // console.log(AirPricePoint);

            $http.post('/api/air-ticket', {AirPricePoint: AirPricePoint, SearchParams: $scope.searchParams})
                .then(function (data) {
                        $window.location.href = '/checkout-ui#?' + $httpParamSerializer({index: data.data.Key}, true);
                },
                function(err){
                    if (err.data == "not-login") {
                        if ($location.search() && $location.search().tripType) {
                            var modalInstance = $uibModal.open({
                                animation: true,
                                templateUrl: '/template/modal/traveller-login-book.modal.html',
                                controller: 'TravellerLoginBookModalCtrl',
                                size: 'lg',
                                resolve: {
                                    AirPricePoint: function () {
                                        return AirPricePoint
                                    },
                                    SearchParams: function () {
                                        return $scope.searchParams
                                    }
                                }
                            });
                        }
                    }
                });


        };

        $scope.removeFromCart = function (segmentKey) {
            delete $scope.cartList[segmentKey];
        };
        $scope.multicityTotal = 2;
        // $scope.totalSetect = 0;

        $scope.addAnotherFlight = function () {
            if ($scope.multicityTotal < 4) {
                $scope.multicityTotal++;
            }
        };


        $scope.selectName = function (index, input) {
            $scope.obj[input] = index;
            $scope.locationShow[input] = false;
        };


        $scope.showByFilter = function (row) {
            //-----------------//console.log($scope.slider.minValue,$scope.slider.maxValue)

            if ($scope.tAirlines_selected.length > 0)
                return $scope.tAirlines_selected.indexOf(row.PlatingCarrier) !== -1;

            if ($scope.tStopsSelected.length > 0) {
                var stops = row.Journey.Routes[0].Stops;
                var flag = false;
                $scope.tStopsSelected.forEach(function (stop) {
                    if (stop.value === stops || (stop.value === 3 && stop.value <= stops ))
                        flag = true;

                });
                return flag;
            }

            if ($scope.tLayoverTimeSelected.length > 0) {
                var d = row.Journey.Routes[0].Duration;
                var duration = d.d * 24 + d.h + d.i / 60;
                var flag = false;
                $scope.tLayoverTimeSelected.forEach(function (time) {
                    if (time.min <= duration && duration <= time.max)
                        flag = true;

                });
                return flag;
            }


            if ($scope.tFareTypeSelected.length > 0) {
                var fareType = "false";
                if (row.Refundable && row.Refundable === "true") {
                    fareType = "true";
                }
                $scope.tFareTypeSelected.forEach(function (type) {
                    console.log(type.value, fareType);
                    if (type.value === fareType)
                        flag = true;

                });
                return flag;
            }

            if ($scope.tDepartureTimeSelected.length > 0) {
                var date = new Date(row.Journey.Routes[0].DepartureTime);

                var depart = date.getHours();
                var flag = false;
                $scope.tDepartureTimeSelected.forEach(function (time) {
                    if (time.min <= depart && depart <= time.max)
                        flag = true;

                });
                return flag;
            }


            return true;
        };

    }]);
    app.directive('sidebarDirective', function () {
        return {
            link: function (scope, element, attr) {
                scope.$watch(attr.sidebarDirective, function (newVal) {
                    if (newVal) {
                        element.addClass('show');
                        return;
                    }
                    element.removeClass('show');
                });
            }
        };
    });
    app.filter('mydate', function () {
        return function (input, option1, option2) {
            var date = new Date(input);
            if (option1 === 'date')
                return date.toLocaleDateString();
            if (option1 === 'time')
                return date.toLocaleTimeString();
            return date.toLocaleString();
        }
    });
    app.directive('ngSparkline', function () {
        return {
            restrict: 'A',
            template: '<div class="sparkline"></div>'
        }
    });
    app.filter('propsFilter', function () {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function (item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });
})();
