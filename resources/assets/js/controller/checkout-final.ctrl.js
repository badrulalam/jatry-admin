(function () {
    'use strict';
    angular.module('app')
        .controller('CheckoutFinalCtrl', ["$scope","$http", "$location", "$window", "$httpParamSerializer", function($scope, $http, $location, $window, $httpParamSerializer) {
            $scope.payment = {};
            $scope.isProcessing = false;
            $scope.error = false;
            $scope.submit= function () {
                console.log($scope.payment);
                $scope.isProcessing = true;
                //$http.post('/api/checkout-complete',$scope.payment)
                $http.post('/api/checkout-complete')
                    .then(function (data) {
                        
                        $scope.isProcessing = false;
                        console.log("data", data);
                        if(data.data.flag == "true"){
                            $window.location.href = '/ticket-sale/'+data.data.invoice;

                        }else{
                            $scope.error = true;
                        }
                    })

            }
        }]);
})();
