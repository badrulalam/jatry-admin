/**
 * Created by mahfuz on 8/9/16.
 */
(function () {
    'use strict';
    angular.module('app')
        .controller('TravellerCardListModalCtrl', ["$scope", "$uibModalInstance", "$http", "travellercards", "$httpParamSerializer", "$window", function ($scope, $uibModalInstance, $http, travellercards, $httpParamSerializer,$window) {

            
            $scope.travellercards = travellercards;
            
            $scope.selected = {
                item: $scope.travellercards[0]
            };

            $scope.ok = function () {
                $uibModalInstance.close($scope.selected.item);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
    }]);
})();
