(function () {
    'use strict';
    angular.module('app')
        .controller('TravellerDetailsCtrl', ["$scope", "$http", "$location", "$window", "$httpParamSerializer","$uibModal", "$log", function ($scope, $http, $location, $window, $httpParamSerializer,$uibModal, $log) {
                $http.get('/api/air-ticket')
                    .then(function (data) {

                        $scope.passenger = data.data.ticket.passenger;
                        $scope.cart = data.data.ticket;
                        console.log(data.data);

                    });
        
            
            $http.get('/api/my-traveller')
                .then(function (data) {
                    $scope.myTraveller = data.data;
                });
            //$scope.payment = {}
            $scope.saveAndContinue = function () {
                console.log($scope.ticket);
                $http.put('/api/air-ticket', {passenger:$scope.passenger, customeradd:$scope.customeradd, custpayment:$scope.payment})
                    .then(function (data) {
                        console.log(data.data);
                        $window.location.href = '/confirm-ticket';
                    });
            };
            $scope.selectTraveller = function (index, size) { console.log(size)

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/template/traveller-details.modal.html',
                    controller: 'TravellerListModalCtrl',
                    size: size,
                    resolve: {
                        travellers: function () {
                            return $scope.myTraveller
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.updateTraveller($scope.passenger[index], selectedItem);
                    // $scope.passenger[index] = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


            //=========Card Modal start=======================

            $scope.payment = {};
            $scope.customeradd = {};
            $http.get('/api/traveller-card')
                .then(function (data) {
                    $scope.myTravellerCard = data.data;
                });



            $scope.selectTravellerCard = function ( size) { console.log(size)

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/template/traveller-card.modal.html',
                    controller: 'TravellerCardListModalCtrl',
                    size: size,
                    resolve: {
                        travellercards: function () {
                            return $scope.myTravellerCard
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    $scope.updateBillingAddress($scope.customeradd, $scope.payment, selectedItem);
                    // $scope.passenger[index] = selectedItem;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
            $scope.updateBillingAddress = function (customeradd, payment, item) {
                console.log(item)
                customeradd.first_name = item.billing_firstname;
                customeradd.last_name = item.billing_lastname;
                customeradd.email = item.billing_email;
                customeradd.address = item.billing_address;
                customeradd.city = item.billing_city;
                customeradd.postalcode = item.billing_postalcode;
                customeradd.country = item.billing_country;
                customeradd.state = item.billing_state;
                customeradd.phone = item.billing_phone;

                payment.card_number = item.card_number;
                payment.cardholder = item.cardholder;
                payment.exp_date = item.exp_date;
                payment.cvv = item.cvv;


            }


            //=========Card Modal End=========================



            $scope.updateTraveller = function (passenger, item) {
                passenger.title = item.title;
                //passenger.tgit itle = 'Miss';
                passenger.first_name = item.first_name;
                passenger.middle_name = item.middle_name;
                passenger.last_name = item.last_name;
                passenger.dob = item.dob;
                passenger.nationality = item.nationality;
                passenger.passport_no = item.passport_no;
                passenger.passport_issue_country = item.passport_issue_country;
                passenger.passport_expiry_date = item.passport_expiry_date;
            }

        }]);
})();
