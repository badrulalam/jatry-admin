<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\Role;
use DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'userregister', 'userregistersave', 'frontAgentLoginAction']]);
    }





/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function doLogin_org(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

//        $email = Request::input('email');
        $email = $request->input('email');

//        $password = Request::input('password');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
//            return redirect()->intended('dashboard');
            return redirect()->route('allusers');
        }
    }




    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

//        if ($this->auth->attempt($credentials, $request->has('remember')))
        if (Auth::attempt($credentials, $request->has('remember')))
        {
//            if(Auth::user()->active == 1)
//            {
//                return redirect()->intended($this->redirectPath());
//            }

//            return redirect('aut.reset');
//            return redirect('userregister');
            return redirect()->route('allusers');

        }

        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
//                'email' => $this->getFailedLoginMesssage(),
//                'crederr' => $this->getFailedLoginMesssage(),
                  'crederr' => $this->getFailedLoginMessage(),
            ]);
    }

//    protected function getFailedLoginMesssage()
//    {
//        return Lang::has('auth.failed')
//            ? Lang::get('auth.failed')
//            : 'These credentials do not match our recordsDDD.';
//    }


    public function frontAgentLoginDOAction(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

//        if ($this->auth->attempt($credentials, $request->has('remember')))
        if (Auth::attempt($credentials, $request->has('remember')))
        {
//            if(Auth::user()->active == 1)
//            {
//                return redirect()->intended($this->redirectPath());
//            }


            return redirect()->route('allusers');

        }

        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
//                'email' => $this->getFailedLoginMesssage(),
//                'crederr' => $this->getFailedLoginMesssage(),
                'crederr' => $this->getFailedLoginMessage(),
            ]);
    }



















    protected function userregister()
    {
        $roles = Role::lists('display_name','id');
        return view('auth.userregister',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function userregistersave(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }


    protected function agentRegisterSaveAction(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
//        $input['password'] = Hash::make($input['password']);
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('allusers')
            ->with('success','User created successfully');
    }


}
